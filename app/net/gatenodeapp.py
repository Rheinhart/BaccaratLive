#coding:utf8
'''
Created on 2013-8-14

@author: lan (www.9miao.com)
'''
from firefly.server.globalobject import GlobalObject,remoteserviceHandle
from datetime import datetime

@remoteserviceHandle('gate')
def pushObject(topicID,msg,sendList):
    _sendList = [str(i) for i in sendList]
    print "[Analysis]", GlobalObject().name+"#"+",".join(_sendList), "net push", topicID-10000, datetime.now()
    #name = GlobalObject().json_config.get("name")
    #print name, "push to ", sendList, "with command Id ", topicID, (datetime.now() - starttime).total_seconds()
    GlobalObject().netfactory.pushObject(topicID, msg, sendList)

@remoteserviceHandle('gate')
def getClientIP(dynamicID):
    conn = GlobalObject().netfactory.connmanager.getConnectionByID(dynamicID)
    if conn:
        return conn.instance.transport.client[0]
    else:
        return None