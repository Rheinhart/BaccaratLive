#coding:utf8
'''
Created on 2013-8-14

@author: lan (www.9miao.com)
'''
from firefly.server.globalobject import GlobalObject
from firefly.netconnect.datapack import DataPackProtoc


def callWhenConnLost(conn):
    # Server不會在斷開連接時把客戶清除, 只有:
    # 1. 客戶主動登出
    # 2. 客戶心跳過時, Server主動清除
    # 3. 客戶重複登錄, Server主動發905包給原客戶
    # 但需要把customer(login server) 及player (game server)的connected改成false, 不再發消息
    dynamicId = "%s#%s" % (GlobalObject().name, conn.transport.sessionno)
    GlobalObject().remote['gate'].callRemote("netconnlost", dynamicId)

GlobalObject().netfactory.doConnectionLost = callWhenConnLost
dataprotocl = DataPackProtoc(78,37,38,48,9,0)
GlobalObject().netfactory.setDataProtocl(dataprotocl)



def loadModule():
    import netapp
    import gatenodeapp