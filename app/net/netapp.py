#coding:utf8
'''
Created on 2013-8-14

@author: lan (www.9miao.com)
'''
from firefly.server.globalobject import GlobalObject
from firefly.utils.services import CommandService,Service
from twisted.python import log
from twisted.internet import defer, threads
from datetime import datetime

class NetCommandService(CommandService):
    
    def callTargetSingle(self,targetKey,*args,**kw):
        '''call Target by Single
        @param conn: client connection
        @param targetKey: target ID
        @param data: client data
        '''

        self._lock.acquire()
        try:
            target = self.getTarget(0)
            if not target:
                log.err('the command '+str(targetKey)+' not Found on service')
                return None
            if targetKey not in self.unDisplay:
                log.msg("call method %s on service[single]"%target.__name__)
            defer_data = target(targetKey,*args,**kw)
            if not defer_data:
                return None
            if isinstance(defer_data,defer.Deferred):
                return defer_data
            d = defer.Deferred()
            d.callback(defer_data)
        finally:
            self._lock.release()
        return d


    def callTargetParallel(self,targetKey,*args,**kw):
        '''call Target by Single
        @param conn: client connection
        @param targetKey: target ID
        @param data: client data
        '''
        self._lock.acquire()
        try:
            target = self.getTarget(0)
            if not target:
                log.err('the command '+str(targetKey)+' not Found on service')
                return None
            log.msg("call method %s on service[parallel]"%target.__name__)
            d = threads.deferToThread(target,targetKey,*args,**kw)
        finally:
            self._lock.release()
        return d



netservice = NetCommandService("loginService", runstyle=Service.SINGLE_STYLE)

def netserviceHandle(target):
    """
    """
    netservice.mapTarget(target)

GlobalObject().netfactory.addServiceChannel(netservice)


@netserviceHandle
def Forwarding_0(keyname, _conn, data):
    '''消息转发，将客户端发送的消息请求转发给gateserver分配处理
    '''
    dynamicId = "%s#%s" % (GlobalObject().name, _conn.transport.sessionno)
    print "[Analysis]", dynamicId, "net forwarding", keyname, datetime.now()
    ip = _conn.transport.client[0]
    dd = GlobalObject().remote['gate'].callRemote("forwarding", keyname, dynamicId, ip, data)
    return dd

