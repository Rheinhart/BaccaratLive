#-*-coding:utf8-*-
'''
Created on 2013-6-5

@author: lan (www.9miao.com)
'''
from firefly.dbentrust.mmode import MAdmin

tb_table_admin = MAdmin('t_table', 'tableid')
tb_table_admin.insert()

tb_video_admin = MAdmin('t_video', 'videoid', fk="flag")
tb_video_admin.insert()