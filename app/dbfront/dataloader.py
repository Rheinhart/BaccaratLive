#coding:utf8
'''
Created on 2013-8-14

@author: lan (www.9miao.com)
'''
import memmode
from firefly.dbentrust.madminanager import MAdminManager
from twisted.internet import reactor
reactor = reactor

def initData():
    """载入角色初始数据
    """
    # McharacterManager().initData()
    
def registe_madmin():
    """注册数据库与memcached对应
    """
    MAdminManager().registe(memmode.tb_table_admin)
    MAdminManager().registe(memmode.tb_video_admin)

def CheckMemDB(delta):
    """同步内存数据到数据库
    """
    MAdminManager().checkAdmins()
    reactor.callLater(delta, CheckMemDB, delta)
