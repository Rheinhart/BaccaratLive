#coding:utf8
__author__ = 'Alan'

from firefly.dbentrust.dbpool import dbpool
from MySQLdb.cursors import DictCursor
from datetime import datetime
from time import sleep
import os
from app.game.core.ProcessQueue import ProcessQueue
from hashlib import md5
MAX_RETRY = 3
from thread import allocate_lock
mutex = allocate_lock()

def callProceure(name, sql):
    try:
        trial = 0
        while True:
            ProcessQueue().add("DB")
            # startcalltime = datetime.now()
            conn = dbpool.connection()
            cursor = conn.cursor()
            cursor.execute(sql)
            result = cursor.fetchone()
            cursor.close()
            #conn.commit()
            ProcessQueue().release("DB")
            conn.close()
            if result[0] == 0:
                return 0
            else:
                print name, "failed:", result
                if "try restarting transaction" in result[0]:
                    trial += 1
                    if trial > MAX_RETRY:
                        print "Max retry is reached. Please checck the problem manually."
                        return 1003
                    print "Wait 1 second to retry. Retry = %s. Max retry = %s." % (trial, MAX_RETRY)
                    sleep(1)
                else:
                    return 1003
    except Exception as e:
        print name, "failed:", e
        return 1004

def callOnBet(roundcode,loginname, playtype, bet_amount_cents, tableid, seat, create_ip, videoid, gametype, dealer):
    '''下注
    '''
    #from app.game.core.PlayersManager import PlayersManager
    #dynamicID = PlayersManager().getPlayerByLoginname(loginname).dynamicId
    hash = md5("%s%s%s" % (roundcode,playtype,bet_amount_cents)).hexdigest()
    sql = "call onbet('%s', '%s', %s, %s, '%s', %s,'%s', '%s', '%s','%s','%s');" % (roundcode, loginname, playtype, bet_amount_cents, tableid, seat, create_ip, videoid, gametype, dealer, hash)
    name = "10028 %s %s %s" % (roundcode, loginname, "callOnBet", )
    result = callProceure(name, sql)
    return result

def callOnReckon(roundcode):
    '''結算
    '''
    sql = "call onreckonround('%s');" % (roundcode)
    name = "%s %s" % (roundcode, "callOnReckon")
    result = callProceure(name, sql)
    return result

def callOnCancel(roundcode):
    '''取消結算
    '''
    sql = "call oncancelround('%s');" % (roundcode)
    name = "%s %s" % (roundcode, "callOnCancel")
    return callProceure(name, sql)

def callOnRecalc(roundcode):
    '''重新結算
    '''
    sql = "call onrecalcround('%s');" % (roundcode)
    name = "%s %s" % (roundcode, "callOnRecalc")
    return callProceure(name, sql)