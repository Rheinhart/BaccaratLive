# coding:utf8
"""
Created on 2015-9-20
@__author__ = 'Thomas'
"""
from firefly.dbentrust.dbpool import dbpool
from MySQLdb.cursors import DictCursor


def getAllPersonalLimitFromDB():
    '''从数据库读取所有个人限红数据'''
    sql = "SELECT * FROM t_personal_limitset where flag = 0;"
    conn = dbpool.connection()
    cursor = conn.cursor(cursorclass=DictCursor)
    cursor.execute(sql)
    result = cursor.fetchall()
    cursor.close()
    conn.close()
    limits = {}
    for _item in result:
        limitid = _item['limitid']
        playtype = _item['playtype']
        if not limits.has_key(limitid):
            limits[limitid] = {}
        limits[limitid][playtype] = _item
    return limits

def getAllTableLimitFromDB():
    '''从数据库读取所有桌台限红数据'''
    sql = "SELECT * FROM t_table_limitset where flag = 0;"
    conn = dbpool.connection()
    cursor = conn.cursor(cursorclass=DictCursor)
    cursor.execute(sql)
    result=cursor.fetchall()
    cursor.close()
    conn.close()
    limits = {}
    for _item in result:
        limitid = _item['limitid']
        playtype = _item['playtype']
        if not limits.has_key(limitid):
            limits[limitid] = {}
        limits[limitid][playtype] = _item
    return limits