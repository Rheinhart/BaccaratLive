#coding:utf8
__author__ = 'Alan'

from firefly.dbentrust.dbpool import dbpool
from MySQLdb.cursors import DictCursor

def getReckonResult(roundcode):
    """
    """
    sql = "select loginname, win_amount_cents, bet_amount_cents, playtype, tableid from t_orders where roundcode = '%s' and flag = 1" % roundcode
    conn = dbpool.connection()
    cursor = conn.cursor(cursorclass=DictCursor)
    cursor.execute(sql)
    result=cursor.fetchall()
    cursor.close()
    conn.close()
    return result

def getRecalcResult(roundcode):
    """
    """
    sql = "select loginname, win_amount_cents, bet_amount_cents, playtype, tableid from t_orders where roundcode = '%s' and flag = 8" % roundcode
    conn = dbpool.connection()
    cursor = conn.cursor(cursorclass=DictCursor)
    cursor.execute(sql)
    result=cursor.fetchall()
    cursor.close()
    conn.close()
    return result