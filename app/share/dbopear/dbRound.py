#coding=utf8
__author__ = 'joshua'


from firefly.dbentrust.dbpool import dbpool
from MySQLdb.cursors import DictCursor

def updateRoundClosing(roundCode, playerpoint, bankerpoint, cards, cardnum, pair):
    """更新局结果
    """
    try:
        sql = "update `t_rounds` set playerpoint = %d, " \
              " bankerpoint = %d, cards = '%s', cardnum = %d, pair = %d, closetime = now() " \
              "where roundcode = '%s'" % (playerpoint, bankerpoint, cards, cardnum, pair, roundCode)
        conn = dbpool.connection()
        cursor = conn.cursor(cursorclass=DictCursor)
        affected_count = cursor.execute(sql)
        cursor.close()
        conn.commit()
        conn.close()
        if affected_count == 1:
            return 0
        else:
            print "updateRoundInfo failed: incorrect affected row:", affected_count
            return 1003
    except Exception as e:
        print "updateRoundInfo failed:", e
        return 1003

def insertRound(roundcode, gametype, videoid, flag, begintime, shoecode, dealer):
    """插入新的局
    :param data:
    :return:
    """
    try:
        sql = "insert into `t_rounds` (roundcode, gametype, videoid, flag, begintime, shoecode, dealer) " \
              "values('%s', '%s', '%s', %d, '%s', '%s', '%s')" % (roundcode, gametype, videoid, flag, begintime, shoecode, dealer)
        conn = dbpool.connection()
        cursor = conn.cursor(cursorclass=DictCursor)
        affected_count = cursor.execute(sql)
        cursor.close()
        conn.commit()
        conn.close()
        if affected_count == 1:
            return 0
        else:
            print "insertRound failed: incorrect affected row:", affected_count
            return 1003
    except Exception as e:
        print "insertRound failed:", e
        return 1003

def getLatestRoundCodeByVideoID(videoId):
    sql = "select roundcode, shoecode from `t_rounds` where videoid = '%s' order by roundcode DESC limit 1" % videoId
    conn = dbpool.connection()
    cursor = conn.cursor(cursorclass=DictCursor)
    cursor.execute(sql)
    result = cursor.fetchone()
    cursor.close()
    conn.close()
    return result

def getRoundInfoByShoeCode(shoecode):
    sql = "select * from `t_rounds` WHERE shoecode = '%s' order by roundcode DESC limit 1" % shoecode
    conn = dbpool.connection()
    cursor = conn.cursor(cursorclass=DictCursor)
    cursor.execute(sql)
    result = cursor.fetchall()
    cursor.close()
    conn.close()
    return result