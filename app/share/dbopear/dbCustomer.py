#coding:utf8
__author__ = 'joshua'

from firefly.dbentrust.dbpool import dbpool
from MySQLdb.cursors import DictCursor
import datetime

def getCustomerInfo(loginname, password):
    sql = "select * from `t_customers` where loginname = '%s'\
     and password = '%s'" % (loginname, password)
    conn = dbpool.connection()
    cursor = conn.cursor(cursorclass=DictCursor)
    cursor.execute(sql)
    result = cursor.fetchone()
    cursor.close()
    conn.close()
    return result

def updateCustomerLoginInfo(loginname, login_ip):
    try:
        sql = "update `t_customers` set last_login_time = now()\
         , last_login_ip = '%s' where loginname = '%s'" % (login_ip, loginname)
        conn = dbpool.connection()
        cursor = conn.cursor(cursorclass=DictCursor)
        affected_count = cursor.execute(sql)
        cursor.close()
        conn.commit()
        conn.close()
        if affected_count == 1:
            return 0
        else:
            print "updateCustomerLoginInfo failed: incorrect affected row:", affected_count
            return 1003
    except Exception as e:
        print "updateCustomerLoginInfo failed:", e
        return 1003