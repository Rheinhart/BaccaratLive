#coding=utf8
__author__ = 'joshua'
from firefly.dbentrust.dbpool import dbpool
from MySQLdb.cursors import DictCursor

def getLastValidBulletin():
    sql = "SELECT * FROM t_bulletin  WHERE flag =  0 ORDER BY create_time DESC LIMIT 1;"
    conn = dbpool.connection()
    cursor = conn.cursor(cursorclass=DictCursor)
    cursor.execute(sql)
    result = cursor.fetchall()
    cursor.close()
    conn.close()
    return result