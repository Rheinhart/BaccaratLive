#coding=utf8
__author__ = 'joshua'

from firefly.dbentrust.dbpool import dbpool
from MySQLdb.cursors import DictCursor

def getBillsByBillNo(billNo):
    sql = "select a.*, b.bankerpoint, b.playerpoint from `t_orders` a JOIN `t_rounds` b on a.roundcode = b.roundcode" \
          " WHERE billno = '%s'" % billNo
    conn = dbpool.connection()
    cursor = conn.cursor(cursorclass=DictCursor)
    cursor.execute(sql)
    result = cursor.fetchone()
    cursor.close()
    conn.close()
    return result

def getBillsByRoundCode(loginName, roundCode, page, countOfPage):
    conn = dbpool.connection()
    cursor = conn.cursor(cursorclass=DictCursor)
    sql = "select count(*) from `t_orders` WHERE roundcode = '%s' and loginname = '%s'"
    cursor.execute(sql)
    cnt = cursor.fetchone()
    if cnt == 0:
        return cnt, []
    sql = "select a.*, b.bankerpoint, b.playerpoint from `t_orders` a JOIN `t_rounds` b on a.roundcode = b.roundcode" \
          " WHERE a.roundcode = '%s' and a.loginname = '%s' ORDER BY a.billno DESC LIMIT %d, %d;" \
          % (roundCode, loginName, countOfPage*(page-1), countOfPage)
    print sql
    cursor.execute(sql)
    result = cursor.fetchall()
    cursor.close()
    conn.close()
    return cnt, result

def getBillsByTimeRange(loginName, page, countOfPage, startTime, endTime, game_type):
    conn = dbpool.connection()
    cursor = conn.cursor(cursorclass=DictCursor)
    sql = "select count(*) from `t_orders` WHERE loginname = '%s'" \
          " and create_time BETWEEN '%s' AND '%s'" % (loginName, startTime, endTime)
    cursor.execute(sql)
    cnt = cursor.fetchone()
    if cnt == 0:
        return cnt, []
    sql = "select a.*, b.bankerpoint, b.playerpoint from `t_orders` a JOIN `t_rounds` b on a.roundcode = b.roundcode" \
          " WHERE a.loginname = '%s' and a.create_time BETWEEN '%s' AND '%s'" \
          "ORDER BY a.billno DESC LIMIT %d, %d;" % (loginName, startTime, endTime, countOfPage*(page-1), countOfPage)
    print sql
    cursor.execute(sql)
    result = cursor.fetchall()
    cursor.close()
    conn.close()
    return cnt, result
