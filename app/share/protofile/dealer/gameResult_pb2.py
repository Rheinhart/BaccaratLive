# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: gameResult.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='gameResult.proto',
  package='protofile.dealer.gameResult',
  serialized_pb=_b('\n\x10gameResult.proto\x12\x1bprotofile.dealer.gameResult\"\x81\x01\n\x12gameResultResponse\x12\x11\n\troundCode\x18\x01 \x02(\t\x12\x0f\n\x07videoId\x18\x02 \x02(\t\x12\x13\n\x0b\x62\x61nkerPoint\x18\x03 \x02(\x05\x12\x13\n\x0bplayerPoint\x18\x04 \x02(\x05\x12\x0c\n\x04pair\x18\x05 \x02(\x05\x12\x0f\n\x07\x63\x61rdNum\x18\x06 \x02(\x05')
)
_sym_db.RegisterFileDescriptor(DESCRIPTOR)




_GAMERESULTRESPONSE = _descriptor.Descriptor(
  name='gameResultResponse',
  full_name='protofile.dealer.gameResult.gameResultResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='roundCode', full_name='protofile.dealer.gameResult.gameResultResponse.roundCode', index=0,
      number=1, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='videoId', full_name='protofile.dealer.gameResult.gameResultResponse.videoId', index=1,
      number=2, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='bankerPoint', full_name='protofile.dealer.gameResult.gameResultResponse.bankerPoint', index=2,
      number=3, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='playerPoint', full_name='protofile.dealer.gameResult.gameResultResponse.playerPoint', index=3,
      number=4, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='pair', full_name='protofile.dealer.gameResult.gameResultResponse.pair', index=4,
      number=5, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
    _descriptor.FieldDescriptor(
      name='cardNum', full_name='protofile.dealer.gameResult.gameResultResponse.cardNum', index=5,
      number=6, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=50,
  serialized_end=179,
)

DESCRIPTOR.message_types_by_name['gameResultResponse'] = _GAMERESULTRESPONSE

gameResultResponse = _reflection.GeneratedProtocolMessageType('gameResultResponse', (_message.Message,), dict(
  DESCRIPTOR = _GAMERESULTRESPONSE,
  __module__ = 'gameResult_pb2'
  # @@protoc_insertion_point(class_scope:protofile.dealer.gameResult.gameResultResponse)
  ))
_sym_db.RegisterMessage(gameResultResponse)


# @@protoc_insertion_point(module_scope)
