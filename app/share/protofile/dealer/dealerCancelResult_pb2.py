# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: dealerCancelResult.proto

import sys
_b=sys.version_info[0]<3 and (lambda x:x) or (lambda x:x.encode('latin1'))
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
from google.protobuf import descriptor_pb2
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='dealerCancelResult.proto',
  package='protofile.dealer.dealerCancelResult',
  serialized_pb=_b('\n\x18\x64\x65\x61lerCancelResult.proto\x12#protofile.dealer.dealerCancelResult\".\n\x19\x64\x65\x61lerCancelResultRequest\x12\x11\n\troundCode\x18\x01 \x02(\t\"*\n\x1a\x64\x65\x61lerCancelResultResponse\x12\x0c\n\x04\x63ode\x18\x01 \x02(\x05')
)
_sym_db.RegisterFileDescriptor(DESCRIPTOR)




_DEALERCANCELRESULTREQUEST = _descriptor.Descriptor(
  name='dealerCancelResultRequest',
  full_name='protofile.dealer.dealerCancelResult.dealerCancelResultRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='roundCode', full_name='protofile.dealer.dealerCancelResult.dealerCancelResultRequest.roundCode', index=0,
      number=1, type=9, cpp_type=9, label=2,
      has_default_value=False, default_value=_b("").decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=65,
  serialized_end=111,
)


_DEALERCANCELRESULTRESPONSE = _descriptor.Descriptor(
  name='dealerCancelResultResponse',
  full_name='protofile.dealer.dealerCancelResult.dealerCancelResultResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='code', full_name='protofile.dealer.dealerCancelResult.dealerCancelResultResponse.code', index=0,
      number=1, type=5, cpp_type=1, label=2,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      options=None),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  options=None,
  is_extendable=False,
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=113,
  serialized_end=155,
)

DESCRIPTOR.message_types_by_name['dealerCancelResultRequest'] = _DEALERCANCELRESULTREQUEST
DESCRIPTOR.message_types_by_name['dealerCancelResultResponse'] = _DEALERCANCELRESULTRESPONSE

dealerCancelResultRequest = _reflection.GeneratedProtocolMessageType('dealerCancelResultRequest', (_message.Message,), dict(
  DESCRIPTOR = _DEALERCANCELRESULTREQUEST,
  __module__ = 'dealerCancelResult_pb2'
  # @@protoc_insertion_point(class_scope:protofile.dealer.dealerCancelResult.dealerCancelResultRequest)
  ))
_sym_db.RegisterMessage(dealerCancelResultRequest)

dealerCancelResultResponse = _reflection.GeneratedProtocolMessageType('dealerCancelResultResponse', (_message.Message,), dict(
  DESCRIPTOR = _DEALERCANCELRESULTRESPONSE,
  __module__ = 'dealerCancelResult_pb2'
  # @@protoc_insertion_point(class_scope:protofile.dealer.dealerCancelResult.dealerCancelResultResponse)
  ))
_sym_db.RegisterMessage(dealerCancelResultResponse)


# @@protoc_insertion_point(module_scope)
