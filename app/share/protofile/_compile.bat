@echo off
protoc -I=./ --python_out=./ player.proto
protoc -I=./ --python_out=./ game.proto
protoc -I=./ --python_out=./ login.proto
protoc -I=./ --python_out=./ getVideos.proto
protoc -I=./ --python_out=./ getVideoList.proto
protoc -I=./ --python_out=./ addVideo.proto
protoc -I=./ --python_out=./ tableLimit.proto
protoc -I=./ --python_out=./ personalLimit.proto
protoc -I=./ --python_out=./ dealer.proto


pause