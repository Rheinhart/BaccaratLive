#coding:utf8
"""
@__author__ = 'Thomas'
"""
from app.share.dbopear import dbLimit

_PersonalLimits = dbLimit.getAllPersonalLimitFromDB()
_TableLimits = dbLimit.getAllTableLimitFromDB()

def GetAllPersonalLimitInfo():
    '''获取所有个人限红'''
    return _PersonalLimits

def GetAllTableLimitInfo():
     '''获取所有桌台限红'''
     return _TableLimits

def GetPersonalLimitInfo(limitid, playtype):
    '''获取指定个人限红'''
    limits = _PersonalLimits.get(limitid)
    if limits:
        return limits.get(playtype)
    return None

def GetTableLimitInfo(limitid, playtype):
    '''获取指定桌台限红'''
    limits = _TableLimits.get(limitid)
    if limits:
        return limits.get(playtype)
    return None