#coding=utf8
__author__ = 'joshua'

import time
from datetime import datetime

from app.game.core.GameDataManager import GameDataManager
from app.game.core.GameVideo import GameStatus
from app.game.core.PlayersManager import PlayersManager
from app.share.protofile import game_pb2
from app.game.gatenodeservice import gatepush, gateDropClient


def videoProcess():
    """
    线程不停的（0.5S）检查一次视频的下注状态，当下注倒计时结束就更新视频状态并通知dealer发牌，收到dealer的发牌，更新video的当前局的牌信息并发给client.
    Gameprocess 游戏进程控制， 定时查询视频的状态， 当下注倒计时为0的时候， 把视频的状态修改成发牌状态，发消息通知荷官端发牌。
    """
    while True:
        time.sleep(1)
        #print "videoProcess", datetime.now()
        for gamevideo in GameDataManager().getAllVideos():
            # 下注狀態
            if gamevideo.gameStatus == GameStatus.EGS_BETTING and gamevideo.startTime is not None:
                # 倒計時為0, 切換成發牌
                if gamevideo.getBetTime() <= 0:
                    gamevideo.onStopBet()


def broadcast():
    while True:
        time.sleep(10)
        #print "broadcast", datetime.now()
        # lobby broadcast
        dynamicIDs = PlayersManager().getAllLobbyPlayerIDs()
        # print "getLobbyPlayerDynamicIDList", len(dynamicIDs)
        # 大厅主玩法彩池信息 0x0002002c
        response = game_pb2.lobbyPotResponse()
        videos = GameDataManager().getAllVideos()
        response.num = len(videos)
        for gamevideo in videos:
            data = gamevideo.getVideoMainPotData()
            response_item = response.details.add()
            response_item.videoid = data.get('videoid')
            response_item.player_num = data.get('player_num')
            response_item.banker_bet_number = data.get('banker_bet_number')
            response_item.player_bet_number = data.get('player_bet_number')
            response_item.tie_bet_number = data.get('tie_bet_number')
            response_item.banker_bet_amount_cents = data.get('banker_bet_amount_cents')
            response_item.player_bet_amount_cents = data.get('player_bet_amount_cents')
            response_item.tie_bet_amount_cents = data.get('tie_bet_amount_cents')
        gatepush(20044, response.SerializeToString(), dynamicIDs)

        # table broadcast
        for gamevideo in GameDataManager().getAllVideos():
            # 下注狀態
            if gamevideo.gameStatus == GameStatus.EGS_BETTING:
                for gametable in gamevideo.tables:
                    # 桌子已下注信息 0x00020027
                    gametable.sendTableBet()
                    # 实时彩池 0x0002001d （视频总彩池）
                    gametable.sendPot()


def detectHeartBeat():
    while True:
        time.sleep(10)
        #print "detectHeartBeat", datetime.now()
        for player in PlayersManager().getAllPlayers():
            if player.lastHeartBeat:
                if (datetime.now() - player.lastHeartBeat).seconds > 120:
                    print "end heartbeat for more than 2 minute"
                    PlayersManager().dropPlayer(player)
                    gateDropClient(dynamicId=player.dynamicId)
                # else:
                #     print (datetime.now() - player.lastHeartBeat).seconds
            else:
                pass

def performanceMonitor():
    from app.game.core.ProcessQueue import ProcessQueue
    from app.game.core.ThreadController import ThreadController
    import os
    import csv
    queue = ProcessQueue()
    thread = ThreadController()
    f_queue = open(os.path.join("app", "logs", "performance-queue.log"), "wb")
    #csv_queue = csv.writer(f_queue, delimiter = '\t')
    f_thread = open(os.path.join("app", "logs", "performance-thread.log"), "wb")
    #csv_thread = csv.writer(f_thread, delimiter = '\t')
    try:
        #csv_thread.writerow(thread.output_headers)
        #csv_queue.writerow(["datetime", "bet queue", "reckon queue", "db queue"])
        f_queue.write("time\tbet queue\treckon queue\tdb queue" + "\n")
        f_thread.write(thread.output_headers + "\n")
        while True:
            time.sleep(5)
            # for queue
            #row = [time.strftime("%y/%m/%d %H:%M:%S"), "{:3d}".format(queue.get("callOnBet")), "{:3d}".format(queue.get("onReckonRound")), "{:3d}".format(queue.get("DB"))]
            row = "%s\t%5d\t%5d\t%5d" % (time.strftime("%y/%m/%d %H:%M:%S"), queue.get("callOnBet"), queue.get("onReckonRound"), queue.get("DB"))
            #csv_queue.writerow(row)
            f_queue.write(row + "\n")
            # for thread
            while thread.output_queue.qsize() > 0:
                row = thread.output_queue.get()
                #csv_thread.writerow(row)
                f_thread.write(row + "\n")
            f_queue.flush()
            f_thread.flush()
    finally:
        print "close file"
        f_queue.close()
        f_thread.close()
