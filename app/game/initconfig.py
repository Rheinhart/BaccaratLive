#coding:utf8
'''
Created on 2013-8-14

@author: lan (www.9miao.com)
'''
from twisted.python import log
from gatenodeapp import *
from dataloader import load_config_data,registe_madmin
from firefly.server.globalobject import GlobalObject
from app.game.core.PlayersManager import PlayersManager


def doWhenStop():
    """服务器关闭前的处理
    """
    for player in PlayersManager().getAllPlayers():
        try:
            PlayersManager().dropPlayer(player)
        except Exception as ex:
            log.err(ex) 
    
GlobalObject().stophandler = doWhenStop


def loadModule():
    """
    """
    load_config_data()
    registe_madmin()
    from service.GameProcess import videoProcess, detectHeartBeat, broadcast, performanceMonitor
    from twisted.internet import reactor, threads
    reactor.suggestThreadPoolSize(10)
    reactor.callLater(5, threads.deferToThread, detectHeartBeat)
    reactor.callLater(5, threads.deferToThread, videoProcess)
    reactor.callLater(5, threads.deferToThread, broadcast)
    reactor.callLater(5, threads.deferToThread, performanceMonitor)


