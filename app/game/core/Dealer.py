#coding=utf8
__author__ = 'joshua'

import string
import random
from time import strftime

import GameDataManager
from firefly.utils.singleton import Singleton


def genRandomStr(length):
    chars=string.ascii_letters+string.digits
    # chars = string.digits
    return ''.join([random.choice(chars) for i in range(length)])


class Dealer:

    def __init__(self, dynamicId, videoId, name):
        self.videoId = videoId
        self.id = strftime("%y%m%d%H%M") + genRandomStr(3)
        self.name = name
        self.dynamicId = dynamicId
        self.video = None
        self.setVideo(videoId)

    def setVideo(self, videoid):
        gamevideo = GameDataManager.GameDataManager().getVideoByID(videoid)
        if gamevideo:
            self.video = gamevideo

    def onStartGame(self):
        if self.video:
            response = self.video.onStartGame()
            if response:
                return response
        return {"result": False}


    def onNewShoe(self):
        if self.video:
            response = self.video.onNewShoe()
            if response:
                return {"code": 0}
        return {"code": 1000}


    def onNewCard(self, cardIndex, cardValue):
        pass


    def onCloseGame(self):
        pass

    def sendDispatch(self):
        pass

    def sendGameSnapshot(self):
        pass


class DealerManager:

    __metaclass__ = Singleton

    def __init__(self):
        self._dealers = {}
        self._dealer_client = {}

    def dealerLogin(self, dealer):
        dynamicId = dealer.dynamicId
        id = dealer.id
        videoId = dealer.videoId

        self._dealers[id] = dealer
        self._dealer_client[dynamicId] = dealer
        gamevideo = GameDataManager.GameDataManager().getVideoByID(videoId)
        if gamevideo:
            gamevideo.setDealer(dealer)
        else:
            return {"code": 1000}
        return {"code": 0}

    def dropDealer(self, dynamicId):
        dealer = self._dealer_client[dynamicId]
        self._dealer_client.__delitem__(dynamicId)
        self._dealers.__delitem__(dealer.id)