#coding=utf8
# __author__ = 'Alan'

"""牌值说明
牌	值
花    A - K	1-13
方块  A - K	17 - 29
红心  A - K	33 - 45
黑桃  A - K	49 - 61
"""

def getCardsFromString(cards):
    cards_ = cards.split(":")
    bankerCards = [int(c) for c in cards_[0].split(",")]
    playerCards = [int(c) for c in cards_[1].split(",")]
    return bankerCards, playerCards

def isValidCards(cards):
    try:
        cards_ = cards.split(":")
        # 只有2組卡
        assert len(cards_) == 2, "Invalid cards string"
        bankerCards = cards_[0].split(",")
        playerCards = cards_[1].split(",")
        # 只可能是2,3張牌
        assert len(bankerCards) in (2,3), "Invalid banker cards string"
        assert len(playerCards) in (2,3), "Invalid player cards string"
        for card in bankerCards:
            # 檢查每張牌的牌值是否有效
            assert isValidCard(int(card)), "Invalid  card %s" % card
        for card in playerCards:
            # 檢查每張牌的牌值是否有效
            assert isValidCard(int(card)), "Invalid  card %s" % card
        return True
    except Exception as e:
        print e
        return False

def isValidCard(cardValue):
    return (cardValue <= 61) and (cardValue % 16) <= 13 and (cardValue % 16 > 0)

def computePair(bankerCards, playerCards):
    banker0 = getConvertedCardValue(bankerCards[0])
    banker1 = getConvertedCardValue(bankerCards[1])
    player0 = getConvertedCardValue(playerCards[0])
    player1 = getConvertedCardValue(playerCards[1])
    if banker0 == banker1 and player0 == player1:
        pair = 4 # 雙對
    elif banker0 == banker1:
        pair = 1 # 莊對
    elif player0 == player1:
        pair = 2 # 閒對
    else:
        pair = 0 # 無對
    return pair

def computeCardNum(bankerCards, playerCards):
    return len(bankerCards) + len(playerCards)

def computeCardsPoint(cards):
    return sum(cards) % 10

def getConvertedCardValue(cardValue):
    return cardValue % 16

""" 10、J、Q及K的撲克牌則被算作零點 """
def getCardPoint(cardValue):
    value = getConvertedCardValue(cardValue)
    if value >= 10:
        return 0
    return value