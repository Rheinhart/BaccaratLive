__author__ = 'Alan'

from firefly.utils.singleton import Singleton
from thread import allocate_lock

class ProcessQueue:

    __metaclass__ = Singleton

    def __init__(self):
        self.queue = {}
        self.mutex = allocate_lock()

    def create(self, name):
        self.queue[name] = 0

    def add(self, name):
        with self.mutex:
            if self.queue.has_key(name):
                self.queue[name] += 1
            else:
                self.create(name)
                self.queue[name] += 1

    def release(self, name):
        with self.mutex:
            if self.queue.has_key(name):
                self.queue[name] -= 1

    def get(self, name):
        return self.queue.get(name, -1)

    def getall(self):
        return self.queue
