__author__ = 'Alan'

from twisted.internet import reactor, threads
from twisted.python import threadpool
from thread import start_new_thread
from app.game.core.ProcessQueue import ProcessQueue
from datetime import datetime
import time

from firefly.utils.singleton import Singleton
from thread import allocate_lock
import Queue

class ThreadController:

    __metaclass__ = Singleton

    def __init__(self):
        self.mutex = allocate_lock()
        #self.thread_queue = []
        self.id = 0
        self.output_queue = Queue.Queue()
        self.thread_pool = {}
        self.thread_pool["callOnBet"] = threadpool.ThreadPool(1,30)
        self.thread_pool["callOnBet"].start()
        self.thread_pool["onReckonRound"] = threadpool.ThreadPool(1,3)
        self.thread_pool["onReckonRound"].start()
        #self.output_headers = ["datetime", "name", "id", "total time", "queue time", "run time"]
        self.output_headers = "time\ttotal time\tqueue time\trun time\tname"

    def getID(self):
        with self.mutex:
            self.id += 1
            return self.id

    def deferedCall(self, f, *args):
        ProcessQueue().add(f.__name__)
        item = {"name":f.__name__, "queueat":datetime.now(), "callat":None, "finishat":None, "result":None}
        pool = self.thread_pool.get(f.__name__)
        #self.thread_queue.append(item)
        f = self.runWithLog(f, item)
        if pool:
            d = threads.deferToThreadPool(reactor, pool, f, *args)
        else:
            d = threads.deferToThread(f, *args)
        d.addCallback(self.defaultCallback, item)
        d.addErrback(self.defaultCallback, item)
        return d

    def defaultCallback(self, result, item):
        item["finishat"] = datetime.now()
        item["result"] = "done"
        self.writeToOutput(item)
        return result

    def defaultErrback(self, failure, item):
        item["finishat"] = datetime.now()
        item["result"] = "fail"
        self.writeToOutput(item)
        return failure

    def writeToOutput(self, item):
        totaltime = (item["finishat"]-item["queueat"]).total_seconds()
        queuetime = (item["callat"]-item["queueat"]).total_seconds()
        runtime = (item["finishat"]-item["callat"]).total_seconds()
        output = "%s\t%.2f\t%.2f\t%.2f\t%s" % (time.strftime("%Y/%m/%d %H:%M:%S"), totaltime, queuetime, runtime, item["name"])
        self.output_queue.put(output)

    def runWithLog(self, func, item):
        def withLog(*args, **kwargs):
            ProcessQueue().release(item["name"])
            item["callat"] = datetime.now()
            return func(*args, **kwargs)
        return withLog