#coding:utf8

# __author__ = 'Alan'

from thread import allocate_lock

from firefly.utils.singleton import Singleton
from app.game.core.Player import Player


class PlayersManager:

    __metaclass__ = Singleton

    def __init__(self):
        self._players = {}
        self._dynamicIds = {}
        self.mutex = allocate_lock()

    def addPlayer(self, player_data, dynamicId=-1):
        loginname = player_data.get('loginname')
        if not self._players.has_key(loginname):
            # For new player, create the player and save the player in the list
            player = Player(player_data, dynamicId)
            with self.mutex:
                self._players[loginname] = player
                self._dynamicIds[dynamicId] = loginname
            return player
        else:
            player = self._players[loginname]
            return player

    def updatePlayer(self, loginname, dynamicId):
        # For existing player,
        # 1. replace the list wirh the new dynamic id
        # 2. set the new dynamic id for the player
        player = self._players[loginname]
        original_dynamicId = player.dynamicId
        with self.mutex:
            del self._dynamicIds[original_dynamicId]
            self._dynamicIds[dynamicId] = loginname
            player.dynamicId = dynamicId
            player.connected = True
        return player

    def getPlayerByID(self,dynamicId):
        loginname = self._dynamicIds.get(dynamicId)
        if loginname:
            return self.getPlayerByLoginname(loginname)
        return None

    def getPlayerByLoginname(self, loginname):
        return self._players.get(loginname)

    def getAllPlayers(self):
        return self._players.values()

    def getAllPlayerIDs(self):
        return [player.dynamicId for player in self._players.values() if player.connected]

    def getAllLobbyPlayerIDs(self):
        return [player.dynamicId for player in self._players.values() if player.gametable is None and player.connected]

    def dropPlayer(self, player):
        player.onExitTable()
        loginname = player.loginname
        dynamicId = player.dynamicId
        with self.mutex:
            try:
                del self._players[loginname]
            except:
                pass
            try:
                del self._dynamicIds[dynamicId]
            except:
                pass

    def isPlayerOnline(self, loginname):
        return self._players.has_key(loginname)

    def doPlayerOffLine(self,player):
        pass

    def getLoginnameByDid(self, dynamicId):
        return self._dynamicIds.get(dynamicId)