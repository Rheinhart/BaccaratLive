#coding=utf8
__author__ = 'joshua'

from datetime import datetime
from time import strftime
import string
from thread import allocate_lock

from enum import Enum

from GameUtility import getConvertedCardValue, isValidCard, computePair, computeCardNum, computeCardsPoint
from app.game.memmode import tb_video_admin
import GameDataManager
from BeadRoad import BeadRoad
from PlayersManager import PlayersManager
from app.game.gatenodeservice import gatepush
from app.share.dbopear import dbRound, procedures
from app.share.dbopear import dbOrders
from app.share.protofile import game_pb2, player_pb2
from app.share.protofile import dealer_pb2
from twisted.internet import threads
from app.game.core.ThreadController import ThreadController

class GameStatus(Enum):
    EGS_UNKNOWN = 0
    EGS_NEWSHOE = 1  # 新建靴
    EGS_BETTING = 2  # 只在有这个状态下才接受下注
    EGS_DISTACH_CARD = 3  # 发牌状态
    EGS_CLOSEGAME = 4  # 游戏结束

class PlayType(Enum):
    BANKER = 1
    PLAYER = 2
    TIE = 3
    PAIR_BANKER = 4
    PAIR_PLAYER = 5
    LARGE = 6
    SMALLER = 7

class GameVideo:


    def __init__(self, data):
        self.videoID = data.get("videoid", "")
        self.gameType = data.get("gametype", "")
        self.betTime = data.get("bettime", 0)
        self.flag = 0
        self.url = data.get("url", "")

        self.gameStatus = GameStatus.EGS_UNKNOWN
        self.roundCode = ""
        self.shoeCode = ""
        self.cardIndex = 0
        self.bankerCards = []
        self.playerCards = []
        self.dealer = None
        self.tables = []
        self.beadRoads = {}  # one shoe one luzhu, clear when new a shoe
        self.videopot = {}  # one round one pot

        self.bankerPoint = 0
        self.playerPoint = 0
        self.cardNum = 0
        self.beadRoad = None
        self.pair = 0

        self.startTime = None

        self.mutex = allocate_lock()
        self.isReckonRunning = False
        mdata = tb_video_admin.getObjData(self.videoID)
        if mdata:
            print "video", mdata
            self.initWitMemode(mdata)
        else:
            tb_video_admin.new(data)
            print "new a video in memcache", tb_video_admin.getObjData(self.videoID)

        self.initVideo()

    def initWitMemode(self, data):
        self.videoID = data['videoid']
        self.gameType = data['gametype']
        self.betTime = data['bettime']
        self.flag = data['flag']
        self.url = data['url']
        self.tables = GameDataManager.GameDataManager().getTablesByVideoID(self.videoID)

    def initVideo(self):
        # read the lastest shoe, round and beadroads
        result = dbRound.getLatestRoundCodeByVideoID(self.videoID)
        if result:
            if result.get("roundcode", None):
                self.roundCode = result.get("roundcode")
            if result.get("shoecode", None):
                self.shoeCode = result.get("shoecode")
            if self.roundCode and self.roundCode != "" and self.shoeCode != "":
                rounds = dbRound.getRoundInfoByShoeCode(self.shoeCode)
                if rounds:
                    for round in rounds:
                        if round.get("bankerpoint", None):
                            self.beadRoads[round["roundcode"]] = BeadRoad(round["roundcode"], round)

    def setDealer(self, dealer):
        self.dealer = dealer
        # 視頻狀態更新 0x0002002d
        self.sendVideoStatus("dealer")

    def onNewShoe(self):
        if self.shoeCode.startswith(self.videoID + strftime("%y%m%d")):
            self.shoeCode = self.videoID + strftime("%y%m%d") + \
                           "{:03d}".format((int(self.shoeCode[-3:])+1) % 1000)
        else:
            self.shoeCode = self.videoID + strftime("%y%m%d") + "000"
        self.onStatusChange(GameStatus.EGS_NEWSHOE)
        self.beadRoads = {}
        print "new shoe", self.shoeCode
        # send new shoe info to all clients
        # 新建靴 0x0002002b
        # for gametable in self.tables:
        #     gametable.sendNewShoe()
        return self.shoeCode

    def onStartGame(self):
        if self.isReckonRunning:
            return {"code": 1009}

        if not self.shoeCode and self.shoeCode == "":
            self.onNewShoe()

        prefix = self.videoID + strftime("%y") + hex(int(strftime("%m")))[2:].upper() + strftime("%d")
        if self.roundCode.startswith(prefix):
            self.roundCode = prefix + calcRoundSeq(self.roundCode[-3:])
        else:
            self.roundCode = prefix + "000"
        self.videopot = {}
        self.beadRoad = BeadRoad(self.roundCode)
        self.cardIndex = 0
        self.bankerCards = []
        self.playerCards = []
        self.bankerPoint = 0
        self.playerPoint = 0
        self.betTime = 21
        self.pair = 0
        self.cardNum = 0
        self.startTime = datetime.now()
        result = dbRound.insertRound(self.roundCode, self.gameType, self.videoID, self.flag, datetime.now(), self.shoeCode, self.getDealerName())
        if result != 0:
            return {"code":result}
        self.onStatusChange(GameStatus.EGS_BETTING)
        for gametable in self.tables:
            gametable.onStartGame()
        # 开局回包 0x00040003
        response = dict()
        response["code"] = 0
        response["betTime"] = self.betTime
        response["roundCode"] = self.roundCode
        return response

    def onPlayerBet(self, player, roundcode, playtype, bet_amount_cents):
        if self.roundCode != roundcode:
            return 1007 # 局號過期
        if self.gameStatus != GameStatus.EGS_BETTING:
            return 1009 # 遊戲狀態不對
        args = (roundcode, player.loginname, playtype, bet_amount_cents, player.tableid, player.seat, player.ip, self.videoID, self.gameType, self.getDealerName())
        cargs = (player, playtype, bet_amount_cents)
        d = ThreadController().deferedCall(procedures.callOnBet, *args)
        d.addCallback(self.afterBet, *cargs)
        return d

    def afterBet(self, result, player, playtype, bet_amount_cents):
        if result != 0:
            return result
        self.savePotInfo(player, playtype, bet_amount_cents)
        return 0

    def onStopBet(self):
        self.onStatusChange(GameStatus.EGS_DISTACH_CARD)
        print datetime.now()
        self.sendCardReminder(1)

    def onCloseRound(self):
        self.isReckonRunning = True
        self.onStatusChange(GameStatus.EGS_CLOSEGAME)
        # update bead road info
        self.beadRoad = BeadRoad(self.roundCode)
        self.beadRoad.playerPoint = self.playerPoint
        self.beadRoad.bankerPoint = self.bankerPoint
        self.beadRoad.cardNum = self.cardNum
        self.beadRoad.pair = self.pair
        self.beadRoads[self.roundCode] = self.beadRoad
        d = ThreadController().deferedCall(self.onReckonRound, self.roundCode, self.playerPoint, self.bankerPoint,
                                self.getBankerCards() + ":" + self.getPlayerCards(),
                                self.cardNum, self.pair)
        d.addCallback(self.sendReckonResult, self.roundCode)
        d.addErrback(self.sendReckonError)
        return d

    def onReckonRound(self, roundCode, playerpoint, bankerpoint, cards, cardnum, pair):
        result = dbRound.updateRoundClosing(roundCode, playerpoint, bankerpoint, cards, cardnum, pair)
        assert result == 0, 'DB error when updating round %s' % roundCode
        # 结算 0x00020021
        result = procedures.callOnReckon(roundCode)
        assert result == 0, 'DB error when calling reckon round %s' % roundCode
        result = dbOrders.getReckonResult(roundCode)
        return result

    def sendReckonError(self, failure):
        self.isReckonRunning = False
        print failure.value
        if "DB error" in str(failure.value):
            return 1003
        else:
            return 1000

    def onCancelResult(self):
        self.onStatusChange(GameStatus.EGS_DISTACH_CARD)
        self.cardIndex = 0
        self.bankerCards = []
        self.playerCards = []
        self.bankerPoint = 0
        self.playerPoint = 0
        return {"code": 0}

    def onNewCard(self, cardIndex, cardValue):  # card Index order : 1, 2, 4, 5, 3, 6
        # print "onNewCard", cardIndex, cardValue
        if self.gameStatus != GameStatus.EGS_DISTACH_CARD:
            return {"code": 1009}
        if cardIndex != self.cardIndex:
            return {"code": 1016}
        if cardIndex > 6:
            return {"code": 1016}
        if not isValidCard(cardValue):
            return {"code": 1008}
        # if computeCardNum(self.playerCards, self.bankerCards) >= 4 and self.checkGameOver():
        #     return {"code": 1009}
        # self.cardIndex = cardIndex
        if cardIndex <= 3:
            self.playerCards.append(cardValue)
            self.playerPoint = computeCardsPoint(self.playerCards)
        else:
            self.bankerCards.append(cardValue)
            self.bankerPoint = computeCardsPoint(self.bankerCards)
        # Update card info for clients
        # 發牌 0x0002002e
        for gametable in self.tables:
            gametable.sendCardInfo()
        response = {"code": 0}
        return response

    def onCheckNextCard(self, cardIndex):
        if cardIndex >= 5 or cardIndex == 3:
            if self.checkGameOver():
                print "game over"
                # self.beadRoad
                self.sendResultConfirm()
        else:
            if cardIndex == 1:
                self.sendCardReminder(2)
            elif cardIndex == 2:
                self.sendCardReminder(4)
            elif cardIndex == 4:
                self.sendCardReminder(5)

    def onStatusChange(self, status):
        with self.mutex:
            self.gameStatus = status
            # 視頻狀態更新 0x0002002d
            self.sendVideoStatus("status")

    def checkGameOver(self):
        if self.cardIndex == 5:
            if self.playerPoint in [6, 7, 8, 9]:
                if self.bankerPoint in [7, 8, 9]:
                    return True
                # request the 6th card, the last card of banker
                self.sendCardReminder(6)
                return False
            else:
                # request the 3rd card, 3 is the last card of player
                self.sendCardReminder(3)
                return False

        elif self.cardIndex == 3:  # 3 is the last card of player
            playerThirdCard = getConvertedCardValue(self.playerCards[3-1])
            if (self.bankerPoint == 6 and playerThirdCard in [6, 7]) \
                    or (self.bankerPoint == 5 and playerThirdCard not in [0, 1, 8, 9]) \
                    or (self.bankerPoint == 4 and playerThirdCard not in [0, 1, 2, 3, 8, 9]) \
                    or (self.bankerPoint == 3 and playerThirdCard != 8):
                self.sendCardReminder(6)  # request the 6th card, the last card of banker
                return False
            return True
        elif self.cardIndex == 6:
            return True
        return False

    def sendReckonResult(self, result, roundCode):
        # Split the reckon result by player and by tables
        players = {}
        tables = {}
        for row in result:
            tableid = row.get("tableid")
            loginname = row.get("loginname")
            win_amount_cents = row.get("win_amount_cents")
            bet_amount_cents = row.get("bet_amount_cents")
            playtype = row.get("playtype")
            result = {"win_amount_cents":win_amount_cents, "bet_amount_cents":bet_amount_cents, "playtype":playtype}
            # Split by loginname
            if not players.has_key(loginname):
                players[loginname] = []
            players[loginname].append(result)
            # Split by tableid
            if not tables.has_key(tableid):
                tables[tableid] = {}
            tableplayers = tables.get(tableid)
            if not tableplayers.has_key(loginname):
                tableplayers[loginname] = []
            tableplayers[loginname].append(result)
        # Send reckon result to the player only if he is online
        for loginname, reckon_dict in players.items():
            player = PlayersManager().getPlayerByLoginname(loginname)
            if player:
                player.sendReckon(self.videoID, roundCode, reckon_dict)
        # Send reckon result to all other players in the table, even if this player has left or offline
        for tableid, tableplayers_dict in tables.items():
            gametable = GameDataManager.GameDataManager().getTableByID(tableid)
            if gametable:
                gametable.sendReckonByTable(tableplayers_dict)
        self.isReckonRunning = False
        return 0

    def sendResultConfirm(self): # to dealer

        self.pair = computePair(self.bankerCards, self.playerCards)
        self.cardNum = computeCardNum(self.bankerCards, self.playerCards)

        # send result to dealer for confirming
        result = dealer_pb2.gameResultResponse()
        result.roundCode = self.roundCode
        result.videoId = self.videoID
        result.bankerPoint = self.bankerPoint
        result.playerPoint = self.playerPoint
        result.pair = self.pair
        result.cardNum = self.cardNum
        gatepush(40007, result.SerializeToString(), [self.dealer.dynamicId])

        # send payout confirm to dealer

    def sendCardReminder(self, cardIndex): #提示发牌
        # show reminder in screen or ?
        self.cardIndex = cardIndex
        request = dealer_pb2.senderCardReminderRequest()
        request.roundCode = self.roundCode
        request.cardIndex = cardIndex
        gatepush(40004, request.SerializeToString(), [self.dealer.dynamicId])

    def sendVideoStatus(self, field_updated):
        # 視頻狀態更新 0x0002002d
        response = self.getVideoStatusUpdateResponse(field_updated)
        gatepush(20045, response.SerializeToString(), PlayersManager().getAllPlayerIDs())

    def getGameStatus(self):
        return self.gameStatus.value

    def getBankerCards(self):
        return ",".join([str(i) for i in self.bankerCards])

    def getPlayerCards(self):
        return ",".join([str(i) for i in self.playerCards])

    def getBetTime(self):
        if self.gameStatus == GameStatus.EGS_BETTING:
            time_diff = self.betTime - (datetime.now() - self.startTime).seconds
            return max(0, time_diff)
        else:
            return 0

    def getDealerName(self):
        if self.dealer:
            return self.dealer.name
        else:
            return ""

    def getVideoStatusData(self):
        data = {}
        # 視頻狀態列表 0x00020023
        data['videoid'] = self.videoID
        data['dealername'] = self.getDealerName()
        data['gametype'] = self.gameType
        data['roundcode'] = self.roundCode
        data['status'] = self.getGameStatus()
        data['bettime'] = self.getBetTime()
        return data

    def getVideoStatusResponse(self):
        # 視頻狀態 0x00020023
        response = game_pb2.videoStatusResponse()
        response.videoid = self.videoID
        response.dealername = self.getDealerName()
        response.gametype = self.gameType
        response.roundcode = self.roundCode
        response.status = self.getGameStatus()
        response.bettime = self.getBetTime()
        response.bankercards = self.getBankerCards()
        response.playercards = self.getPlayerCards()
        return response

    def getVideoStatusUpdateResponse(self, field_updated):
        # 視頻狀態更新 0x0002002d
        response = game_pb2.videoStatusUpdateResponse()
        response.videoid = self.videoID
        response.gametype = self.gameType
        response.roundcode = self.roundCode
        if field_updated == "status":
            response.status = self.getGameStatus()
            response.bettime = self.getBetTime()
            if self.gameStatus == GameStatus.EGS_CLOSEGAME:
                response.bankerpoint = self.bankerPoint
                response.playerpoint = self.playerPoint
                response.pair = self.pair
                response.cardnum = self.cardNum
        elif field_updated == "dealer":
            response.dealername = self.getDealerName()
        return response

    def getCardInfoResponse(self):
        # 發牌 0x0002002e
        response = game_pb2.cardInfoResponse()
        response.videoid = self.videoID
        response.bankercards = self.getBankerCards()
        response.playercards = self.getPlayerCards()
        return response

    def getDealerVideoStatusResponse(self):
        # 视频状态 0x00040006
        response = dealer_pb2.videoStatusResponse()
        response.videoid = self.videoID
        response.gametype = self.gameType
        response.roundcode = self.roundCode
        response.status = self.getGameStatus()
        response.bankercards = self.getBankerCards()
        response.playercards = self.getPlayerCards()
        response.bettime = self.getBetTime()
        return response

    def getVideoPlayersResponse(self):
        response = player_pb2.videoPlayersResponse()
        response.code = 0
        response.videoid = self.videoID
        count = 0
        for gametable in self.tables:
            response_item = response.tables.add()
            response_item.tableid = gametable.tableID
            count2 = 0
            for player in gametable.getAllPlayers():
                response_item_sub = response_item.seats.add()
                response_item_sub.loginname = player.loginname
                response_item_sub.nickname = player.nickname
                response_item_sub.seat = player.seat
                count2 += 1
            response_item.num = count2
            count += 1
        response.num = count
        return response

    def getVideoPot(self, playtype = None):
        if playtype:
            return self.videopot.get(playtype, [])
        else:
            return self.videopot

    def getVideoMainPotData(self):
        data = {}
        bankerpot = self.getVideoPot(PlayType.BANKER.value)
        playerpot = self.getVideoPot(PlayType.PLAYER.value)
        tiepot = self.getVideoPot(PlayType.TIE.value)
        # 大厅主玩法彩池信息 0x0002002c
        data['videoid'] = self.videoID
        data['player_num'] = sum([gametable.getPlayerNum() for gametable in self.tables])
        data['banker_bet_number'] = len(bankerpot)
        data['player_bet_number'] = len(playerpot)
        data['tie_bet_number'] = len(tiepot)
        data['banker_bet_amount_cents'] = sum([pot.get('amount_cents', 0) for pot in bankerpot])
        data['player_bet_amount_cents'] = sum([pot.get('amount_cents', 0) for pot in playerpot])
        data['tie_bet_amount_cents'] = sum([pot.get('amount_cents', 0) for pot in tiepot])
        return data

    def savePotInfo(self, player, playtype, amount_cents):
        # 儲存視頻下注信息
        if not self.videopot.has_key(playtype):
            self.videopot[playtype] = []
        player_data = {
            "loginname": player.loginname,
            "nickname": player.nickname,
            "tableid": player.tableid,
            "seat": player.seat,
        }
        pot = {"player_data":player_data, "amount_cents":amount_cents}
        self.videopot[playtype].append(pot)

def calcRoundSeq(str):
    letters = string.digits + string.ascii_uppercase
    for i in range(len(str) - 1, -1, -1):
        if str[i] == 'Z':
            str = str[:i] + letters[0] + str[i+1:]
            continue
        else:
            str = str[:i] + letters[letters.index(str[i]) + 1] + str[i+1:]
            break
    return str
