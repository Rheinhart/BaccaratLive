#coding=utf8
__author__ = 'joshua'

from thread import allocate_lock

from app.game.memmode import tb_table_admin
import GameDataManager
from app.share.protofile import game_pb2, player_pb2
from app.game.gatenodeservice import gatepush
from app.game.appinterface.limit import GetTableLimitInfo
from twisted.internet import defer


class GameTable:

    def __init__(self, data):

        self.tableID = data.get("tableid", "")
        self.videoID = data.get("videoid", "")
        self.gameType = ""
        self.limitID = data.get("limitid", "")
        self.flag = 0
        self.seats = 8
        self.seat = [None for i in range(0, self.seats)]
        self.tablepot = {}
        self.mutex = allocate_lock()

        mdata = tb_table_admin.getObjData(self.tableID)
        if mdata:
            print "table", mdata
            self.initWitMemode(mdata)
        else:
            tb_table_admin.new(data)
            print "new a table in memcache", tb_table_admin.getObjData(self.videoID)

        self.seat = [None for i in range(0, self.seats)]

    def initWitMemode(self, data):
        self.tableID = data['tableid']
        self.videoID = data['videoid']
        self.gameType = data['gametype']
        self.limitID = data['limitid']
        self.flag = data['flag']
        self.seats = data['seats']

    def setVideo(self):
        self.gamevideo = GameDataManager.GameDataManager().getVideoByID(self.videoID)

    def onEnterPlayer(self, player, seat):
        with self.mutex:
            if seat >= len(self.seat) or seat < 0:
                return -1
            if self.seat[seat] == None:
                self.seat[seat] = player
                return seat
            elif self.seat[seat] == player:
                return seat
            else:
                return -1

    def onExitPlayer(self, player):
        with self.mutex:
            if player in self.seat:
                self.seat[self.seat.index(player)] = None


    def onStartGame(self):
        # 清除桌子下注信息
        self.tablepot = {}
        for player in self.getAllPlayers():
            player.onStartGame()

    def onPlayerBet(self, player, roundcode, playtype, bet_amount_cents):
        limit = GetTableLimitInfo(self.limitID, playtype)
        if limit:
            min_cents = limit.get("min_cents", 0)
            max_cents = limit.get("max_cents", 0)
        else:
            min_cents = 0
            max_cents = 0
        if bet_amount_cents < min_cents:
            return 1014 # 不足桌台限紅
        if bet_amount_cents + self.getPlayerBetAmount(player.loginname, playtype) > max_cents:
            return 1012 # 超過桌台限紅
        result = self.gamevideo.onPlayerBet(player, roundcode, playtype, bet_amount_cents)
        if isinstance(result, defer.Deferred):
            result.addCallback(self.afterBet, player, playtype, bet_amount_cents)
            return result
        else:
            return self.afterBet(result, player, playtype, bet_amount_cents)

    def afterBet(self, result, player, playtype, bet_amount_cents):
        if result != 0:
            return result
        self.savePotInfo(player, playtype, bet_amount_cents)
        return 0

    def onPotDetails(self, playtype):
        # 实时彩池明细 0x0002001e
        return self.gamevideo.getVideoPot(playtype)

    def sendGameResult(self):
        # 游戏结果 0x00020020
        response = game_pb2.gameResultResponse()
        response.videoid = self.gamevideo.videoID
        response.roundcode = self.gamevideo.roundCode
        response.bankerpoint = self.gamevideo.bankerPoint
        response.playerpoint = self.gamevideo.playerPoint
        response.pair = self.gamevideo.pair
        response.cardnum = self.gamevideo.cardNum
        gatepush(20032, response.SerializeToString(), self.getAllPlayerIDs())

    def sendCardInfo(self):
        # 發牌 0x0002002e
        response = self.gamevideo.getCardInfoResponse()
        gatepush(20046, response.SerializeToString(), self.getAllPlayerIDs())

    def sendPot(self):
        # 实时彩池 0x0002001d
        response = self.getPotReponse()
        gatepush(20029, response.SerializeToString(), self.getAllPlayerIDs())

    def sendTableBet(self):
        # 桌子已下注信息 0x00020027
        response = self.getTableBetResponse()
        gatepush(20039, response.SerializeToString(), self.getAllPlayerIDs())

    def sendBetByTable(self, seat, playtype, amount_cents):
        # 同桌其他人下注 0x00020022
        response = game_pb2.otherPlayersBetResponse()
        response.tableid = self.tableID
        response.seat = seat
        response.playtype = playtype
        response.amount_cents = amount_cents
        gatepush(20034, response.SerializeToString(), self.getAllPlayerIDs())

    def sendReckonByTable(self, tableplayers_dict):
        # 同桌其他人结算 0x00020028
        response = game_pb2.otherPlayersReckonResponse()
        response.tableid = self.tableID
        response.num = len(tableplayers_dict)
        for loginname, reckon_dict in tableplayers_dict.items():
            response_item = response.details.add()
            response_item.loginname = loginname
            response_item.num = len(reckon_dict)
            for reckon_item in reckon_dict:
                details_item = response_item.details.add()
                details_item.playtype = reckon_item.get('playtype')
                details_item.win_cents = reckon_item.get('win_amount_cents')
        gatepush(20040, response.SerializeToString(), self.getAllPlayerIDs())

    # def sendNewShoe(self):
    #     # 新建靴 0x0002002b
    #     response = game_pb2.newShowResponse()
    #     response.videoid = self.videoID
    #     gatepush(20043, response.SerializeToString(), self.getAllPlayerIDs())

    def getAllPlayers(self):
        return [player for player in self.seat if player is not None]

    def getAllPlayerIDs(self):
        return [player.dynamicId for player in self.seat if player is not None and player.connected]

    def getPlayerNum(self):
        return len([player for player in self.seat if player is not None])

    def savePotInfo(self, player, playtype, amount_cents):
        # 儲存桌子下注信息
        if not self.tablepot.has_key(playtype):
            self.tablepot[playtype] = {}
        if not self.tablepot[playtype].has_key(player.loginname):
            self.tablepot[playtype][player.loginname] = []
        player_data = {
            "loginname": player.loginname,
            "nickname": player.nickname,
            "tableid": player.tableid,
            "seat": player.seat,
        }
        pot = {"player_data":player_data, "amount_cents":amount_cents}
        self.tablepot[playtype][player.loginname].append(pot)

    def getPotReponse(self):
        videopot = self.gamevideo.getVideoPot()
        # 实时彩池 0x0002001d
        response = game_pb2.potResponse()
        response.videoid = self.videoID
        response.num = len(videopot)
        for playtype, pots in videopot.items():
            response_item = response.details.add()
            response_item.playtype = playtype
            response_item.amount_cents = sum([pot.get("amount_cents") for pot in pots])
            response_item.num = len(pots)
        return response

    def getTableBetResponse(self):
        # 桌子已下注信息 0x00020027
        count = 0
        response = game_pb2.tableBetResponse()
        response.tableid = self.tableID
        for playtype, player_pots in self.tablepot.items():
            for loginanme, pots in player_pots.items():
                count += len(pots)
                for pot in pots:
                    response_item = response.details.add()
                    response_item.playtype = playtype
                    response_item.amount_cents = pot.get("amount_cents")
                    player_data = pot.get("player_data")
                    response_item.loginname = player_data.get("loginname")
                    response_item.nickname = player_data.get("nickname")
                    response_item.seat = player_data.get("seat")
        response.num = count
        return response

    def getTablePlayersResponse(self):
        response = player_pb2.tablePlayersResponse()
        response.tableid = self.tableID
        count = 0
        for player in self.getAllPlayers():
            response_item = response.seats.add()
            response_item.loginname = player.loginname
            response_item.nickname = player.nickname
            response_item.seat = player.seat
            count += 1
        response.num = count
        return response

    def getPlayerBetAmount(self, loginname, playtype):
        if self.tablepot.has_key(playtype):
            if self.tablepot[playtype].has_key(loginname):
                return sum([pot.get('amount_cents', 0) for pot in self.tablepot[playtype][loginname]])
        return 0