#coding=utf8
__author__ = 'joshua'

from app.game.memmode import tb_table_admin, tb_video_admin
from app.game.core.GameVideo import GameVideo
from app.game.core.GameTable import GameTable
from firefly.utils.singleton import Singleton

class GameDataManager:

    __metaclass__ = Singleton

    def __init__(self):
        self._tables = {}
        self._videos = {}

    def getAllTables(self):
        return self._tables.values()

    def getAllVideos(self):
        return self._videos.values()

    def getAllVideosInDict(self):
        return self._videos

    def getTablesByVideoID(self, videoID):
        tables = []
        tidlist = tb_table_admin.getAllPkByFk(videoID)
        for tid in tidlist:
            data = {}
            data["tableid"] = tid
            table = GameTable(data)
            tables.append(table)
            self._tables[table.tableID] = table
        return tables

    def getTableByID(self, tableid):
        return self._tables.get(tableid)

    def getVideoByID(self, vid):
        return self._videos.get(vid)

    def onAddVideo(self, data):
        print "onAddVideo", data
        if data['videoid'] in self._videos.keys():
            print "duplicate video id"
            return {'result': False, 'message': u'duplicate video id', 'data': None}
        else:
            video = GameVideo(data)
            self._videos[video.videoID] = video
            return {'result': True, 'message': u'Success', 'data': video}

    def onDelVideo(self, data):
        if data['videoid'] in self._videos.keys():
            self._videos.__delitem__(data['videoid'])
            tb_video_admin.deleteMode(data['videoid'])
            return {'result': True, 'message': u'Success', 'data': None}
        else:
            return {'result': True, 'message': u'Video Not Found', 'data': None}

    def onAddTable(self, data):
        table = GameTable(data)
        self._tables[table.tableID] = table
        if table not in self._videos.get(table.videoID).tables:  #fix
            self._videos[table.tableID] = table #fix
        return table

    def onDelTable(self, tid):
        if tid in self._tables:
            table = self._tables.get(tid)
            if table in self._videos.get(table.videoID).tables:
                self._videos.get(table.videoID).tables.__delitem__(tid)
            self._tables.__delitem__(tid)
            tb_table_admin.deleteMode(tid)

    def initData(self):
        self._videos = {}
        vkeylist = tb_video_admin.getAllPkByFk(0)
        for vid in vkeylist:
            data = {}
            data["videoid"] = vid
            video = GameVideo(data)
            self._videos[video.videoID] = video

        for table in self.getAllTables():
            table.setVideo()

