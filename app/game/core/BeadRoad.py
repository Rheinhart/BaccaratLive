#coding=utf8
__author__ = 'joshua'


class BeadRoad:

    def __init__(self, roundCode, data=None):
        self.roundCode = roundCode
        self.bankerPoint = data.get("bankerpoint", 0) if data else None
        self.playerPoint = data.get("playerpoint", 0) if data else None
        self.pair = data.get("pair", 0) if data else None
        self.cardNum = data.get("cardnum", 0) if data else None