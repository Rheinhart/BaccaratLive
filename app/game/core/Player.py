#coding:utf8

# __author__ = 'Alan'
from app.share.protofile import game_pb2, player_pb2
from app.game.gatenodeservice import gatepush
from app.game.appinterface.limit import GetPersonalLimitInfo
import GameDataManager
from twisted.internet import defer

class Player:
    def __init__(self, player_data, dynamicId=-1):
        self.loginname = player_data['loginname']
        self.nickname = player_data['nickname']
        self.limitid = player_data['limitid']
        self.credit_cents = player_data['credit_cents']
        self.ip = player_data['ip']
        self.dynamicId = dynamicId
        self.gametable = None
        self.tableid = ""
        self.seat = -1
        self.nobetrounds = 0
        self.bets = {}
        self.lastHeartBeat = None
        self.connected = True

    def onEnterTable(self, tableid, seat):
        gametable = GameDataManager.GameDataManager().getTableByID(tableid)
        if gametable == None:
            return 1002 # 參數錯誤
        myseat = gametable.onEnterPlayer(self, seat)
        if myseat == -1:
            return 1002 # 參數錯誤
        self.tableid = tableid
        self.gametable = gametable
        self.seat = myseat
        self.nobetrounds = 0
        self.bets = {}
        return 0

    def onExitTable(self):
        if self.gametable:
            self.gametable.onExitPlayer(self)
            self.gametable = None
            self.tableid = ""
            self.seat = -1
            self.nobetrounds = 0
            self.bets = {}
        return 0

    def onStartGame(self):
        self.bets = {}
        # 累加玩家不下注局數
        self.nobetrounds += 1
        # 三局不下提示 0x00020026
        if self.nobetrounds >= 3:
            self.sendNoBetAlert()

    def onBet(self, roundcode, playtype, bet_amount_cents):
        if not self.checkIfEnoughCredit(bet_amount_cents):
            return 1011 # 餘額不足
        print self.limitid, playtype
        limit = GetPersonalLimitInfo(self.limitid, playtype)
        if limit:
            min_cents = limit.get("min_cents", 0)
            max_cents = limit.get("max_cents", 0)
        else:
            min_cents = 0
            max_cents = 0
        if bet_amount_cents < min_cents:
            return 1015 # 不足個人限紅
        if bet_amount_cents +  self.bets.get(playtype, 0) > max_cents:
            return 1013 # 超過個人限紅
        if not self.gametable: # 用户没有进桌
            return 1002
        result = self.gametable.onPlayerBet(self, roundcode, playtype, bet_amount_cents)
        if isinstance(result, defer.Deferred):
            result.addCallback(self.afterBet, playtype, bet_amount_cents)
            return result
        else:
            return self.afterBet(result, playtype, bet_amount_cents)

    def afterBet(self, result, playtype, bet_amount_cents):
        if result != 0:
            return result
        self.nobetrounds = 0
        if self.bets.has_key(playtype):
            self.bets[playtype] += bet_amount_cents
        else:
            self.bets[playtype] = bet_amount_cents
        # self.updateTrans(roundcode, playtype, bet_amount_cents)
        self.credit_cents -= bet_amount_cents
        return 0

    def sendReckon(self, videoid, roundcode, reckon_dict):
        if not self.connected:
            return
        # 结算 0x00020021
        totalwin_cents = 0
        response = game_pb2.reckonResponse()
        response.videoid = videoid
        response.roundcode = roundcode
        for reckon_item in reckon_dict:
            response_item = response.details.add()
            response_item.playtype = reckon_item.get('playtype')
            response_item.win_cents = reckon_item.get('win_amount_cents')
            totalwin_cents += reckon_item.get('win_amount_cents')
            self.credit_cents += reckon_item.get('win_amount_cents') + reckon_item.get('bet_amount_cents')
        response.totalwin_cents = totalwin_cents
        #response.left_cents = self.credit_cents
        response.num = len(reckon_dict)
        gatepush(20033, response.SerializeToString(), [self.dynamicId])
        # 玩家额度更新 0x00020018
        response = player_pb2.playerCreditResponse()
        response.credit_cents = self.credit_cents
        gatepush(20024, response.SerializeToString(), [self.dynamicId])

    def sendNoBetAlert(self):
        if not self.connected:
            return
        # 三局不下提示 0x00020026
        response = game_pb2.noBetResponse()
        response.roundcount = self.nobetrounds
        gatepush(20038, response.SerializeToString(), [self.dynamicId])

    def checkIfEnoughCredit(self, bet_amount_cents):
        """ bet_amount_cents: 下注前檢查是否有足夠餘額 """
        return self.credit_cents - bet_amount_cents >= 0

    def __str__(self):
        output = ""
        for keyname in self.__dict__.keys():
            if not keyname.startswith('_'):
                output += keyname + "=" + str(self.__dict__.get(keyname)) + "\n"
        return output

    def test(self):
        self.gametable.gamevideo.test()

    def onAutoEnterTable(self, video_id):
        for gametable in GameDataManager.GameDataManager().getVideoByID(video_id).tables:
            for index, seat in enumerate(gametable.seat):
                if not seat:
                    myseat = gametable.onEnterPlayer(self, index)
                    if myseat == -1:
                        continue
                    self.tableid = gametable.tableID
                    self.gametable = gametable
                    self.seat = myseat
                    self.nobetrounds = 0
                    return 0
        return 1002 # 參數錯誤
