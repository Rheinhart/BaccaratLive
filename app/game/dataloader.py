#coding:utf8
'''
Created on 2013-8-14

@author: lan (www.9miao.com)
'''
# from app.share.dbopear import dbItems,dbMonster,dbExperience,dbSkill,dbProfession,dbZhanyi,dbDropout,dbShieldWord,dbCharacterPet
import memmode
from firefly.dbentrust.madminanager import MAdminManager
from app.game.core.GameDataManager import GameDataManager
from app.share.dbopear import dbLimit


def load_config_data():
    """从数据库中读取配置信息
    """
    GameDataManager().initData()
    
def registe_madmin():
    """注册数据库与memcached对应
    """
    MAdminManager().registe(memmode.tb_video_admin)
    MAdminManager().registe(memmode.tb_table_admin)