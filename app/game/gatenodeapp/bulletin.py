#coding=utf8
__author__ = 'joshua'
import datetime

from app.share.protofile import bulletin_pb2
from app.share.dbopear import dbBulletin
from app.game.gatenodeservice import remoteserviceHandle
from app.game.gatenodeservice import gatepush
from app.game.core import PlayersManager


@remoteserviceHandle
def GetLastBulletinInfo_20037():
    """返回最后一条有效公告"""
    print "GetAllBulletinInfo_20037"
    bulletins = dbBulletin.getLastValidBulletin()
    response = bulletin_pb2.bulletinResponse()
    if len(bulletins) > 0:
        one = bulletins[0]
        # for one in bulletins:
        response.beginTime = one.get('create_time', datetime.datetime.now).strftime("%Y-%m-%d %H:%M:%S")
        response.endTime = one.get('expired_time', datetime.datetime.now).strftime("%Y-%m-%d %H:%M:%S")
        response.text = one.get('text', "")
        return response.SerializeToString()
    else:
        return None

@remoteserviceHandle
def GetLastBulletinInfo_50013(data):
    """返回最后一条有效公告给所有大厅用户"""
    print "GetAllBulletinInfo_50013"
    bulletins = dbBulletin.getLastValidBulletin()
    response = bulletin_pb2.bulletinResponse()
    if len(bulletins) > 0:
        one = bulletins[0]
        # for one in bulletins:
        response.beginTime = one.get('create_time', datetime.datetime.now).strftime("%Y-%m-%d %H:%M:%S")
        response.endTime = one.get('expired_time', datetime.datetime.now).strftime("%Y-%m-%d %H:%M:%S")
        response.text = one.get('text', "")
        LobbyPlayerIDs = PlayersManager.PlayersManager().getAllLobbyPlayerIDs()
        gatepush(50013, response.SerializeToString(), LobbyPlayerIDs)
        return response.SerializeToString()
    else:
        return None