#coding=utf8

# __author__ = 'Alan'

from datetime import datetime

from app.game.gatenodeservice import remoteserviceHandle, gatepush
from app.game.core.PlayersManager import PlayersManager
from app.share.protofile import player_pb2, game_pb2, login_pb2
from app.game.core.GameDataManager import GameDataManager


@remoteserviceHandle
def enterGame_20017(dynamicId, player_data):
    print "enterGame_20017"
    loginname = player_data.get("loginname")
    isExisting = PlayersManager().isPlayerOnline(loginname)
    response = login_pb2.loginGameResultResponse()
    if not isExisting:
        PlayersManager().addPlayer(player_data, dynamicId)
        response.code = 0
        gatepush(20017, response.SerializeToString(), [dynamicId])
    else:
        player = PlayersManager().updatePlayer(loginname, dynamicId)
        response.code = 0
        gatepush(20017, response.SerializeToString(), [dynamicId])
        # 玩家最後位置 0x00020031
        response = player_pb2.lastPositionResponse()
        response.tableid = player.tableid
        response.seat = player.seat
        gatepush(20049, response.SerializeToString(), [dynamicId])

    dynamicIDs = [dynamicId]
    # print "getLobbyPlayerDynamicIDList", len(dynamicIDs)
    # 大厅主玩法彩池信息 0x0002002c
    response = game_pb2.lobbyPotResponse()
    videos = GameDataManager().getAllVideos()
    response.num = len(videos)
    for gamevideo in videos:
        data = gamevideo.getVideoMainPotData()
        response_item = response.details.add()
        response_item.videoid = data.get('videoid')
        response_item.player_num = data.get('player_num')
        response_item.banker_bet_number = data.get('banker_bet_number')
        response_item.player_bet_number = data.get('player_bet_number')
        response_item.tie_bet_number = data.get('tie_bet_number')
        response_item.banker_bet_amount_cents = data.get('banker_bet_amount_cents')
        response_item.player_bet_amount_cents = data.get('player_bet_amount_cents')
        response_item.tie_bet_amount_cents = data.get('tie_bet_amount_cents')
    gatepush(20044, response.SerializeToString(), dynamicIDs)


@remoteserviceHandle
def playerList_10021(dynamicId):
    print "playerList_10021"
    # 在线玩家列表 0x00020015
    response = player_pb2.playerListResponse()
    players = PlayersManager().getAllPlayers()
    response.num = len(players)
    for p in players:
        response_item = response.player.add()
        response_item.loginname = p.loginname
        response_item.nickname = p.nickname
    gatepush(20021, response.SerializeToString(), [dynamicId])

@remoteserviceHandle
def getVideoStatusList_20047(dynamicId):
    print "getVideoStatusList_20047"
    videos = GameDataManager().getAllVideos()
    # 视频状态列表 0x0002002f
    response = game_pb2.videoStatusListResponse()
    response.num = len(videos)
    for gamevideo in videos:
        data = gamevideo.getVideoStatusData()
        response_item = response.details.add()
        response_item.videoid = data.get('videoid')
        response_item.dealername = data.get('dealername')
        response_item.gametype = data.get('gametype')
        response_item.roundcode = data.get('roundcode')
        response_item.status = data.get('status')
        response_item.bettime = data.get('bettime')
    return response.SerializeToString()

@remoteserviceHandle
def enterTable_10025(dynamicId, request_proto):
    """ 进桌 0x00010019 """
    print "enterTable_10025", dynamicId
    request = player_pb2.enterTableRequest()
    request.ParseFromString(request_proto)
    tableid = request.tableid
    seat = request.seat
    # Call player
    player = PlayersManager().getPlayerByID(dynamicId)
    gametable = player.gametable
    if gametable is not None:
        response = playerSeatUpdate(player, False)
        tableplayers = gametable.getAllPlayerIDs()
        player.onExitTable()
        tableplayers.remove(dynamicId)
        # 玩家位置更新 0x00020017
        gatepush(20023, response.SerializeToString(), tableplayers)
    result = player.onEnterTable(tableid, seat)
    # 进桌回包 0x00020019
    response = player_pb2.enterTableResponse()
    response.code = result
    gatepush(20025, response.SerializeToString(), [dynamicId])
    if result != 0:
        return
    # 桌台玩家列表 0x00020033
    response = player.gametable.getTablePlayersResponse()
    gatepush(20051, response.SerializeToString(), [dynamicId])
    # 玩家额度更新 0x00020018
    response = player_pb2.playerCreditResponse()
    response.credit_cents = player.credit_cents
    gatepush(20024, response.SerializeToString(), [dynamicId])
    # 視頻狀態 0x00020023
    response = player.gametable.gamevideo.getVideoStatusResponse()
    gatepush(20035, response.SerializeToString(), [dynamicId])
    # 实时彩池 0x0002001d
    response = player.gametable.getPotReponse()
    gatepush(20029, response.SerializeToString(), [dynamicId])
    # 桌子已下注信息 0x00020027
    response = player.gametable.getTableBetResponse()
    gatepush(20039, response.SerializeToString(), [dynamicId])
    # 玩家位置更新 0x00020017
    response = playerSeatUpdate(player, True)
    tableplayers = player.gametable.getAllPlayerIDs()
    tableplayers.remove(dynamicId)
    gatepush(20023, response.SerializeToString(), tableplayers)
    return

@remoteserviceHandle
def exitTable_10026(dynamicId, request_proto):
    """ 离桌 0x0001001a """
    print "exitTable_10026", dynamicId, request_proto
    # 玩家位置更新 0x00020017
    player = PlayersManager().getPlayerByID(dynamicId)
    gametable = player.gametable
    if gametable is None:
        return
    response = playerSeatUpdate(player, False)
    tableplayers = gametable.getAllPlayerIDs()
    player.onExitTable()
    # 离桌 回包 0x0002001a
    gatepush(20026, "", [dynamicId])
    tableplayers.remove(dynamicId)
    gatepush(20023, response.SerializeToString(), tableplayers)
    return

@remoteserviceHandle
def onAutoEnterTable_10048(dynamicId, request_proto):
    """ 自动进桌  0x00010030 """
    print "onAutoEnterTable_10048", dynamicId
    request = player_pb2.autoEnterTableRequest()
    request.ParseFromString(request_proto)
    # Call player
    player = PlayersManager().getPlayerByID(dynamicId)
    gametable = player.gametable
    if gametable is not None:
        response = playerSeatUpdate(player, False)
        tableplayers = gametable.getAllPlayerIDs()
        player.onExitTable()
        tableplayers.remove(dynamicId)
        # 玩家位置更新 0x00020017
        gatepush(20023, response.SerializeToString(), tableplayers)
    result = player.onAutoEnterTable(request.videoid)
    # 进桌回包 0x00020019
    response = player_pb2.enterTableResponse()
    response.code = result
    gatepush(20025, response.SerializeToString(), [dynamicId])
    if result != 0:
        return
    # 桌台玩家列表 0x00020033
    response = player.gametable.getTablePlayersResponse()
    gatepush(20051, response.SerializeToString(), [dynamicId])
    # 玩家额度更新 0x00020018
    response = player_pb2.playerCreditResponse()
    response.credit_cents = player.credit_cents
    gatepush(20024, response.SerializeToString(), [dynamicId])
    # 視頻狀態 0x00020023
    response = player.gametable.gamevideo.getVideoStatusResponse()
    gatepush(20035, response.SerializeToString(), [dynamicId])
    # 实时彩池 0x0002001d
    response = player.gametable.getPotReponse()
    gatepush(20029, response.SerializeToString(), [dynamicId])
    # 桌子已下注信息 0x00020027
    response = player.gametable.getTableBetResponse()
    gatepush(20039, response.SerializeToString(), [dynamicId])
    # 玩家位置更新 0x00020017
    response = playerSeatUpdate(player, True)
    tableplayers = player.gametable.getAllPlayerIDs()
    tableplayers.remove(dynamicId)
    gatepush(20023, response.SerializeToString(), tableplayers)
    return

@remoteserviceHandle
def getVideoPlayers_10050(dynamicId, request_proto):
    # 視频座位列表 0x00010032
    print "getVideoSeats_10050", dynamicId
    request = player_pb2.videoPlayersRequest()
    request.ParseFromString(request_proto)
    videoid = request.videoid
    gamevideo = GameDataManager().getVideoByID(videoid)
    if gamevideo:
        response = gamevideo.getVideoPlayersResponse()
    else:
        response = player_pb2.videoPlayersResponse()
        response.code = 1002
    gatepush(20050, response.SerializeToString(), [dynamicId])

@remoteserviceHandle
def getPlayerPosition_10052(dynamicId, request_proto):
    # 查詢玩家位置 0x00010034
    print "getPlayerPosition_10052", dynamicId
    request = player_pb2.playerPositionRequest()
    request.ParseFromString(request_proto)
    loginname = request.loginname
    player = PlayersManager().getPlayerByLoginname(loginname)
    response = player_pb2.playerPositionResponse()
    if player:
        response.code = 0
        response.loginname = loginname
        response.tableid = player.tableid
        response.seat = player.seat
    else:
        response.code = 1002
    gatepush(20052, response.SerializeToString(), [dynamicId])

def playerSeatUpdate(player, action):
    response = player_pb2.playerSeatUpdateResponse()
    response.loginname = player.loginname
    response.nickname = player.nickname
    response.tableid = player.tableid
    response.seat = player.seat
    response.action = action
    return response

@remoteserviceHandle
def heartBeat_1(dynamicId, request_proto):
    player = PlayersManager().getPlayerByID(dynamicId)
    if player:
        player.lastHeartBeat = datetime.now()
    else:
        pass