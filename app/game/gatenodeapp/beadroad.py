#coding=utf8
__author__ = 'joshua'

from app.share.protofile import getBeadRoad_pb2
from app.game.gatenodeservice import remoteserviceHandle
from app.game.core.GameDataManager import GameDataManager


@remoteserviceHandle
def getBeadRoadInfo_20020():
    """返回露珠信息"""
    print "getBeadRoadInfo_20020"

    response = getBeadRoad_pb2.getBeadRoadResponse()

    beadroads = {}
    for gamevideo in GameDataManager().getAllVideos():
        beadroads[gamevideo.videoID] = gamevideo.beadRoads

    response.num = len(beadroads.values())

    for videoBeadRoadId in beadroads.keys():
        item = response.videoBeadRoad.add()
        item.videoId = videoBeadRoadId
        item.num = len(beadroads[videoBeadRoadId])
        for beadRoad in beadroads[videoBeadRoadId].values():
            br = item.beadRoad.add()
            br.roundCode = beadRoad.roundCode
            br.bankerPoint = beadRoad.bankerPoint
            br.playerPoint = beadRoad.playerPoint
            br.pair = beadRoad.pair
            br.cardNum = beadRoad.cardNum

    return response.SerializeToString()
