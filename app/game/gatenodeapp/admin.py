#coding=utf8
__author__ = 'joshua'

from datetime import datetime

from app.game.gatenodeservice import remoteserviceHandle
from app.game.core.GameDataManager import GameDataManager
from app.share.protofile.controller import *
from app.share.dbopear import  procedures
from app.share.dbopear import dbRound
from app.game.gatenodeservice import gatepush
from app.game.core.PlayersManager import PlayersManager
from app.game.core.GameDataManager import GameDataManager
from app.game.core.GameVideo import GameStatus
from app.game.core.GameUtility import isValidCards, getCardsFromString, computePair, computeCardNum, computeCardsPoint
from twisted.internet import threads


# @remoteserviceHandle
# def operaVideo_50003(data):
#     """添加视频
#     """
#     print "operaVideo_50003", data
#     result = {}
#     if data['operation'] == 'add':
#         data.pop('operation', None)
#         result = GameDataManager().onAddVideo(data)
#     elif data['operation'] == 'del':
#         data.pop('operation', None)
#         result = GameDataManager().onDelVideo(data)
#
#     return result

# @remoteserviceHandle
# def addTable_50006(data):
#     """添加桌台
#     """
#     GameDataManager().onAddTable(data)
@remoteserviceHandle
def loginController_50001(data):
    """管理者登录
    """
    print 'Controller:%s login'%data.get('controller')
    return 60001
    #gatepush(60001, request.SerializeToString())

@remoteserviceHandle
def getVideos_50002(data):
    """查询视频列表
    """
    return GameDataManager().getAllVideosInDict()

@remoteserviceHandle
def recalcRound_50016(data):
    """重新结算局 0x00050010
    """
    print "recalcRound_50016"
    roundCode = data.get("roundcode")
    print roundCode
    if not roundCode:
        response = {"code":1002} # 參數錯誤
        return response
    cards = data.get("cards")
    print cards
    if not isValidCards(cards):
        response = {"code":1002} # 參數錯誤
        return response
    bankerCards, playerCards = getCardsFromString(cards)
    pair = computePair(bankerCards, playerCards)
    cardNum = computeCardNum(bankerCards, playerCards)
    playerPoint = computeCardsPoint(playerCards)
    bankerPoint = computeCardsPoint(bankerCards)
    args = (roundCode, playerPoint, bankerPoint, cards, cardNum, pair)
    d = threads.deferToThread(recalc, *args)
    d.addCallback(afterRecalc)

def recalc(roundCode, playerpoint, bankerpoint, cards, cardnum, pair):
    dbRound.updateRoundClosing(roundCode, playerpoint, bankerpoint, cards, cardnum, pair)
    result = procedures.callOnRecalc(roundCode)
    return result

def afterRecalc(result):
    response = {"code":result}
    # 重新結算局回包 0x00060010
    return response

@remoteserviceHandle
def cancelRound_50017(data):
    """取消局注单结算 0x00050011
    """
    roundCode = data.get("roundcode")
    print roundCode
    if not roundCode:
        # 取消局注單結算回包 0x00060011
        response = {"code":1002} # 參數錯誤
        return response
    d = threads.deferToThread(procedures.callOnCancel, roundCode)
    d.addCallback(afterCancel)

def afterCancel(result):
    response = {"code":result}
    # 取消局注單結算回包 0x00060011
    return response

@remoteserviceHandle
def onAddVideo_50003(data):
    # add video to memory
    GameDataManager().onAddVideo(data)
    # push to all clients
    request = addVideo_pb2.addVideoRequest()
    request.videoId = data.get("videoid")
    request.gameType = data.get("gametype")
    request.betTime = data.get("bettime")
    request.url = data.get("url")
    gatepush(50003, request.SerializeToString(), PlayersManager().getAllPlayerIDs())

@remoteserviceHandle
def onModifyVideo_50004(data):
    # update video in memory
    for video in GameDataManager().getAllVideos():
        if video.videoID == data.get("videoid"):
            if video.gameStatus == GameStatus.EGS_CLOSEGAME:
                video.betTime = data.get("bettime")
                video.url = data.get("url")
            break
    # push to all clients
    request = modifyVideo_pb2.modifyVideoRequest()
    request.videoId = data.get("videoid")
    request.betTime = data.get("bettime")
    request.url = data.get("url")
    gatepush(50004, request.SerializeToString(), PlayersManager().getAllPlayerIDs())

@remoteserviceHandle
def onAddTable_50006(data):
    # add table to memory
    GameDataManager().onAddTable(data)
    # push to all clients
    request = addTable_pb2.addTableRequest()
    request.tableId = data.get("tableid")
    request.videoId = data.get("videoid")
    request.limitId = data.get("limitid")
    gatepush(50006, request.SerializeToString(), PlayersManager().getAllPlayerIDs())
    pass

@remoteserviceHandle
def onModifyTable_50007(data):
    # update table in memory
    for table in GameDataManager().getAllTables():
        if table.tableID == data.get("tableid"):
            table.videoID = data.get("videoid")
            table.limitID = data.get("limitid")
            table.flag = data.get("flag")
            break
    # push to all clients
    request = modifyTable_pb2.modifyTableRequest()
    request.tableId = data.get("tableid")
    request.videoId = data.get("videoid")
    request.limitId = data.get("limitid")
    request.flag = data.get("flag")
    gatepush(50007, request.SerializeToString(), PlayersManager().getAllPlayerIDs())
    pass

@remoteserviceHandle
def onModifyTableLimit_50009(data):
    request = modifyTableLimit_pb2.modifyTableLimitRequest()
    request.limitId = data.get("limitid")
    limits = data.get("details", [])
    count = 0
    for limit in limits:
        one = request.tableLimit.add()
        one.playType = limit.get("playtype")
        one.minval_cents = limit.get("minval_cents")
        one.maxval_cents = limit.get("maxval_cents")
        one.flag = limit.get("flag")
        count += 1
    request.num = count
    gatepush(50009, request.SerializeToString(), PlayersManager().getAllPlayerIDs())


@remoteserviceHandle
def onModifyPersonalLimit_50011(data):
    request = modifyPersonalLimit_pb2.modifyPersonalLimitRequest()
    request.limitId = data.get("limitid")
    limits = data.get("details", [])
    count = 0
    for limit in limits:
        one = request.tableLimit.add()
        one.playType = limit.get("playtype")
        one.minval_cents = limit.get("minval_cents")
        one.maxval_cents = limit.get("maxval_cents")
        one.flag = limit.get("flag")
        count += 1
    request.num = count
    gatepush(50011, request.SerializeToString(), PlayersManager().getAllPlayerIDs())