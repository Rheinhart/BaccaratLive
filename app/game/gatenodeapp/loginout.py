#coding:utf8
'''
Created on 2013-8-14

@author: lan (www.9miao.com)
'''
from app.game.gatenodeservice import remoteserviceHandle
from app.game.core.PlayersManager import PlayersManager

from app.game.gatenodeservice import remoteserviceHandle, gatepush
from datetime import datetime

@remoteserviceHandle
def NetConnLost_2(dynamicId):
    '''disable
    '''
    print "NetConnLost_2", dynamicId
    player = PlayersManager().getPlayerByID(dynamicId)
    if player:
        player.connected = False
    return True
    

@remoteserviceHandle
def logout_20003(dynamicId):
    '''disable
    '''
    print "logout_20003"
    player = PlayersManager().getPlayerByID(dynamicId)
    if player:
        PlayersManager().dropPlayer(player)
    return True

@remoteserviceHandle
def test_19999(dynamicId, request_proto):
    '''disable
    '''
    print "[Analysis]", dynamicId, "game test", 19999, datetime.now()
    gatepush(29999, "", [dynamicId])