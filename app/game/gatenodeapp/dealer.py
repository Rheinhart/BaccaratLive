#coding=utf8
__author__ = 'joshua'

from app.share.protofile import dealer_pb2
from app.game.gatenodeservice import remoteserviceHandle
from app.game.core.Dealer import Dealer, DealerManager
from app.game.gatenodeservice import gatepush
from twisted.internet import defer


@remoteserviceHandle
def dealerLogin_30001(dynamicId, request_proto):
    """Dealer登陆"""
    print "DealerLogin_30001"
    # 荷官登录 0x00030001
    request = dealer_pb2.dealerLoginRequest()
    request.ParseFromString(request_proto)

    loginname = request.loginName
    videoId = request.videoId

    dealer = Dealer(dynamicId, videoId, loginname)
    res = DealerManager().dealerLogin(dealer)

    # 荷官登录回包 0x00040001
    response = dealer_pb2.dealerLoginResponse()
    response.code = res.get('code', 0)
    gatepush(40001, response.SerializeToString(), [dynamicId])

    # 视频状态 0x00040006
    gamevideo = dealer.video
    response = gamevideo.getDealerVideoStatusResponse()
    gatepush(40006, response.SerializeToString(), [dynamicId])


@remoteserviceHandle
def newShoe_30002(dynamicId, request_proto):
    # 新建靴 0x00030002
    dealer = DealerManager()._dealer_client[dynamicId]
    res = dealer.onNewShoe()
    # 新建靴回包 0x00040002
    response = dealer_pb2.dealerNewShoeResponse()
    response.code = res.get('code', 1000)
    gatepush(40002, response.SerializeToString(), [dynamicId])


@remoteserviceHandle
def startRound_30003(dynamicId, request_proto):
    # 开局 0x00030003
    dealer = DealerManager()._dealer_client[dynamicId]
    res = dealer.onStartGame()
    # 开局回包 0x00040003
    response = dealer_pb2.dealerNewRoundResponse()
    response.code = res.get("code", 1000)
    if response.code == 0:
        response.roundCode = res.get("roundCode", "")
        response.betTime = res.get("betTime", 30)
    # return response.SerializeToString()
    gatepush(40003, response.SerializeToString(), [dynamicId])

@remoteserviceHandle
def sendCard_30005(dynamicId, request_proto):
    # 发牌 0x00030005
    request = dealer_pb2.dealerNewCardRequest()
    request.ParseFromString(request_proto)
    dealer = DealerManager()._dealer_client[dynamicId]
    gamevideo = dealer.video
    # 发牌回包 0x00040005
    response = dealer_pb2.dealerNewCardResponse()
    if request.roundCode != gamevideo.roundCode:
        response.code = 1007
        gatepush(40005, response.SerializeToString(), [dynamicId])
        return
    res = gamevideo.onNewCard(request.cardIndex, request.cardValue)
    response.code = res.get("code", 0)
    gatepush(40005, response.SerializeToString(), [dynamicId])
    gamevideo.onCheckNextCard(request.cardIndex)

@remoteserviceHandle
def getVideoStatus_30006(dynamicId, request_proto):
    pass


@remoteserviceHandle
def confirmResult_30009(dynamicId, request_proto):
    # 确认结果 0x00030009
    request = dealer_pb2.dealerConfirmResultRequest()
    request.ParseFromString(request_proto)
    dealer = DealerManager()._dealer_client[dynamicId]
    gamevideo = dealer.video
    # 确认结果回包 0x00040009
    response = dealer_pb2.dealerConfirmResultResponse()
    if request.roundCode != gamevideo.roundCode:
        response.code = 1000
        gatepush(40009, response.SerializeToString(), [dynamicId])
        return
    if gamevideo.cardNum == 0:
        response.code = 1009
        gatepush(40009, response.SerializeToString(), [dynamicId])
        return
    # response.code = 0
    # gatepush(40009, response.SerializeToString(), [dynamicId])
    result = gamevideo.onCloseRound()
    if isinstance(result, defer.Deferred):
        result.addCallback(afterConfirm, dynamicId)
    else:
        return afterConfirm(result, dynamicId)

def afterConfirm(result, dynamicId):
    # 确认结果回包 0x00040009
    response = dealer_pb2.dealerConfirmResultResponse()
    response.code = result
    gatepush(40009, response.SerializeToString(), [dynamicId])

@remoteserviceHandle
def cancelResult_30010(dynamicId, request_proto):
    # 取消结果 0x0003000a
    request = dealer_pb2.dealerConfirmResultRequest()
    request.ParseFromString(request_proto)
    # 取消结果回包 0x0004000a
    response = dealer_pb2.dealerConfirmResultResponse()
    dealer = DealerManager()._dealer_client[dynamicId]

    if request.roundCode != dealer.video.roundCode:
        response.code = 1000
        gatepush(40009, response.SerializeToString(), [dynamicId])
        return
    if dealer.video.cardNum == 0:
        response.code = 1009
        gatepush(40009, response.SerializeToString(), [dynamicId])
        return
    res = dealer.video.onCancelResult()
    response.code = res.get("code", 0)
    gatepush(40010, response.SerializeToString(), [dynamicId])
    if response.code == 0:
        dealer.video.sendCardReminder(1)
