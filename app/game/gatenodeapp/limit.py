#coding:utf8
"""
@__author__ = 'Thomas'
"""
from app.game.gatenodeservice import remoteserviceHandle
from app.game.appinterface import limit
from app.share.protofile import personalLimit_pb2, tableLimit_pb2

@remoteserviceHandle
def GetAllPersonalLimitInfo_20018():
    """返回所有个人限红"""
    print "getPersonalLimit_20018"
    limits = limit.GetAllPersonalLimitInfo()
    response = personalLimit_pb2.personalLimitResponse()
    count = 0
    for playtypes in limits.values():
        for playtype in playtypes.values():
            item = response.personalLimit.add()
            item.limitid = playtype.get('limitid', "")
            item.playtype = playtype.get('playtype', -1)
            item.minval_cents = playtype.get('min_cents', -1)
            item.maxval_cents = playtype.get('max_cents', -1)
            count += 1
    response.num = count
    return response.SerializeToString()

@remoteserviceHandle
def GetAllTableLimitInfo_20019():
    """返回所有桌台限红"""
    print "getTableLimit_20019"
    limits = limit.GetAllTableLimitInfo()
    response = tableLimit_pb2.tableLimitResponse()
    count = 0
    for playtypes in limits.values():
        for playtype in playtypes.values():
            item = response.tableLimit.add()
            item.limitid = playtype.get('limitid', "")
            item.playtype = playtype.get('playtype', -1)
            item.minval_cents = playtype.get('min_cents', -1)
            item.maxval_cents = playtype.get('max_cents', -1)
            count += 1
    response.num = count
    return response.SerializeToString()