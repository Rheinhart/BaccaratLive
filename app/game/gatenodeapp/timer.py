#coding=utf8
__author__ = 'joshua'

from app.game.gatenodeservice import remoteserviceHandle, gatepush
from app.game.service.GameProcess import videoProcess, broadcast, detectHeartBeat

@remoteserviceHandle
def gameProcess_21001():
    """  """
    print "gameProcess_21001 start"
    videoProcess()
    return

@remoteserviceHandle
def lobbyBroadCast_21002():
    """  """
    print "broadCast_21002 start"
    broadcast()
    return

@remoteserviceHandle
def detectHeartBeat_21003():
    print "detectHeartBeat_210003 start"
    detectHeartBeat()
    return