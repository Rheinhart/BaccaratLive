#coding=utf8

# __author__ = 'Alan'

from app.game.core.PlayersManager import PlayersManager
from app.game.core.GameDataManager import GameDataManager
from app.game.gatenodeservice import remoteserviceHandle, gatepush
from app.share.protofile import game_pb2, player_pb2, getBetRecords_pb2
from app.share.dbopear import dbBill
from twisted.internet import defer

__author__ = 'Alan'

@remoteserviceHandle
def bet_10028(dynamicId, request_proto):
    """ 下注 0x0001001c """
    print "bet_10028", dynamicId
    request = game_pb2.betRequest()
    request.ParseFromString(request_proto)
    roundcode = request.roundcode
    playtype = int(request.playtype)
    amount_cents = int(request.amount_cents)
    print roundcode, playtype, amount_cents
    # Call player
    player = PlayersManager().getPlayerByID(dynamicId)
    gametable = player.gametable
    result = player.onBet(roundcode, playtype, amount_cents)
    if isinstance(result, defer.Deferred):
        result.addCallback(afterBet, dynamicId, gametable, player, playtype, amount_cents)
    else:
        return afterBet(result, dynamicId, gametable, player, playtype, amount_cents)

def afterBet(result, dynamicId, gametable, player, playtype, amount_cents):
    # print "After bet ", dynamicId
    if not player or not player.connected:
        print dynamicId, "disconnected. No bet response sent "
        return
    # 下注回包 0x0002001c
    response = game_pb2.betResponse()
    response.code = result
    gatepush(20028, response.SerializeToString(), [dynamicId])
    if result != 0:
        return
    # 玩家额度更新 0x00020018
    response = player_pb2.playerCreditResponse()
    response.credit_cents = player.credit_cents
    gatepush(20024, response.SerializeToString(), [dynamicId])
    # 同桌其他人下注 0x00020022
    gametable.sendBetByTable(player.seat, playtype, amount_cents)
    return

@remoteserviceHandle
def potDetails_10030(dynamicId, request_proto):
    """ 查询实时彩池明细 0x0001001e """
    print "potDetails_10030", dynamicId
    request = game_pb2.tablePotDetailsRequest()
    request.ParseFromString(request_proto)
    tableid = request.tableid
    playtype = int(request.playtype)
    roundcode = request.roundcode
    # TODO need to check roundcode?
    # 实时彩池明细 0x0002001e
    response = game_pb2.tablePotDetailsResponse()
    gametable = GameDataManager().getTableByID(tableid)
    if gametable and 1 <= playtype <= 7:
        pots = gametable.onPotDetails(playtype)
        response.code = 0
        response.playtype = playtype
        response.num = len(pots)
        for pot in pots:
            response_item = response.details.add()
            response_item.amount_cents = pot.get("amount_cents")
            player_data = pot.get("player_data")
            response_item.loginname = player_data.get("loginname")
            response_item.nickname = player_data.get("nickname")
            response_item.tableid = player_data.get("tableid")
            response_item.seat = player_data.get("seat")
        gatepush(20030, response.SerializeToString(), [dynamicId])
    else:
        response.code = 1002
        gatepush(20030, response.SerializeToString(), [dynamicId])

@remoteserviceHandle
def getBetRecords_10041(dynamicId, request_proto):
    print "getBetRecords_10041", dynamicId
    request = getBetRecords_pb2.getBetRecordsRequest()
    request.ParseFromString(request_proto)
    start_time = request.startTime
    end_time = request.endTime
    game_type = request.gameType
    round_code = request.roundCode
    bill_no = request.billNo
    page = request.page
    countOfPage = request.countOfPage

    response = getBetRecords_pb2.getBetRecordsResponse()

    if bill_no != "":  # get record by bill no
        record = dbBill.getBillsByBillNo(bill_no)
        if record:
            response.total = 1
            response.currentCount = 1
            bet_record = response.betRecord.add()
            bet_record.billNo = str(record.get('billno'))
            bet_record.billTime = record.get('create_time').strftime("%Y-%m-%d %H:%M:%S") if record.get('create_time') else ""
            bet_record.reckonTime = record.get("reckon_time").strftime("%Y-%m-%d %H:%M:%S") if record.get('reckon_time') else ""
            bet_record.gameType = record.get("gametype")
            bet_record.playType = int(record.get("playtype"))
            bet_record.betCents = int(record.get("bet_amount_cents")) if record.get("bet_amount_cents") else 0
            bet_record.winCents = int(record.get("win_amount_cents")) if record.get("win_amount_cents") else 0
            bet_record.validCents = int(record.get("valid_amount_cents")) if record.get("valid_amount_cents") else 0
            bet_record.flag = int(record.get('flag'))
            bet_record.bankerPoint = record.get('bankerpoint') if record.get('bankerpoint') else -1
            bet_record.playerPoint = record.get('playerpoint') if record.get('playerpoint') else -1
            bet_record.roundCode = record.get('roundcode')
        else:
            response.total = 0
            response.currentCount = 0
        gatepush(20041, response.SerializeToString(), [dynamicId])
        return

    if round_code != "":  # get records by roundcode
        login_name = PlayersManager().getLoginnameByDid(dynamicId)
        cnt, records = dbBill.getBillsByRoundCode(login_name, round_code, page, countOfPage)
        response.total = int(cnt['count(*)'])
        response.currentCount = len(records)
        print response.total, response.currentCount
        if len(records) > 0:
            for record in records:
                bet_record = response.betRecord.add()
                bet_record.billNo = str(record.get('billno'))
                bet_record.billTime = record.get('create_time').strftime("%Y-%m-%d %H:%M:%S") if record.get('create_time') else ""
                bet_record.reckonTime = record.get("reckon_time").strftime("%Y-%m-%d %H:%M:%S") if record.get('reckon_time') else ""
                bet_record.gameType = record.get("gametype")
                bet_record.playType = int(record.get("playtype"))
                bet_record.betCents = int(record.get("bet_amount_cents")) if record.get("bet_amount_cents") else 0
                bet_record.winCents = int(record.get("win_amount_cents")) if record.get("win_amount_cents") else 0
                bet_record.validCents = int(record.get("valid_amount_cents")) if record.get("valid_amount_cents") else 0
                bet_record.flag = int(record.get('flag'))
                bet_record.bankerPoint = record.get('bankerpoint') if record.get('bankerpoint') else -1
                bet_record.playerPoint = record.get('playerpoint') if record.get('playerpoint') else -1
                bet_record.roundCode = record.get('roundcode')
        gatepush(20041, response.SerializeToString(), [dynamicId])
        return

    login_name = PlayersManager().getLoginnameByDid(dynamicId)
    cnt, records = dbBill.getBillsByTimeRange(login_name, page, countOfPage, start_time, end_time, game_type)
    response.total = cnt['count(*)']
    response.currentCount = len(records)
    print response.total, response.currentCount
    if len(records) > 0:
        for record in records:
            bet_record = response.betRecord.add()
            bet_record.billNo = str(record.get('billno'))
            bet_record.billTime = record.get('create_time').strftime("%Y-%m-%d %H:%M:%S") if record.get('create_time') else ""
            bet_record.reckonTime = record.get("reckon_time").strftime("%Y-%m-%d %H:%M:%S") if record.get('reckon_time') else ""
            bet_record.gameType = record.get("gametype")
            bet_record.playType = int(record.get("playtype"))
            bet_record.betCents = int(record.get("bet_amount_cents")) if record.get("bet_amount_cents") else 0
            bet_record.winCents = int(record.get("win_amount_cents")) if record.get("win_amount_cents") else 0
            bet_record.validCents = int(record.get("valid_amount_cents")) if record.get("valid_amount_cents") else 0
            bet_record.flag = int(record.get('flag'))
            bet_record.bankerPoint = record.get('bankerpoint') if record.get('bankerpoint') else -1
            bet_record.playerPoint = record.get('playerpoint') if record.get('playerpoint') else -1
            bet_record.roundCode = record.get('roundcode')
    gatepush(20041, response.SerializeToString(), [dynamicId])
