#coding:utf8
'''
Created on 2013-8-14

@author: lan (www.9miao.com)
'''
from firefly.server.globalobject import GlobalObject
from firefly.utils.services import CommandService, Service
from twisted.internet import reactor
from datetime import datetime

remoteservice = CommandService("gateremote", runstyle=Service.SINGLE_STYLE)
GlobalObject().remote['gate']._reference.addService(remoteservice)


def remoteserviceHandle(target):
    """
    """
    remoteservice.mapTarget(target)

def threadSafeHandle(func):
    def with_threadSafe(*args, **kwargs):
        reactor.callFromThread(func, *args, **kwargs)
    return with_threadSafe

@threadSafeHandle
def gatepush(command, response, dynamicId):
    print "[Analysis]", ",".join(dynamicId), "game push", command-10000, datetime.now()
    gate_node = GlobalObject().remote.get('gate')
    if len(dynamicId) > 0:
        gate_node.callRemote("pushObjectRoot", command, response, dynamicId)

@threadSafeHandle
def gateDropClient(dynamicId):
    print "gateDropClient", dynamicId
    gate_node = GlobalObject().remote.get('gate')
    gate_node.callRemote("dropClientCalledByGameServer", dynamicId)