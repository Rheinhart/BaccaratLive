#coding:utf8
'''
Created on 2013-8-14

@author: lan (www.9miao.com)
'''
from firefly.server.globalobject import rootserviceHandle, GlobalObject
from app.gate.gateservice import localservice
from app.gate.core.CustomerManager import CustomerManager
from datetime import datetime
print "gate add rootservice"

@rootserviceHandle
def forwarding(key, dynamicId, ip, data):
    """
    """
    #import time
    #time.sleep(5)
    print "[Analysis]", dynamicId, "gate forwarding", key, datetime.now()
    if localservice._targets.has_key(key):
        return localservice.callTarget(key,dynamicId,ip,data)
    elif key > 30000:
        return GlobalObject().root.callChild("game1", key, dynamicId, data)
    else:
        customer = CustomerManager().getCustomerByDynamicId(dynamicId)
        if not customer:
            print "Invalid customer"
            return
        if customer.getNode() is None:
            print "Customer not in game"
            return
        return GlobalObject().root.callChild("game1", key, dynamicId, data)

@rootserviceHandle
def pushObjectRoot(topicID,msg,sendList):
    pushObject(topicID,msg,sendList)

def pushObject(topicID,msg,sendList):
    print "[Analysis]", ",".join(sendList), "gate push", topicID-10000, datetime.now()
    sendListByNode = {}
    for dynamicId in sendList:
        node, session_no = dynamicId.split("#")
        if node not in sendListByNode:
            sendListByNode[node] = []
        sendListByNode[node].append(int(session_no))
    for node, dynamicIds in sendListByNode.items():
        GlobalObject().root.callChild(node, "pushObject", topicID, msg, dynamicIds)

#def getClientIP(dynamicID):
#    return GlobalObject().root.callChild("net", "getClientIP", dynamicID)

@rootserviceHandle
def pushAdminMessage(command,senddata):
    """转发admin后台消息
    """
    return GlobalObject().root.callChild("game1", int(command), senddata)

def dropClient(deferResult,dynamicId):
    CustomerManager().dropCustomerByDynamicId(dynamicId)

@rootserviceHandle
def netconnlost(dynamicId):
    '''客户端断开连接时的处理
    @param dynamicId: int 客户端的动态ID
    '''
    print "gate netconnlost", dynamicId
    customer = CustomerManager().getCustomerByDynamicId(dynamicId)
    if customer:
        customer.connected = False
        if customer.getNode() is not None: #判断是否已经登入game server
            GlobalObject().root.callChild(customer.getNode(),2,dynamicId) #在game server 更新player成disconnected



@rootserviceHandle
def dropClientCalledByGameServer(dynamicId):
    """客户端心跳超时，由GameServer 调用此方法
    :param dynamicId:
    :return:
    """
    print "dropClientCalledByGameServer"
    customer = CustomerManager().getCustomerByDynamicId(dynamicId)
    if customer:
        dropClient(None, dynamicId)