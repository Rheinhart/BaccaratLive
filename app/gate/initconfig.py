#coding:utf8
'''
Created on 2013-8-14

@author: lan (www.9miao.com)
'''
from gaterootapp import *
from localservice import *
from dataloader import registe_madmin, initData

def loadModule():
    import gateservice
    registe_madmin()
    initData()
