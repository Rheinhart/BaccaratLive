#coding=utf8

# __author__ = 'Alan'


from app.gate.core.Customer import Customer
from app.gate.core.CustomerManager import CustomerManager
from app.share.protofile import login_pb2


def getLoginResult(dynamicId, loginname, password, ip):
    response = login_pb2.loginResultResponse()
    customer = Customer(dynamicId)
    if not customer.login(loginname, password, ip):
        response.code = 1001 # 密碼錯誤
    else:
        CustomerManager().addCustomer(customer)
        token = customer.getToken()
        response.code = 0
        response.flag = customer.flag
        response.token = token
    return response

def getUserInfo(dynamicId):
    response = login_pb2.userInfoResponse()
    customer = CustomerManager().getCustomerByDynamicId(dynamicId)
    response.nickname = customer.getNickName()
    response.credit_cents = customer.getCredit()
    return response

def verifyToken(dynamicId, loginname, token):
    print "verifyToken"
    response = login_pb2.loginGameResultResponse()
    if CustomerManager().verifyToken(dynamicId, loginname, token):
        response.code = 0
    else:
        response.code = 1009 # 遊戲狀態不對
    return response