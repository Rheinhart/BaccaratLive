#coding=utf8
__author__ = 'joshua'
from app.gate.core.VideoTableManager import getAllVideos
from app.share.protofile import getVideos_pb2


def getVideoAndTable_ProtoData():
    response = getVideos_pb2.getVideosResponse()
    videos = getAllVideos()
    if videos:
        response.videoCount = len(videos)
        for video in videos:
            response_item = response.video.add()
            response_item.videoId = video.get('videoid',"")
            response_item.gameType = video.get('gametype',"")
            response_item.url = video.get('url', "")
            tabs = video['tables']
            response_item.tableCount = len(tabs)
            for tab in tabs:
                table = response_item.table.add()
                table.tableId = tab['tableid']
                table.limitId = tab.get('limitid', "")
                table.seats = tab.get('seats', 0)
    return response.SerializeToString()
