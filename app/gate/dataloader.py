#coding:utf8
__author__ = 'joshua'

import memmode
from firefly.dbentrust.madminanager import MAdminManager
from firefly.server.globalobject import GlobalObject

def initData():
    """载入初始数据
    """
    # VideoManager().initVideoList()
    # from twisted.internet import reactor
    # reactor.callLater(5, f)
    # reactor.callLater(5, f2)
    # reactor.callLater(5, f3)


# def f(): GlobalObject().root.callChild("game1", 21001)
# def f2(): GlobalObject().root.callChild("game1", 21002)
# def f3(): GlobalObject().root.callChild("game1", 21003)


def registe_madmin():
    """注册数据库与memcached对应
    """
    MAdminManager().registe(memmode.tb_table_admin)
    MAdminManager().registe(memmode.tb_video_admin)