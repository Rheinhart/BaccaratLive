#coding=utf8

from app.gate.gateservice import localserviceHandle
from app.gate.core.CustomerManager import CustomerManager
from firefly.server.globalobject import  GlobalObject

__author__ = 'Alan'

# 10003, logout
@localserviceHandle
def logout_10003(key, dynamicId, ip, request_proto):
    print "logout_10003", key, dynamicId, request_proto
    customer = CustomerManager().getCustomerByDynamicId(dynamicId)
    if customer and customer.getNode() is not None: #判断是否已经登入game server
        d = GlobalObject().root.callChild(customer.getNode(),20003,dynamicId) #在game server的player manager刪除用戶
        d.addCallback(dropClient,dynamicId)
    else:
        dropClient(None, dynamicId)

def dropClient(deferResult,dynamicId):
    '''清理客户端的记录
    @param result: 写入后返回的结果
    '''
    #node = customer.getNode()
    #if node:
    #    GameSerManager().dropClient(node, dynamicId)
    CustomerManager().dropCustomerByDynamicId(dynamicId)
