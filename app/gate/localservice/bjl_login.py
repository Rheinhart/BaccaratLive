#coding=utf8

# import json
from app.gate.appinterface import bjl_login,video
from app.gate.gateservice import localserviceHandle
from app.gate.core.CustomerManager import CustomerManager
from app.share.protofile import login_pb2
from firefly.server.globalobject import GlobalObject
from app.gate.gaterootapp.netforwarding import pushObject

__author__ = 'Alan'
from datetime import datetime
# 10001, loginToServer
@localserviceHandle
def loginToServer_10001(key, dynamicId, ip, request_proto):
    print "loginToServer_10001", key, dynamicId, request_proto
    request = login_pb2.loginRequest()
    request.ParseFromString(request_proto)
    loginname = request.loginname
    password = request.password
    loginResult = bjl_login.getLoginResult(dynamicId, loginname, password, ip)
    # 登录回包 0x00020001
    pushObject(20001, loginResult.SerializeToString(), [dynamicId])
    if loginResult.code != 0:
        return
    # 用户信息 0x00020002
    userInfo = bjl_login.getUserInfo(dynamicId)
    pushObject(20002, userInfo.SerializeToString(), [dynamicId])
    # 视频桌台信息 0x00020005
    pushObject(20005, video.getVideoAndTable_ProtoData(), [dynamicId])
    return

# 10011, loginToGameServer
@localserviceHandle
def loginToGameServer_10017(key, dynamicId, ip, request_proto):
    print "loginToGameServer_10017", key, dynamicId, request_proto
    request = login_pb2.loginGameRequest()
    request.ParseFromString(request_proto)
    loginname = request.loginname
    token = request.token
    if not CustomerManager().verifyToken(dynamicId, loginname, token):
        # 登录回包 0x00020011
        response = login_pb2.loginGameResultResponse()
        response.code = 1009 # 遊戲狀態不對
        pushObject(20017, response.SerializeToString(), [dynamicId])
        return
    customer = CustomerManager().getCustomerByToken(token)
    # 連接Game node
    nownode = "game1"
    customer.setNode(nownode)
    # Call enterGame_20017
    GlobalObject().root.callChild(nownode,20017,dynamicId, customer.getCustomerData())
    # 个人限红 0x00020012
    plimit = GlobalObject().root.callChild(nownode, 20018)
    plimit.addCallback(deferred_push, 20018, dynamicId)
    # 桌台限红 0x00020013
    tlimit = GlobalObject().root.callChild(nownode, 20019)
    tlimit.addCallback(deferred_push, 20019, dynamicId)
    # 历史露珠 0x00020014
    beadRoad = GlobalObject().root.callChild(nownode, 20020)
    beadRoad.addCallback(deferred_push, 20020, dynamicId)
    # 视频状态列表 0x0002002f
    # Call getVideoStatusList_20047
    videoStatuslist = GlobalObject().root.callChild(nownode,20047,dynamicId)
    videoStatuslist.addCallback(deferred_push,20047,dynamicId)
    # 公告信息 0x00020025
    bulletin = GlobalObject().root.callChild(nownode, 20037)
    bulletin.addCallback(deferred_push, 20037, dynamicId)
    # 在线玩家列表 0x00020015
    # Call playerList_10021
    GlobalObject().root.callChild(nownode,10021,dynamicId)
    return

def deferred_push(response, command, dynamicId):
    pushObject(command, response, [dynamicId])
    return