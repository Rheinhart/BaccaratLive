__author__ = 'Alan'

from firefly.utils.singleton import Singleton
from twisted.python import log
from firefly.server.globalobject import GlobalObject

# Removed on 2015-11-11 by Alan

"""

UP = 1000 # unused

class  GameSer:

    def __init__(self,gameId):
        self.id = gameId
        self._clients = set()

    def addClient(self,clientId):
        self._clients.add(clientId)

    def dropClient(self,clientId):
        self._clients.remove(clientId)

    def getClientCnt(self):
        return len(self._clients)

class GameSerManager:

    __metaclass__ = Singleton

    def __init__(self):
        self._gameSers = {}
        self.initGameSers()

    def initGameSers(self):
        for childname in GlobalObject().root.childsmanager._childs.keys():
            if "game" in childname:
                self.addGameSer(childname)

    def addGameSer(self,gameId):
        gameSer = GameSer(gameId)
        self._gameSers[gameSer.id] = gameSer
        return gameSer


    def getGameSerById(self,gameId):
        gameSer = self._gameSers.get(gameId)
        if not gameSer:
            gameSer = self.addGameSer(gameId)
        return gameSer

    def addClient(self,gameId,clientId):
        gameSer = self.getGameSerById(gameId)
        if not gameSer:
            return False
        gameSer.addClient(clientId)
        return True

    def dropClient(self,gameId,clientId):
        gameSer = self.getGameSerById(gameId)
        if gameSer:
            try:
                gameSer.dropClient(clientId)
            except Exception:
                msg = "gameId:%d-------clientId:%d"%(gameId,clientId)
                log.err(msg)

    def getAllClientCnt(self):
        return sum([ser.getClientCnt() for ser in self._gameSers])


    def getBsetScenNodeId(self):
        serverlist = self._gameSers.values()
        slist = sorted(serverlist,reverse=False,key = lambda gser:gser.getClientCnt())
        if slist:
            return slist[0].id
"""