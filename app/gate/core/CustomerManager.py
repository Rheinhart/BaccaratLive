#coding:utf8
__author__ = 'joshua'

from firefly.utils.singleton import Singleton
import hashlib
from time import strftime
from thread import allocate_lock

class CustomerManager:

    __metaclass__ = Singleton

    def __init__(self):
        self._customers = {}
        self._tokens = {}
        self.mutex = allocate_lock()

    def addCustomer(self, customer):
        """添加一个用户
        """
        loginname = customer.getLoginName()
        if not self._customers.has_key(loginname):
            # For new customer, gen token and save the customer in the list
            print loginname, "connected"
            token = self.genToken(loginname)
            self._tokens[token] = loginname
            with self.mutex:
                self._customers[loginname] = customer
            customer.setToken(token)
        else:
            # For existing customer,
            # 1. send disconnect message to the original one by dynamic id
            # 2. replace the list wirh the new customer
            # 3. set the original token
            print loginname, "reconnected"
            original_customer = self.getCustomerByName(loginname)
            original_token = original_customer.getToken()
            original_customer.disconnectClient()
            with self.mutex:
                self._customers[loginname] = customer
            customer.setToken(original_token)

    def verifyToken(self, dynamicId, loginname, token):
        loginname_ = self._tokens.get(token)
        if loginname_ is None or loginname_ != loginname:
            return False
        customer = self.getCustomerByName(loginname)
        # For existing customer with different dynamic id,
        # 1. Disconnect the original customer
        # 2. Update the customer with new dynamic id
        if customer.dynamicId != dynamicId:
            customer.disconnectClient()
            customer.dynamicId = dynamicId
            customer.connected = True
        return True

    def getCustomerByToken(self, token):
        loginname = self._tokens.get(token)
        return self.getCustomerByName(loginname)

#    def getLoginNameByToken(self, token):
#        return self._tokens.get(token)

    def getCustomerByName(self, loginname):
        return self._customers.get(loginname)

    def getCustomerByDynamicId(self,dynamicId):
        for customer in self._customers.values():
            if customer.dynamicId==dynamicId:
                return customer
        return None

    def dropCustomerByName(self, loginname):
        customer = self.getCustomerByName(loginname)
        if customer:
            self.dropCustomer(customer)

    def dropCustomerByDynamicId(self, dynamicId):
        customer = self.getCustomerByDynamicId(dynamicId)
        if customer:
            self.dropCustomer(customer)

    def dropCustomer(self, customer):
        loginname = customer.loginname
        token = customer.getToken()
        with self.mutex:
            try:
                del self._customers[loginname]
            except Exception,e:
                print e
            try:
                del self._tokens[token]
            except Exception,e:
                print e

    def genToken(self, loginname):
        now = strftime("%Y%m%d%H%M%S")
        return hashlib.md5(loginname.encode()+str(now).encode()).hexdigest()

