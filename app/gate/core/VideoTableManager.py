#coding=utf8
__author__ = 'joshua'

from app.gate.memmode import tb_table_admin, tb_video_admin


def getAllVideos():
    videos = []
    vkeylist = tb_video_admin.getAllPkByFk(0)
    for vid in vkeylist:
        video = getVideoByID(vid)
        videos.append(video)
    return videos


def getVideoByID(vid):
    video = tb_video_admin.getObjData(vid)
    tables = getTablesByVideoID(vid)
    video["tablecount"] = len(tables)
    video["tables"] = tables
    return video


def getTablesByVideoID(videoID):
    tables = []
    tidlist = tb_table_admin.getAllPkByFk(videoID)
    for tid in tidlist:
        table = tb_table_admin.getObjData(tid)
        # print table
        tables.append(table)
    return tables

