#coding:utf8
__author__ = 'joshua'

from app.share.dbopear import dbCustomer
from app.gate.gaterootapp.netforwarding import pushObject
from datetime import datetime
from twisted.python import threadpool
from twisted.internet import reactor, threads

INITTOWN = 1000
pool = threadpool.ThreadPool(1,30)
class Customer:

    def __init__(self, dynamicId = -1):
        self.loginname = ""
        self.agentcode = -1
        self.password = ""
        self.nickname = ""
        self.flag = 0
        self.credit_cents = 0
        self.limitid = ""
        self.last_login_ip = ""
        self.pwd_expired_time = ""
        self.node = None
        self.dynamicId = dynamicId
        self.connected = True

    def login(self, loginname, password, ip):
        data = dbCustomer.getCustomerInfo(loginname, password)
        if not data:
            return False
        for keyname in data.keys():
            if self.__dict__.has_key(keyname.lower()):
                setattr(self,keyname.lower(),data.get(keyname))
        # ip = getClientIP(self.dynamicId)
        threads.deferToThreadPool(reactor, pool, self.updateLastLoginInfo, ip)
        return True

    def updateLastLoginInfo(self, ip):
        if ip:
            self.last_login_ip = ip
            dbCustomer.updateCustomerLoginInfo(self.loginname, ip)

    def getLoginName(self):
        return self.loginname

    def getNickName(self):
        return self.nickname

    def getCredit(self):
        return self.credit_cents

    def setDynamicId(self,dynamicId):
        self.dynamicId = dynamicId

    def getDynamicId(self):
        return self.dynamicId

    def setToken(self,token):
        self.token = token

    def getToken(self):
        return self.token

    def getNode(self):
        return self.node

    def setNode(self,node):
        self.node = node

    def disconnectClient(self):
        msg = u"您的账户已在其他地方登录".encode('utf-8')
        pushObject(905, msg, [self.dynamicId])

    def getCustomerData(self):
        return {
            'loginname': self.loginname,
            'nickname': self.nickname,
            'limitid': self.limitid,
            'credit_cents': self.credit_cents,
            'ip': self.last_login_ip
        }