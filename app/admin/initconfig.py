#coding:utf8
'''
Created on 2013-9-4
 
@author: hg (www.9miao.com)
'''
 
from twisted.internet import reactor
from twisted.web.server import Site
from twisted.web import vhost
from handle import Video_Operation, Rounds_Operation,Bulletin_Operation,TableLimit_Operation,PersonalLimit_Operation,ControllerLogin_Operation,Table_Operation
 
 
def loadModule():
    root = vhost.NameVirtualHost()
    root.putChild('controller', ControllerLogin_Operation()) #管理员登录
    root.putChild('video', Video_Operation())#在浏览器地址栏输入http://localhost:2012/video?username=xx&opera_str=xxx  username是要操作的账号  opera_str是要操作的脚本
    root.putChild('table', Table_Operation())
    root.putChild('round', Rounds_Operation())
    root.putChild('bulletin',Bulletin_Operation())
    root.putChild('tablelimit',TableLimit_Operation())
    root.putChild('personallimit',PersonalLimit_Operation())
    reactor.listenTCP(2012,Site(root))

