#coding:utf8
'''
@author: thomas
'''
from twisted.web import resource
from twisted.python import log
from firefly.server.globalobject import GlobalObject
from app.share.protofile import bulletin_pb2

class ControllerLogin_Operation(resource.Resource):
    """管理端登錄
    """
    def render(self, request):
        if request:
            command = request.args.get('command',[None])[0]
            controller = request.args.get('controller',[None])[0]
            gete_node = GlobalObject().remote.get('gate')
            senddata = {'controller':controller}
            data = gete_node.callRemote("pushAdminMessage",command,senddata)
            if command == '50001':
                if data:
                    msg = 'Controller Login'
                    log.msg(msg)
                    return '60001'
                else:
                    msg = 'Controller lost game server'
                    log.err(msg)
        else:
            msg = 'no request'
            log.err(msg)
            return '1002'


class Bulletin_Operation(resource.Resource):

    def render(self,request):
        if request:
            command = request.args.get('command',[None])[0]
            senddata = {}
            if command == '50013':
                gete_node = GlobalObject().remote.get('gate')
                data = gete_node.callRemote("pushAdminMessage",command,senddata)
                if data:
                    r =data.addCallback(self._showBulletin)
                else:
                    msg = 'Cannot found command'
                    log.err(msg)
            else:
                msg = 'no message recieved'
                log.err(msg)
                return msg
        else:
            msg = 'no message'
            log.err(msg)
            return '1002'

    def _showBulletin(self,data):
        response = bulletin_pb2.bulletinResponse()
        response.ParseFromString(data)
        print response
        return response

class Video_Operation(resource.Resource):

    def render(self, request):
        if request:
            command = request.args.get('command',[None])[0]
            videoid = request.args.get('videoid',[None])[0]
            gametype = request.args.get('gametype',[None])[0]
            bettime = request.args.get('bettime',[None])[0]
            url = request.args.get('url',[None])[0]
            gete_node = GlobalObject().remote.get('gate')
            senddata = {'videoid':videoid,'gametype':gametype,'bettime':int(bettime),'url':url}
            gete_node.callRemote("pushAdminMessage",command,senddata)

            if command == '50003': #添加视频
                return '60003'
            elif command == '50004': #修改视频
                return '60004'
            else:
                msg = 'not video update message'
                log.err(msg)
        else:
            msg = 'no request'
            log.err(msg)
            return '1002'

class Table_Operation(resource.Resource):

    def render(self, request):
        if request:
            command = request.args.get('command',[None])[0]
            tableid = request.args.get('tableid',[None])[0]
            videoid = request.args.get('videoid',[None])[0]
            limitid = request.args.get('limitid',[None])[0]
            flag = request.args.get('flag',[None])[0]
            gete_node = GlobalObject().remote.get('gate')
            senddata = {'videoid':videoid,'tableid':tableid,'flag':int(flag),'limitid':limitid}
            gete_node.callRemote("pushAdminMessage",command,senddata)
            if command == '50006': #添加桌台
                return '60006'
            elif command == '50007': #修改桌台
                return '60007'
            else:
                msg = 'not table update message'
                log.err(msg)
        else:
            msg = 'no request'
            log.err(msg)
            return '1002'


class Rounds_Operation(resource.Resource):

    def render(self, request):
        if request:
            command = request.args.get('command',[None])[0]
            gete_node = GlobalObject().remote.get('gate')
            roundcode = request.args.get('roundcode',[None])[0]
            cards = request.args.get('cards',[None])[0]
            senddata = {'roundcode':roundcode,'cards':cards}
            data = gete_node.callRemote("pushAdminMessage",command,senddata)
            if command == '50016':
                if data:
                    msg = 'recalcRound'
                    log.msg(msg)
                    return '60016'
                else:
                    msg = 'Cannot recalcRound'
                    log.err(msg)
            elif command == '50017':
                if data:
                    msg = 'cancelRound'
                    log.msg(msg)
                    return '60017'
        else:
            msg = 'no request'
            log.err(msg)
            return '1002'


class TableLimit_Operation(resource.Resource):

    def render(self, request):
        if request:
            command = request.args.get('command',[None])[0]
            limitid = request.args.get('limitid',[None])[0]
            maxval_cents = request.args.get('maxval_cents',[None])[0]
            minval_cents = request.args.get('minval_cents',[None])[0]
            playtype = request.args.get('playtype',[None])[0]
            flag = request.args.get('flag',[None])[0]

            gete_node = GlobalObject().remote.get('gate')
            senddata = {'limitid':limitid,'maxval_cents':maxval_cents,'minval_cents':minval_cents,'playtype':playtype,'flag':flag}
            data = gete_node.callRemote("pushAdminMessage",command,senddata)
            if command == '50009':
                if data:
                    msg = 'Modify the TableLimit'
                    log.msg(msg)
                    return '60009'
                else:
                    msg = 'Cannot Modify the TableLimit'
                    log.err(msg)
        else:
            msg = 'no request'
            log.err(msg)
            return '1002'


class PersonalLimit_Operation(resource.Resource):

    def render(self, request):
        if request:
            command = request.args.get('command',[None])[0]
            limitid = request.args.get('limitid',[None])[0]
            maxval_cents = request.args.get('maxval_cents',[None])[0]
            minval_cents = request.args.get('minval_cents',[None])[0]
            playtype = request.args.get('playtype',[None])[0]
            flag = request.args.get('flag',[None])[0]

            gete_node = GlobalObject().remote.get('gate')
            senddata = {'limitid':limitid,'maxval_cents':maxval_cents,'minval_cents':minval_cents,'playtype':playtype,'flag':flag}
            data = gete_node.callRemote("pushAdminMessage",command,senddata)
            if command == '50011':
                if data:
                    msg = 'Modify the PersonalLimit'
                    log.msg(msg)
                    return '60011'
                else:
                    msg = 'Cannot Modify the PersonalLimit'
                    log.err(msg)
        else:
            msg = 'no request'
            log.err(msg)
            return '1002'