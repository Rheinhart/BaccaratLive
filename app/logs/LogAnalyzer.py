__author__ = 'Alan'
from pprint import pprint
from datetime import datetime
record = []
#record.append({"date":"2015-12-22 12:47:27+0800", "data":"ABC"})
#record.append({"date":"2015-12-22 12:46:27+0800", "data":"BCD"})

#record = sorted(record, key=lambda x:x["date"])
#pprint(record)
#raw_input()
filenames = [
    "net.log",
    "net2.log",
    "gate.log",
    "game1.log"
]
for filename in filenames:
    f = open(filename, "rb")
    for r in f.readlines():
        if "[Analysis]" in r and "19999" in r:
            data = r.split(" ")
            date_= data[0]+" "+data[1]
            date_formatted =  data[8]+" " + data[9].split("\r")[0]
            if "." not in date_formatted:
                date_formatted += ".0"
            record.append({
                #"date_":date_,
                "user":data[4],
                "node":data[5],
                "action":data[6],
                "datetime":datetime.strptime(date_formatted, "%Y-%m-%d %H:%M:%S.%f")
                #"date":data[8],
                #"time":data[9].split("\r")[0]
            })

record = sorted(record, key=lambda x:x["datetime"])
#pprint( record)
users = {}
for row in record:
    user = row["user"]
    if user not in users:
        users[user] = []
    users[user].append(row)
    if row["node"] == "net" and row["action"] == "forwarding":
        row["time_elapsed"] = 0
    else:
        row["time_elapsed"] = (users[user][-1]["datetime"] - users[user][-2]["datetime"]).total_seconds()
action = {}
action["net_forward_to_gate_forward"] = []
action["gate_forward_to_game"] = []
action["game_to_game_push"] = []
action["game_push_to_gate_push"] = []
action["gate_push_to_net_push"] = []
for user in users:
    for row in users[user]:
        if row["node"] == "gate" and row["action"] == "forwarding":
            action["net_forward_to_gate_forward"].append(row["time_elapsed"])
        if row["node"] == "game" and row["action"] == "test":
            action["gate_forward_to_game"].append(row["time_elapsed"])
        if row["node"] == "game" and row["action"] == "push":
            action["game_to_game_push"].append(row["time_elapsed"])
        if row["node"] == "gate" and row["action"] == "push":
            action["game_push_to_gate_push"].append(row["time_elapsed"])
        if row["node"] == "net" and row["action"] == "push":
            action["gate_push_to_net_push"].append(row["time_elapsed"])

for a in action:
    print "%s, %.3f" % (a, sum(action[a]) / len(action[a]))
