from twisted.internet import reactor, threads
import time
def doLongCalculation1():
    # .... do long calculation here ...
    print "hi 1"
    reactor.callLater(1, doLongCalculation1)
def doLongCalculation2():
    # .... do long calculation here ...
    print "hi 5"
    reactor.callLater(5, doLongCalculation2)

def printResult(x):
    print x

# run method in thread and get result as defer.Deferred
pool = reactor.getThreadPool()
reactor.suggestThreadPoolSize(30)
threads.deferToThreadPool(reactor, reactor.getThreadPool(), doLongCalculation1)
threads.deferToThreadPool(reactor, reactor.getThreadPool(), doLongCalculation2)
#reactor.callInThread(doLongCalculation2)
#reactor.callInThread(doLongCalculation1)
reactor.run()