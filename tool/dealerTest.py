#coding=utf8

import sys
sys.path.append("..")
from twisted.internet.protocol import Protocol, ClientFactory
import struct,json
import time
from firefly.netconnect.datapack import DataPackProtoc

from app.share.protofile.dealer import *

token = ""

def sendData(sendstr,commandId):
    """78,37,38,48,9,0"""
    HEAD_0 = chr(78)
    HEAD_1 = chr(37)
    HEAD_2 = chr(38)
    HEAD_3 = chr(48)
    ProtoVersion = chr(9)
    ServerVersion = 0
    sendstr = sendstr
    data = struct.pack('!sssss3I',HEAD_0,HEAD_1,HEAD_2,\
                       HEAD_3,ProtoVersion,ServerVersion,\
                       len(sendstr)+4,commandId)
    senddata = data+sendstr
    return senddata

def resolveRecvdata(data):
    print 'Rev'
    print data
    head = struct.unpack('!sssss3I',data[:17])
    length = head[6] - 6
    print length
    data = data[17:17+length]
    return data

class Echo(Protocol):

    buff = ""
    isEnter = False

    def connectionMade(self):
        self.dealerLoginToServer()
        # self.getVideoandTable()
        self.datahandler=self.dataHandleCoroutine()
        self.datahandler.next()

    def dataHandleCoroutine(self):
        """
        """
        length = self.factory.dataprotocl.getHeadlength()
        while True:
            data = yield
            self.buff += data
            while self.buff.__len__() >= length:
                unpackdata = self.factory.dataprotocl.unpack(self.buff[:length])
                if not unpackdata.get('result'):
                    print ('illegal data package --')
                    #self.transport.loseConnection()
                    break
                command = unpackdata.get('command')
                rlength = unpackdata.get('length')
                request = self.buff[length:length+rlength]
                if request.__len__()< rlength:
                    print('some data lose')
                    break
                self.buff = self.buff[length+rlength:]
                print command
                # Login to server
                if command == 10001:
                    pass
                # 登录回包 0x00030001
                elif command == 30001:
                    response = dealerLogin_pb2.dealerLoginResponse()
                    response.ParseFromString(request)
                    print response
                    self.dealerNewShoe()
                # 新建靴 0x00020002
                elif command == 30002:
                    response = dealerNewShoe_pb2.dealerNewShoeResponse()
                    response.ParseFromString(request)
                    print response
                    self.dealerNewRound()
                elif command == 30003:
                    response = dealerNewRound_pb2.dealerNewRoundResponse()
                    response.ParseFromString(request)
                    print response
                elif command == 30005:
                    response = dealerNewCard_pb2.dealerNewCardResponse()
                    response.ParseFromString(request)
                    print response

                elif command == 40003:
                    response = dealerNewRound_pb2.dealerNewRoundResponse()
                    response.ParseFromString(request)
                    print "round " + response.roundCode + " has been created!"
                    # cardValue = raw_input("please scan the first card for round " + response.roundCode + " :")
                    # self.dealerSendCard(response.roundCode, int(cardValue), 1)

                elif command == 40004:
                    response = sendCardReminder_pb2.senderCardReminderRequest()
                    response.ParseFromString(request)
                    cardValue = raw_input("please scan the " + str(response.cardIndex) + " card for round " + response.roundCode + " :")
                    self.dealerSendCard(response.roundCode, int(cardValue), response.cardIndex)

                elif command == 40007:
                    response = gameResult_pb2.gameResultResponse()
                    response.ParseFromString(request)
                    print "round end!"
                    print response
                    self.dealerConfirmResult(response.roundCode)
                elif command == 40009:
                    response = dealerConfirmResult_pb2.dealerConfirmResultResponse()
                    response.ParseFromString(request)
                    if response.code == 0:
                        print "Round Over!"
                    else:
                        print "Something wrong"
                    self.dealerNewShoe()
                elif command == 40008:
                    # checkout confirm
                    pass
                elif command == 40010:
                    # cancel result ack
                    response = dealerConfirmResult_pb2.dealerConfirmResultResponse()
                    response.ParseFromString(request)
                    if response.code == 0:
                        print "Round been canceled!"
                        cardValue = raw_input("please scan the first card:")
                        self.dealerSendCard(response.roundCode, int(cardValue), 1)



    def dataReceived(self, data):
        # print 'rev', data
        self.datahandler.send(data)

    def dealerLoginToServer(self):
        """ 登录 0x00030001 """
        request = dealerLogin_pb2.dealerLoginRequest()
        request.loginName = "joshua"
        request.videoId = "V01"
        a = sendData(request.SerializeToString(), 30001)
        self.transport.write(a)

    def dealerNewShoe(self):
        """ 登录 0x00010011 """
        cmd = raw_input("do you want to new shoe？")
        if cmd == "y" or cmd == "yes":
            a = sendData("new shoe", 30002)
            self.transport.write(a)
        else:
            self.dealerNewRound()

    def dealerNewRound(self):
        """ 进桌 0x00010019 """
        cmd = raw_input("do you want to new round？")
        if cmd == "y" or cmd == "yes":
            a = sendData("new round", 30003)
            self.transport.write(a)
        else:
            self.dealerNewShoe()

    def dealerConfirmResult(self, roundCode):
        """  0x0001001a """
        input = raw_input("confirm result? ")
        if input == "y" or input == "yes":
            request = dealerConfirmResult_pb2.dealerConfirmResultRequest()
            request.roundCode = roundCode
            a = sendData(request.SerializeToString(), 30009)
            self.transport.write(a)
        elif input == "n" or input == "no":
            request = dealerConfirmResult_pb2.dealerConfirmResultRequest()
            request.roundCode = roundCode
            a = sendData(request.SerializeToString(), 30010)
            self.transport.write(a)

    def dealerSendCard(self, roundCode, cardValue, cardIndex):
        """ 下注 0x0001001c """
        request = dealerNewCard_pb2.dealerNewCardRequest()
        request.roundCode = roundCode
        request.cardValue = cardValue
        request.cardIndex = cardIndex
        a = sendData(request.SerializeToString(), 30005)
        self.transport.write(a)

    def getVideoandTable(self):
        a = sendData("", 20004)
        self.transport.write(a)

class EchoClientFactory(ClientFactory):

    protocol = Echo

    def __init__(self,dataprotocl=DataPackProtoc(78,37,38,48,9,0)):
        self.dataprotocl = dataprotocl

    def startedConnecting(self, connector):
        print 'Started to connect.'

    def buildProtocol(self, addr):
        proto = ClientFactory.buildProtocol(self, addr)
        print 'Connected.'
        return proto

    def clientConnectionLost(self, connector, reason):
        print 'Lost connection.  Reason:', reason

    def clientConnectionFailed(self, connector, reason):
        print 'Connection failed. Reason:', reason


from twisted.internet import reactor
reactor.connectTCP("localhost", 11009, EchoClientFactory())
reactor.run()
