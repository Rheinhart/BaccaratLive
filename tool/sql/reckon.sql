DROP PROCEDURE IF EXISTS onreckonround;
DROP PROCEDURE IF EXISTS onreckon;
DELIMITER $$

CREATE PROCEDURE  onreckonround(IN p_roundcode VARCHAR(16))
BEGIN
    -- From t_rounds
    DECLARE p_bankerpoint, p_playerpoint, p_pair tinyint(4);
    DECLARE p_begintime, p_closetime datetime;
	
	DECLARE p_loginname varchar(32);
	DECLARE p_banker_bet_amount_cents, p_player_bet_amount_cents INT;
    DECLARE done INT DEFAULT FALSE;
	DECLARE cur_loginname cursor FOR select loginname, 
		sum(IF(playtype =1, bet_amount_cents, 0)) AS banker_bet_amount_cents, 
        sum(IF(playtype =2, bet_amount_cents, 0)) AS player_bet_amount_cents
		from t_orders 
		where roundcode = p_roundcode COLLATE utf8_general_ci 
			and flag = 0
            group by loginname 
			order by loginname FOR UPDATE;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	
	DECLARE exit handler for sqlexception
		BEGIN 
			-- ERROR
			GET DIAGNOSTICS CONDITION 1
			@p2 = MESSAGE_TEXT;
			ROLLBACK;
			INSERT t_error_log
				SET source = "onreckonround", error = @p2, extra = p_roundcode;
			COMMIT;
			SELECT @p2;
		END;
		
	START TRANSACTION;
	
    select bankerpoint, playerpoint, pair, begintime, closetime into p_bankerpoint, p_playerpoint, p_pair, p_begintime, p_closetime 
		from t_rounds 
		where roundcode = p_roundcode COLLATE utf8_general_ci for update;

	open cur_loginname;
	read_loop: LOOP
		fetch cur_loginname into p_loginname, p_banker_bet_amount_cents, p_player_bet_amount_cents;
		if done then
			LEAVE read_loop;
		end if;
		call onreckon(p_roundcode, p_loginname, p_banker_bet_amount_cents, p_player_bet_amount_cents, p_bankerpoint, p_playerpoint, p_pair, p_begintime, p_closetime);
	END LOOP;
	-- 更新 t_rounds
	update t_rounds 
		set flag = 1 
		where roundcode = p_roundcode COLLATE utf8_general_ci;
	
	COMMIT;
	SELECT 0;
END$$

CREATE PROCEDURE  onreckon(IN p_roundcode VARCHAR(16), IN p_loginname VARCHAR(32), IN p_banker_bet_amount_cents INT, IN p_player_bet_amount_cents INT, IN p_bankerpoint tinyint(4), IN p_playerpoint tinyint(4), IN p_pair tinyint(4), IN p_begintime datetime, IN p_closetime datetime)
BEGIN

	DECLARE valid_type int;
	DECLARE final_valid_bet_amount_cents, valid_bet_amount_cents, win_amount_cents INT default 0;
	DECLARE new_credit_cents INT;
	DECLARE bfirst bool default true;
	-- From t_customers
	DECLARE p_credit_cents INT;
	DECLARE p_agentcode int;
    -- From t_orders
    DECLARE p_bet_amount_cents INT;
    DECLARE p_playtype tinyint;
    DECLARE p_billno int;
    DECLARE p_billtime datetime;
    -- DECLARE cur_rounds cursor FOR select bankerpoint, playerpoint, pair, cardnum, begintime, closetime from t_rounds where roundcode = roundcode;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cur_orders cursor FOR 
		select  bet_amount_cents, playtype, billno, Create_time 
			from t_orders 
			where roundcode = p_roundcode COLLATE utf8_general_ci 
				and loginname = p_loginname COLLATE utf8_general_ci
				and flag = 0  FOR UPDATE;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
	
	select credit_cents, agentcode into p_credit_cents, p_agentcode 
		from t_customers 
		where loginname = p_loginname COLLATE utf8_general_ci  FOR UPDATE;
    
    open cur_orders;
    
    read_loop: LOOP
		fetch cur_orders into p_bet_amount_cents,  p_playtype, p_billno, p_billtime;
		if done then
			LEAVE read_loop;
		end if;

		if p_playtype = 1 then 
		-- 下注莊贏
			if p_bankerpoint > p_playerpoint then 
			-- 莊贏
				set win_amount_cents = p_bet_amount_cents * 0.95;
			elseif p_bankerpoint < p_playerpoint then
			-- 閒贏
				set win_amount_cents = -p_bet_amount_cents;
			else
			-- 和局
				set win_amount_cents = 0; -- 返还本金
			end if;
		elseif p_playtype = 2 then
		-- 下注閒贏
			if p_bankerpoint > p_playerpoint then 
			-- 莊贏
				set win_amount_cents = -p_bet_amount_cents;
			elseif p_bankerpoint < p_playerpoint then
			-- 閒贏
				set win_amount_cents = p_bet_amount_cents;
			else
			-- 和局
				set win_amount_cents = 0; -- 返还本金
			end if;
		elseif p_playtype = 3 then 
		-- 下注和局
			if p_bankerpoint = p_playerpoint then 
			-- 和局
				set win_amount_cents = p_bet_amount_cents * 8;
			else
			-- 莊贏/閒贏
				set win_amount_cents = -p_bet_amount_cents;
			end if;
		elseif p_playtype = 4 then
		-- 下注莊對
			if p_pair = 1 or p_pair = 4 then 
			-- 莊對/雙對
				set win_amount_cents = p_bet_amount_cents * 11;
			else
				set win_amount_cents = -p_bet_amount_cents;
			end if;
		elseif p_playtype = 5 then
		-- 下注閒對
			if p_pair = 2 or p_pair = 4 then 
			-- 閒對/雙對
				set win_amount_cents = p_bet_amount_cents * 11;
			else
				set win_amount_cents = -p_bet_amount_cents;
			end if;
		end if;

		if bfirst = true and (p_playtype = 1 or p_playtype = 2) then
		-- 莊閒對沖, 只計算第一條投注為有效投注
			set bfirst = false;
			if p_bankerpoint > p_playerpoint then 
			-- 莊贏
				set valid_bet_amount_cents = abs(p_banker_bet_amount_cents * 0.95 - p_player_bet_amount_cents);
			else
			-- 閒贏 或 和局
				set valid_bet_amount_cents = abs(p_player_bet_amount_cents - p_banker_bet_amount_cents);
			end if;
		elseif p_playtype <> 1 and p_playtype <> 2 then
		-- 非庄闲的都算进有效投注额
			set valid_bet_amount_cents = p_bet_amount_cents;
		else
			set valid_bet_amount_cents = 0;
		end if;
		
		set new_credit_cents = p_credit_cents + win_amount_cents + p_bet_amount_cents;
		
		-- 更新 t_orders
		update t_orders 
			set 
				win_amount_cents = win_amount_cents, 
				valid_bet_amount_cents = valid_bet_amount_cents, 
				flag = 1,
				reckon_time = now() 
			where billno = p_billno; -- 在前面能取到billno
		
		-- 插入 t_customer_trans
		insert t_customer_trans 
			set 
				remark = p_billno, 
				action_time = now(), 
				loginname = p_loginname, 
				`action` = '派彩',
				trans_amount_cents = win_amount_cents + p_bet_amount_cents,
				before_credit_cents = p_credit_cents, 
				after_credit_cents = new_credit_cents,
				agentcode = p_agentcode;
		
		-- 更新 t_customers
		update t_customers 
			set credit_cents = new_credit_cents 
			where loginname = p_loginname COLLATE utf8_general_ci;
		
		set p_credit_cents = new_credit_cents;
	END LOOP;
	-- SELECT 1;
END$$

DELIMITER ;