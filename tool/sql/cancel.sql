DROP PROCEDURE IF EXISTS oncancelround;
DROP PROCEDURE IF EXISTS oncancel;
DELIMITER $$

CREATE PROCEDURE  oncancelround(IN p_roundcode VARCHAR(16))
BEGIN
	DECLARE p_loginname varchar(32);
    DECLARE done INT DEFAULT FALSE;
	DECLARE cur_loginname cursor FOR select distinct loginname 
		from t_orders 
		where roundcode = p_roundcode COLLATE utf8_general_ci 
			and flag = 0;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	
	DECLARE exit handler for sqlexception
		BEGIN
			-- ERROR
			ROLLBACK;
			SELECT 1003;
		END;
	START TRANSACTION;
	
	open cur_loginname;
	read_loop: LOOP
		fetch cur_loginname into p_loginname;
		if done then
			LEAVE read_loop;
		end if;
		call oncancel(p_roundcode, p_loginname);
	END LOOP;
	-- 插入 t_recalc_rounds
	insert t_recalc_rounds
		set 
			action = '取消局',
			roundcode = p_roundcode;
	-- 更新 t_rounds
	update t_rounds 
		set flag = 1 
		where roundcode = p_roundcode COLLATE utf8_general_ci;
	
	COMMIT;
	SELECT 0;
END$$

CREATE PROCEDURE  oncancel(IN p_roundcode VARCHAR(16), IN p_loginname VARCHAR(32))
BEGIN

	DECLARE new_credit_cents INT;
	-- From t_customers
	DECLARE p_credit_cents INT;
	DECLARE p_agentcode int;
    -- From t_orders
    DECLARE p_bet_amount_cents INT;
    DECLARE p_billno int;
    -- DECLARE cur_rounds cursor FOR select bankerpoint, playerpoint, pair, cardnum, begintime, closetime from t_rounds where roundcode = roundcode;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cur_orders cursor FOR 
		select  bet_amount_cents, billno
			from t_orders 
			where roundcode = p_roundcode COLLATE utf8_general_ci 
				and loginname = p_loginname COLLATE utf8_general_ci
				and flag = 0;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
	
	select credit_cents, agentcode into p_credit_cents, p_agentcode 
		from t_customers 
		where loginname = p_loginname COLLATE utf8_general_ci limit 1;
    
    open cur_orders;
    
    read_loop: LOOP
		fetch cur_orders into p_bet_amount_cents, p_billno;
		if done then
			LEAVE read_loop;
		end if;
		
		set new_credit_cents = p_credit_cents + p_bet_amount_cents;
		
		-- 更新 t_orders
		update t_orders 
			set 
				win_amount_cents = 0, 
				valid_bet_amount_cents = 0, 
				flag = -1, -- 取消結算
				reckon_time = now() 
			where billno = p_billno; -- 在前面能取到billno
		
		-- 插入 t_customer_trans
		insert t_customer_trans 
			set 
				remark = p_billno, 
				action_time = now(), 
				loginname = p_loginname, 
				`action` = '取消注單',
				trans_amount_cents = p_bet_amount_cents,
				before_credit_cents = p_credit_cents, 
				after_credit_cents = new_credit_cents,
				agentcode = p_agentcode;
		
		-- 更新 t_customers
		update t_customers 
			set credit_cents = new_credit_cents 
			where loginname = p_loginname COLLATE utf8_general_ci;
		
		set p_credit_cents = new_credit_cents;
	END LOOP;
	SELECT 0;
END$$

DELIMITER ;