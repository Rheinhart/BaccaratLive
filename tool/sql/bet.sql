
DROP PROCEDURE IF EXISTS onbet;
DELIMITER $$

CREATE PROCEDURE  onbet(
	IN p_roundcode VARCHAR(16), 
    IN p_loginname VARCHAR(32), 
    IN playtype INT, 
    IN bet_amount_cents INT,
    IN tableid VARCHAR(4), 
    IN seat INT, 
    IN create_ip VARCHAR(16),
    IN p_dealer VARCHAR(16),
    IN p_videoid VARCHAR(4),
    IN p_gametype VARCHAR(16),
	IN hashcode VARCHAR(32))
BEGIN

	
	DECLARE new_credit_cents INT;
    -- DECLARE hashcode VARCHAR(32);
    DECLARE p_billno INT;
	-- From t_customers
	DECLARE p_credit_cents INT;
	DECLARE p_agentcode INT;
	
	DECLARE exit handler for sqlexception
		BEGIN 
			-- ERROR
			GET DIAGNOSTICS CONDITION 1
			@p2 = MESSAGE_TEXT;
			ROLLBACK;
			INSERT t_error_log
				SET source = "onbet", error = @p2, extra = p_roundcode;
			COMMIT;
			SELECT @p2;
		END;
	START TRANSACTION;
    
	select credit_cents, agentcode 
    into p_credit_cents, p_agentcode 
    from t_customers 
    where loginname = p_loginname COLLATE utf8_general_ci FOR UPDATE;
    
    set new_credit_cents = p_credit_cents - bet_amount_cents;
    -- set hashcode = md5(CONCAT(p_roundcode, playtype, bet_amount_cents, 'BaccaAsianArk'));
		
	-- 插入 t_orders
	insert t_orders 
    set 
		gametype = p_gametype, 
        loginname = p_loginname, 
        agentcode = agentcode, 
        roundcode = p_roundcode, 
        videoid = p_videoid, 
        tableid = tableid, 
        seat = seat, 
        dealer = p_dealer, 
        flag = 0, 
        playtype = playtype, 
        bet_amount_cents = bet_amount_cents, 
        hashcode = hashcode, 
        before_credit_cents = p_credit_cents, 
        after_credit_cents = new_credit_cents, 
        create_time = now(),
        create_ip = create_ip;
        
	SELECT LAST_INSERT_ID() into p_billno;
    
    -- 寫入顯示給用戶的訂單號end
    
    update t_orders
    set billno_display = CONCAT(DATE_FORMAT(create_time, "%y%m%d%H%i"), LPAD(billno % 10000, 4, '0'))
	where billno = p_billno;
	
	-- 插入 t_customer_trans
	insert t_customer_trans 
    set 
		remark = p_billno, 
        action_time = now(), 
        loginname = p_loginname, 
        `action` = '下注',
		trans_amount_cents = -bet_amount_cents,
		before_credit_cents = p_credit_cents, 
		after_credit_cents = new_credit_cents, 
		agentcode = p_agentcode;
	
	-- 更新 t_customers
	update t_customers 
    set credit_cents = new_credit_cents 
    where loginname = p_loginname COLLATE utf8_general_ci;
    
	COMMIT;
    SELECT 0;
END$$
DELIMITER ;