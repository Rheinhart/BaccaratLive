DROP PROCEDURE IF EXISTS onrecalcround;
DROP PROCEDURE IF EXISTS onrecalc;
DELIMITER $$

CREATE PROCEDURE  onrecalcround(IN p_roundcode VARCHAR(16))
BEGIN
	DECLARE p_loginname varchar(32);
    DECLARE done INT DEFAULT FALSE;
	DECLARE cur_loginname cursor FOR select distinct loginname 
		from t_orders 
		where roundcode = p_roundcode COLLATE utf8_general_ci 
			and flag = 0;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
	
	DECLARE exit handler for sqlexception
		BEGIN
			-- ERROR
			ROLLBACK;
			SELECT 1003;
		END;
	START TRANSACTION;
	
	-- 先檢查有沒有無效的注單
	-- call checkinvalidorders(p_roundcode);
	open cur_loginname;
	read_loop: LOOP
		fetch cur_loginname into p_loginname;
		if done then
			LEAVE read_loop;
		end if;
		call onreckon(p_roundcode, p_loginname);
	END LOOP;
	-- 插入 t_recalc_rounds
	insert t_recalc_rounds
		set 
			action = '重算局',
			roundcode = p_roundcode;
	-- 更新 t_rounds
	update t_rounds 
		set flag = 1 
		where roundcode = p_roundcode COLLATE utf8_general_ci;
	
	COMMIT;
	SELECT 0;
END$$

CREATE PROCEDURE  onrecalc(IN p_roundcode VARCHAR(16), IN p_loginname VARCHAR(32))
BEGIN

	DECLARE valid_type int;
	DECLARE final_valid_bet_amount_cents, valid_bet_amount_cents, win_amount_cents INT default 0;
	DECLARE banker_bet_amount_cents, player_bet_amount_cents, new_credit_cents INT;
	DECLARE bfirst bool default true;
	-- From t_customers
	DECLARE p_credit_cents INT;
	DECLARE p_agentcode int;
    -- From t_orders
    DECLARE p_bet_amount_cents INT;
    DECLARE p_playtype tinyint;
    DECLARE p_billno int;
    DECLARE p_billtime datetime;
    -- From t_rounds
    DECLARE p_bankerpoint, p_playerpoint, p_pair tinyint(4);
    DECLARE p_begintime, p_closetime datetime;
    -- DECLARE cur_rounds cursor FOR select bankerpoint, playerpoint, pair, cardnum, begintime, closetime from t_rounds where roundcode = roundcode;
    DECLARE done INT DEFAULT FALSE;
    DECLARE cur_orders cursor FOR 
		select  bet_amount_cents, playtype, billno, Create_time 
			from t_orders 
			where roundcode = p_roundcode COLLATE utf8_general_ci 
				and loginname = p_loginname COLLATE utf8_general_ci
				and flag = 0;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    select bankerpoint, playerpoint, pair, begintime, closetime into p_bankerpoint, p_playerpoint, p_pair, p_begintime, p_closetime 
		from t_rounds 
		where roundcode = p_roundcode COLLATE utf8_general_ci limit 1;
	
	select ifnull(sum(bet_amount_cents), 0) into banker_bet_amount_cents 
		from t_orders 
		where loginname = p_loginname COLLATE utf8_general_ci 
			and roundcode = p_roundcode COLLATE utf8_general_ci 
			and playtype = 1
			and flag = 0;
	
	select ifnull(sum(bet_amount_cents), 0) into player_bet_amount_cents 
		from t_orders 
		where loginname = p_loginname COLLATE utf8_general_ci 
			and roundcode = p_roundcode COLLATE utf8_general_ci 
			and playtype = 2
			and flag = 0;
	
	select credit_cents, agentcode into p_credit_cents, p_agentcode 
		from t_customers 
		where loginname = p_loginname COLLATE utf8_general_ci limit 1;
    
    open cur_orders;
    
    read_loop: LOOP
		fetch cur_orders into p_bet_amount_cents,  p_playtype, p_billno, p_billtime;
		if done then
			LEAVE read_loop;
		end if;

		if p_playtype = 1 then 
		-- 下注莊贏
			if p_bankerpoint > p_playerpoint then 
			-- 莊贏
				set win_amount_cents = p_bet_amount_cents * 0.95;
			elseif p_bankerpoint < p_playerpoint then
			-- 閒贏
				set win_amount_cents = -p_bet_amount_cents;
			else
			-- 和局
				set win_amount_cents = 0; -- 返还本金
			end if;
		elseif p_playtype = 2 then
		-- 下注閒贏
			if p_bankerpoint > p_playerpoint then 
			-- 莊贏
				set win_amount_cents = -p_bet_amount_cents;
			elseif p_bankerpoint < p_playerpoint then
			-- 閒贏
				set win_amount_cents = p_bet_amount_cents;
			else
			-- 和局
				set win_amount_cents = 0; -- 返还本金
			end if;
		elseif p_playtype = 3 then 
		-- 下注和局
			if p_bankerpoint = p_playerpoint then 
			-- 和局
				set win_amount_cents = p_bet_amount_cents * 8;
			else
			-- 莊贏/閒贏
				set win_amount_cents = -p_bet_amount_cents;
			end if;
		elseif p_playtype = 4 then
		-- 下注莊對
			if pair = 1 or pair = 4 then 
			-- 莊對/雙對
				set win_amount_cents = p_bet_amount_cents * 11;
			else
				set win_amount_cents = -p_bet_amount_cents;
			end if;
		elseif p_playtype = 5 then
		-- 下注閒對
			if pair = 2 or pair = 4 then 
			-- 閒對/雙對
				set win_amount_cents = p_bet_amount_cents * 11;
			else
				set win_amount_cents = -p_bet_amount_cents;
			end if;
		end if;

		if bfirst = true and (p_playtype = 1 or p_playtype = 2) then
		-- 莊閒對沖, 只計算第一條投注為有效投注
			set bfirst = false;
			if p_bankerpoint > p_playerpoint then 
			-- 莊贏
				set valid_bet_amount_cents = abs(banker_bet_amount_cents * 0.95 - player_bet_amount_cents);
			else
			-- 閒贏 或 和局
				set valid_bet_amount_cents = abs(player_bet_amount_cents - banker_bet_amount_cents);
			end if;
		elseif p_playtype <> 1 and p_playtype <> 2 then
		-- 非庄闲的都算进有效投注额
			set valid_bet_amount_cents = p_bet_amount_cents;
		else
			set valid_bet_amount_cents = 0;
		end if;
		
		set new_credit_cents = p_credit_cents + win_amount_cents + p_bet_amount_cents;
		
		-- 更新 t_orders
		update t_orders 
			set 
				win_amount_cents = win_amount_cents, 
				valid_bet_amount_cents = valid_bet_amount_cents, 
				flag = 8, -- 重新派彩
				reckon_time = now() 
			where billno = p_billno; -- 在前面能取到billno
		
		-- 插入 t_customer_trans
		insert t_customer_trans 
			set 
				remark = p_billno, 
				action_time = now(), 
				loginname = p_loginname, 
				`action` = '重新派彩',
				trans_amount_cents = win_amount_cents + p_bet_amount_cents,
				before_credit_cents = p_credit_cents, 
				after_credit_cents = new_credit_cents,
				agentcode = p_agentcode;
		
		-- 更新 t_customers
		update t_customers 
			set credit_cents = new_credit_cents 
			where loginname = p_loginname COLLATE utf8_general_ci;
		
		set p_credit_cents = new_credit_cents;
	END LOOP;
	SELECT 0;
END$$

DELIMITER ;