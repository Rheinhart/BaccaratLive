#coding:utf8

import gevent
from gevent import monkey
monkey.patch_all()

import struct,json
import random
import time
from datetime import datetime
import websocket
from firefly.netconnect.datapack import DataPackProtoc
from hashlib import md5
from thread import start_new_thread

import commandHandler as ch
key = 'BaccaAsianArk'
code_list = json.load(open("code.json"))


def now():
    return time.strftime("%H:%M:%S")
def produceData(sendstr,commandId):
    """78,37,38,48,9,0"""
    HEAD_0 = chr(78)
    HEAD_1 = chr(37)
    HEAD_2 = chr(38)
    HEAD_3 = chr(48)
    ProtoVersion = chr(9)
    ServerVersion = 0
    sendstr = sendstr
    data = struct.pack('!sssss3I',HEAD_0,HEAD_1,HEAD_2,\
                       HEAD_3,ProtoVersion,ServerVersion,\
                       len(sendstr)+4,commandId)
    senddata = data+sendstr
    return senddata

import Queue
data = Queue.Queue()

class Roboter(gevent.Greenlet):
    
    def __init__(self,loginname,password, index):
        """
        """
        gevent.Greenlet.__init__(self)
        self.loginname = loginname
        self.password = password
        self.buff = ""
        self.tableid = ""
        self.seat = index
        self.status = -1
        self.video_info = {}
        self.video_id = ""
        self.tables = {}
        self.ws = websocket.create_connection("ws://127.0.0.1:11009") # 202.77.29.210:21009
        self.dataprotocl=DataPackProtoc(78,37,38,48,9,0)
        self.datahandler=self.dataHandleCoroutine()
        self.datahandler.next()
        
    def connect(self):
        """建立连接
        """
        #self.ws.connect()
        pass
        #self.client.connect(ADDR)
        
    def loginToServer(self):
        """登陆
        """
        command = 10001
        print self.loginname, "loginToServer"
        loginname = self.loginname
        password = self.password
        md5pwd = md5(loginname+key+password).hexdigest()
        request = ch.getProto(command)
        request.loginname = loginname
        request.password = md5pwd
        self.ws.send(produceData(request.SerializeToString(), command))

    def logout(self):
        """ 登出 0x00010003 """
        command = 10003
        a = produceData("", command)
        self.ws.send(a)

    def loginToGameServer(self):
        """ 登录 0x00010011 """
        command = 10017
        print self.loginname, "loginToGameServer"
        request = ch.getProto(command)
        request.loginname = self.loginname
        request.token = self.token
        a = produceData(request.SerializeToString(), command)
        self.ws.send(a)

    def enterTable(self):
        """ 进桌 0x00010019 """
        command = 10025
        request = ch.getProto(command)
        request.tableid = self.tables.keys()[random.randint(0, len(self.tables.keys())-1)]
        request.seat = random.randint(0, self.tables[request.tableid]['seats'] - 1)
        print self.loginname, "enterTable", request.tableid, request.seat
        self.tableid = request.tableid
        self.video_id = self.tables[self.tableid]['vid']
        a = produceData(request.SerializeToString(), command)
        self.ws.send(a)

    def autoEnterTable(self):
        command = 10048
        request = ch.getProto(command)
        request.videoid = self.video_info.keys()[random.randint(0, len(self.video_info.keys())-1)]
        a = produceData(request.SerializeToString(), command)
        self.ws.send(a)

    def exitTable(self):
        """ 离桌 0x0001001a """
        command = 10026
        print self.loginname, "exitTable"
        request = ch.getProto(command)
        a = produceData(request.SerializeToString(), command)
        self.ws.send(a)

    def bet(self, round_code):
        """ 下注 0x0001001c """
        print self.loginname, "bet", round_code
        command = 10028
        request = ch.getProto(command)
        request.roundcode = round_code
        request.playtype = random.randint(1, 7)
        request.amount_cents = str(random.randint(1000, 10000))
        a = produceData(request.SerializeToString(), command)
        self.ws.send(a)

    def pot_detail(self, table_id, round_code):
        print self.loginname, "potdetail", table_id, round_code
        # if not table_id or not round_code:
        command = 10030
        request = ch.getProto(command)
        request.tableid = table_id
        request.playtype = random.randint(1, 7)
        request.roundcode = round_code
        a = produceData(request.SerializeToString(), command)
        self.ws.send(a)

    def get_video_seats(self):
        command = 10050
        request = ch.getProto(command)
        vid = self.video_info.keys()[random.randint(0, len(self.video_info.keys())-1)]
        print self.loginname, "get_video_seats", vid
        request.videoid = vid
        a = produceData(request.SerializeToString(), command)
        self.ws.send(a)

    def action(self):
        """机器人行动
        """
        if self.status == -1:
            return
        self.starttime = datetime.now()
        if self.status == 0:
            rate = random.choice([1, 2])
            if rate == 1:
                self.enterTable()
            elif rate == 2:
                self.get_video_seats()
            return
        rate = random.choice([1, 2, 3])
        if rate == 1:
            self.bet(self.video_info[self.video_id]["roundcode"])
        elif rate == 2:
            self.pot_detail(self.tableid, self.video_info[self.video_id]["roundcode"])
        elif rate == 3:
            self.exitTable()

    def _run(self):
        self.connect()
        #gevent.sleep(random.random()*2)
        self.starttime = datetime.now()
        self.loginToServer()
        #self.rolelogin()
        #self.datahandler=self.dataHandleCoroutine()
        #self.datahandler.next()
        self.dataprotocl=DataPackProtoc(78,37,38,48,9,0)
        while True:
            gevent.sleep(0.1)
            self.datahandler.send(self.ws.recv())

    def requestHandler(self, command, request):
        #print("*"*45)
        #print(str(command).center(45, " "))
        command_text = ch.getName(command)
        #print self.loginname, command_text
        #print(command_text.center(45, " "))
        #print("*"*45)
        response = ch.getProto(command)
        if response is not None:
            response.ParseFromString(request)
        #print(response)
        # 登录回包 0x00020001
        if command == 20001:
            time_elapsed = (datetime.now() - self.starttime).seconds
            print self.loginname, "login time_elapsed: ", time_elapsed, "seconds"
            #gevent.sleep(random.random()*2)
            if response.code == 0:
                self.token = response.token
                self.loginToGameServer()
        elif command == 20017:
            time_elapsed = (datetime.now() - self.starttime).seconds
            print self.loginname, "login game,time_elapsed,", time_elapsed, "seconds"
            #gevent.sleep(random.random()*2)
        elif command == 20047:
            self.status = 0
            # self.starttime = datetime.now()
            # self.enterTable()
            for video in response.details:
                d = {'roundcode': video.roundcode, 'status': video.status}
                self.video_info[str(video.videoid)] = d
            self.action()
        elif command == 20025:
            if response.code == 0:
                self.status = 1
            time_elapsed = (datetime.now() - self.starttime).seconds
            print self.loginname, "enter time_elapsed: ", time_elapsed, "seconds", "with code", response.code
            self.action()
            # self.exitTable()
        elif command == 20026:
            time_elapsed = (datetime.now() - self.starttime).seconds
            print self.loginname, "exit time_elapsed: ", time_elapsed, "seconds"
            self.status = 0
            # gevent.sleep(random.random()*2)
            # self.starttime = datetime.now()
            # self.enterTable()
            self.action()
        elif command == 905:
            print self.loginname, "was kicked out"
        elif command == 20028:
            time_elapsed = (datetime.now() - self.starttime).total_seconds()
            print self.loginname, "bet time_elapsed: ", time_elapsed, "seconds with code", response.code
            if response.code == 0:
                output = "%s\t%s\t%.1f" % (time.strftime("%y/%m/%d\t%H:%M:%S"), "bet", time_elapsed)
                data.put(output)
            self.action()
        elif command == 20030:
            time_elapsed = (datetime.now() - self.starttime).seconds
            print self.loginname, "potdetail time_elapsed: ", time_elapsed, "seconds"
            self.action()
        elif command == 20050:
            time_elapsed = (datetime.now() - self.starttime).seconds
            print self.loginname, "get_video_seats time_elapsed: ", time_elapsed, "seconds"
            self.action()
        elif command == 20005:
            for video in response.video:
                for table in video.table:
                    info = {'lid':table.limitId, 'seats':table.seats, 'vid':video.videoId}
                    self.tables[table.tableId] = info
        elif command == 20045:
            d = {'roundcode': response.roundcode, 'status': response.status}
            self.video_info[response.videoid] = d


    def dataHandleCoroutine(self):
        length = self.dataprotocl.getHeadlength()
        while True:
            data = yield
            self.buff += data
            while self.buff.__len__() >= length:
                # print type(self.buff[:length]), len(self.buff[:length])
                unpackdata = self.dataprotocl.unpack(self.buff[:length])
                #print unpackdata
                if not unpackdata.get('result'):
                    print ('illegal data package --')
                    #self.transport.loseConnection()
                    break
                command = unpackdata.get('command')
                rlength = unpackdata.get('length')
                request = self.buff[length:length+rlength]
                if request.__len__()< rlength:
                    print('some data lose')
                    break
                self.buff = self.buff[length+rlength:]
                self.requestHandler(command, request)

def writer():
    import csv
    f = open("playerrecord2.log", "wb")
    w = csv.writer(f, delimiter = '\t')
    f.write("datetime\taction\ttime elapsed\n")
    while True:
        gevent.sleep(10)
        while data.qsize() > 0:
            d = data.get()
            f.write(d + "\n")
        f.flush()

if __name__=="__main__":
    start_new_thread(writer, ())
    rotlist = []
    for i in range(1, 100):
        rot = Roboter("jason%s"%i,"123456", i)
        rot.start()
        rotlist.append(rot)
    gevent.joinall(rotlist)
        


        
    
    
    
    
    

    
    
    
    
    
    


