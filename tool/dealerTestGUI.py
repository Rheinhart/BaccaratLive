#coding=utf8

import sys
sys.path.append("..")
import time
from datetime import datetime
import json
from enum import Enum

from Tkinter import *
from twisted.internet import tksupport
from twisted.python import log
from twisted.internet import reactor

from common import WSClientFactory, sendData, MyClientProtocol
import commandHandler as ch

code_list = json.load(open("code.json"))

class GameStatus(Enum):
    EGS_UNKNOWN = 0
    EGS_NEWSHOE = 1  # 新建靴
    EGS_BETTING = 2  # 只在有这个状态下才接受下注
    EGS_DISTACH_CARD = 3  # 发牌状态
    EGS_CLOSEGAME = 4  # 游戏结束

class UI:

    def __init__(self, master):
        self.root = master

    def init(self, protocol):

        self.protocol = protocol
        inputPanel = Frame(self.root)
        inputPanel.pack(side="left", fill="y")
        displayPanel = Frame(self.root)
        displayPanel.pack(side="left")
        tablePanel = Frame(self.root)
        tablePanel.pack(side="left", fill="y")
        self.drawDisplayPanel(displayPanel)
        self.drawInputPanel(inputPanel)
        self.drawTablePanel(tablePanel)

    def drawDisplayPanel(self, parent):
        self.txt = Text(parent, height=50, width=50)
        self.scr = Scrollbar(parent)
        self.scr.config(command=self.txt.yview)
        self.txt.config(yscrollcommand=self.scr.set)
        self.scr.pack(side="right", fill="y", expand=False)
        self.txt.pack(side="left", fill="both", expand=True)

    def drawInputPanel(self, parent):
        self.makebutton(parent, text="Login", command = self.protocol.dealerLoginToServer)
        self.loginname = self.makeentry(parent, "Loginname", default="joshua")
        self.makebutton(parent, text="New shoe", command = self.protocol.dealerNewShoe)
        self.makebutton(parent, text="New round", command = self.protocol.dealerNewRound)
        self.makebutton(parent, text="Send Card", command = self.protocol.dealerSendCard)
        self.cardIndex = self.makeentry(parent, "Card index")
        self.cardValue = self.makeentry(parent, "Card value")
        self.makebutton(parent, text="Confirm Result", command = self.protocol.dealerConfirmResult)
        self.makebutton(parent, text="Cancel Result", command = self.protocol.dealerCancelResult)

    def drawTablePanel(self, parent):
        self.videoid = self.makeentry(parent, "Video id", default="V01")
        self.gametype = self.makeentry(parent, "Game type")
        self.gamestatus = self.makeentry(parent, "Game status")
        self.roundcode = self.makeentry(parent, "Round code")
        self.bettime = self.makeentry(parent, "Bet time")

    def makeentry(self, parent, caption, default="", width=None, **options):
        Label(parent, text=caption).pack()
        entry = Entry(parent, **options)
        if default:
            entry.insert(END, default)
        if width:
            entry.config(width=width)
        entry.pack()
        return entry

    def makebutton(self, parent, text="", command=None):
        button = Button(parent, text=text, command = command, bg="#07B4FF", fg="black")
        button.pack(fill="x")

    def updateEntry(self, entry, text):
        entry.delete(0, END)
        entry.insert(END, str(text))

    def output(self, response):
        self.txt.insert(END, str(response))
        if not type(response) is str:
            try:
                self.txt.insert(END,  "code = " + code_list.get(str(response.code), "")+'\n')
            except:
                pass
        self.txt.insert(END, '\n')
        self.txt.see(END)
        self.txt.update()
        time.sleep(0.1)


class DealerProtocol(MyClientProtocol):

    token = ""
    loginname = ""
    starttime = datetime.now()
    bettime = 60
    status = GameStatus.EGS_UNKNOWN

    def __init__(self):
        MyClientProtocol.__init__(self)
        self.ui = ui
        self.ui.init(self)
        self.updateBetTime()

    def updateBetTime(self):
        if self.status == GameStatus.EGS_BETTING:
            time_diff = self.bettime - (datetime.now() - self.starttime).seconds
            now = max(0, time_diff)
        else:
            now = 0
        self.ui.updateEntry(self.ui.bettime, now)
        self.ui.root.after(500, self.updateBetTime)

    def output(self, text):
        self.ui.output(text)

    def dealerLoginToServer(self):
        """ 荷官登錄 0x00030001 """
        command = 30001
        request = ch.getProto(command)
        request.loginName = self.ui.loginname.get()
        request.videoId = self.ui.videoid.get()
        a = sendData(request.SerializeToString(), command)
        self.sendMessage(a)

    def dealerNewShoe(self):
        """ 新建靴 0x00030002 """
        command = 30002
        a = sendData("new shoe", command)
        self.sendMessage(a)

    def dealerNewRound(self):
        """ 開局 0x00030003 """
        command = 30003
        a = sendData("new shoe", command)
        self.sendMessage(a)


    def dealerSendCard(self):
        """ 發牌 0x00030005 """
        command = 30005
        request = ch.getProto(command)
        request.roundCode = self.ui.roundcode.get()
        request.cardValue = int(self.ui.cardValue.get())
        request.cardIndex = int(self.ui.cardIndex.get())
        a = sendData(request.SerializeToString(), command)
        self.sendMessage(a)
        self.ui.updateEntry(self.ui.cardValue, "")

    def dealerConfirmResult(self):
        """  確認結果 0x00030009"""
        command = 30009
        request = ch.getProto(command)
        request.roundCode = self.ui.roundcode.get()
        a = sendData(request.SerializeToString(), command)
        self.sendMessage(a)

    def dealerCancelResult(self):
        """  取消結果 0x0003000a"""
        command = 30010
        request = ch.getProto(command)
        request.roundCode = self.ui.roundcode.get()
        a = sendData(request.SerializeToString(), command)
        self.sendMessage(a)


    def requestHandler(self, command, request):
        self.output("*"*45)
        self.output(str(command).center(45, " "))
        command_text = ch.getName(command)
        self.output(command_text.center(45, " "))
        self.output("*"*45)
        response = ch.getProto(command)
        response.ParseFromString(request)
        self.output(response)
        # 開局回包 0x00040003
        if command == 40003:
            if response.code == 0:
                self.bettime = response.betTime
                self.starttime = datetime.now()
                self.status = GameStatus.EGS_BETTING
                self.ui.updateEntry(self.ui.gamestatus, self.status.name)
                self.ui.updateEntry(self.ui.roundcode, response.roundCode)
                #self.ui.updateEntry(self.ui.betTime, response.betTime)
        # 新建靴回包 0x00040002
        elif command == 40002:
            if response.code == 0:
                self.status = GameStatus.EGS_NEWSHOE
                self.ui.updateEntry(self.ui.gamestatus, self.status.name)
        # 提示發牌 0x00040004
        elif command == 40004:
            self.status = GameStatus.EGS_DISTACH_CARD
            self.ui.updateEntry(self.ui.gamestatus, self.status.name)
            self.ui.updateEntry(self.ui.cardIndex, response.cardIndex)
        # 遊戲結果 0x00040007
        elif command == 40007:
            self.status = GameStatus.EGS_CLOSEGAME
            self.ui.updateEntry(self.ui.gamestatus, self.status.name)
        # 視頻狀態 0x00040006
        elif command == 40006:
            self.bettime = response.bettime
            self.starttime = datetime.now()
            self.status = GameStatus(int(response.status))
            self.ui.updateEntry(self.ui.gamestatus, self.status.name)
            self.ui.updateEntry(self.ui.gametype, response.gametype)
            self.ui.updateEntry(self.ui.roundcode, response.roundcode)

class DealerFactory(WSClientFactory):
    protocol = DealerProtocol

log.startLogging(sys.stdout)

def on_closing():
    root.destroy()
    reactor.stop()

root = Tk()
root.protocol("WM_DELETE_WINDOW", on_closing)
ui = UI(root)
tksupport.install(root)

# Local
factory = DealerFactory(u"ws://127.0.0.1:11009", debug=False)
reactor.connectTCP("127.0.0.1", 11009, factory)

# Server
# factory = DealerFactory(u"ws://202.77.29.210:21009", debug=False)
# reactor.connectTCP("202.77.29.210", 21009, factory)

reactor.run()