#coding:utf8
"""
@__author__ = 'Thomas'
"""

from twisted.internet import reactor
from twisted.web.server import Site
from twisted.web.resource import Resource
from tool.protoFile import bulletin_pb2

class TestDjangoPost(Resource):
    isLeaf = True

    def render_POST(self, request):
        bulletin = bulletin_pb2.bulletinResponse()
        bulletin.ParseFromString(request.content.read())
        print 'Bulletin received. BeginTime: %s, EndTime: %s'%(bulletin.beginTime,bulletin.endTime)
        print 'Content: %s'%bulletin.text



reactor.listenTCP(2014,Site(TestDjangoPost()))
reactor.run()