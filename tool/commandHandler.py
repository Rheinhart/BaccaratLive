#coding=utf8
from app.share.protofile import *
from app.share.protofile.controller import *

command_list = {
"1":{"name":"心跳包","proto":None},
"905":{"name":"登出包","proto":None},
"10001":{"name":"登錄","proto":login_pb2.loginRequest()},
"20001":{"name":"登錄回包","proto":login_pb2.loginResultResponse()},
"20002":{"name":"用戶信息","proto":login_pb2.userInfoResponse()},
"10003":{"name":"登出","proto":None},
#"20004":{"name":"遊戲服務信息","proto":None},
"20005":{"name":"視頻桌台信息","proto":getVideos_pb2.getVideosResponse()},
"10017":{"name":"登錄遊戲","proto":login_pb2.loginGameRequest()},
"20017":{"name":"登錄回包","proto":login_pb2.loginGameResultResponse()},
"20018":{"name":"個人限紅","proto":personalLimit_pb2.personalLimitResponse()},
"20019":{"name":"桌台限紅","proto":tableLimit_pb2.tableLimitResponse()},
"20020":{"name":"歷史露珠","proto":getBeadRoad_pb2.getBeadRoadResponse()},
"10021":{"name":"請求在線玩家列表","proto":None},
"20021":{"name":"在線玩家列表","proto":player_pb2.playerListResponse()},
"20023":{"name":"玩家位置更新","proto":player_pb2.playerSeatUpdateResponse()},
"20024":{"name":"玩家額度更新","proto":player_pb2.playerCreditResponse()},
"10025":{"name":"進桌","proto":player_pb2.enterTableRequest()},
"20025":{"name":"進桌回包","proto":player_pb2.enterTableResponse()},
"10026":{"name":"離桌","proto":player_pb2.exitTableRequest()},
"20026":{"name":"離桌回包","proto":None},
#"20027":{"name":"開始下注","proto":game_pb2.startGameResponse()},
"10028":{"name":"下注","proto":game_pb2.betRequest()},
"20028":{"name":"下注回包","proto":game_pb2.betResponse()},
"20029":{"name":"實時彩池","proto":game_pb2.potResponse()},
"10030":{"name":"查詢實時彩池明細","proto":game_pb2.tablePotDetailsRequest()},
"20030":{"name":"實時彩池明細","proto":game_pb2.tablePotDetailsResponse()},
#"20032":{"name":"遊戲結果","proto":game_pb2.gameResultResponse()},
"20033":{"name":"結算","proto":game_pb2.reckonResponse()},
"20034":{"name":"同桌其他人下注","proto":game_pb2.otherPlayersBetResponse()},
#"10035":{"name":"請求視頻狀態","proto":None},
"20035":{"name":"視頻狀態","proto":game_pb2.videoStatusResponse()},
#"20036":{"name":"荷官信息","proto":game_pb2.dealerResponse()},
"20037":{"name":"公告信息","proto":bulletin_pb2.bulletinResponse()},
"20038":{"name":"三局不下提示","proto":game_pb2.noBetResponse()},
"20039":{"name":"桌子已下注信息","proto":game_pb2.tableBetResponse()},
"20040":{"name":"同桌其他人結算","proto":game_pb2.otherPlayersReckonResponse()},
"10041":{"name":"查詢下注記錄","proto":getBetRecords_pb2.getBetRecordsRequest()},
"20041":{"name":"下注記錄","proto":getBetRecords_pb2.getBetRecordsResponse()},
#"10042":{"name":"查詢額度記錄","proto":None},
#"20042":{"name":"額度記錄","proto":None},
#"20043":{"name":"新建靴","proto":game_pb2.newShowResponse()},
"20044":{"name":"大廳主玩法彩池信息","proto":game_pb2.lobbyPotResponse()},
"20045":{"name":"視頻狀態更新","proto":game_pb2.videoStatusUpdateResponse()},
"20046":{"name":"發牌","proto":game_pb2.cardInfoResponse()},
"20047":{"name":"視頻狀態列表","proto":game_pb2.videoStatusListResponse()},
"20049":{"name":"玩家最後位置","proto":player_pb2.lastPositionResponse()},
"10048":{"name":"自动进桌","proto":player_pb2.autoEnterTableRequest()},
"10050":{"name":"請求視频座位列表","proto":player_pb2.videoPlayersRequest()},
"20050":{"name":"視频座位列表","proto":player_pb2.videoPlayersResponse()},
"20051":{"name":"桌台玩家列表","proto":player_pb2.tablePlayersResponse()},
"10052":{"name":"查詢玩家位置","proto":player_pb2.playerPositionRequest()},
"20052":{"name":"玩家位置","proto":player_pb2.playerPositionResponse()},
"19999":{"name":"測試","proto":None},
"29999":{"name":"測試回包","proto":None},
"30001":{"name":"荷官登錄","proto":dealerLogin_pb2.dealerLoginRequest()},
"40001":{"name":"荷官登錄回包","proto":dealerLogin_pb2.dealerLoginResponse()},
"30002":{"name":"新建靴","proto":None},
"40002":{"name":"新建靴回包","proto":dealerNewShoe_pb2.dealerNewShoeResponse()},
"30003":{"name":"開局","proto":None},
"40003":{"name":"開局回包","proto":dealerNewRound_pb2.dealerNewRoundResponse()},
"40004":{"name":"提示發牌","proto":sendCardReminder_pb2.senderCardReminderRequest()},
"30005":{"name":"發牌","proto":dealerNewCard_pb2.dealerNewCardRequest()},
"40005":{"name":"發牌回包","proto":dealerNewCard_pb2.dealerNewCardResponse()},
#"30006":{"name":"請求視頻狀態","proto":None},
"40006":{"name":"視頻狀態","proto":dealerVideoStatus_pb2.videoStatusResponse()},
"40007":{"name":"遊戲結果","proto":gameResult_pb2.gameResultResponse()},
#"40008":{"name":"結算確認","proto":payOutConfirm_pb2.payOutConfirmResponse()},
"30009":{"name":"確認結果","proto":dealerConfirmResult_pb2.dealerConfirmResultRequest()},
"40009":{"name":"確認結果回包","proto":dealerConfirmResult_pb2.dealerConfirmResultResponse()},
"30010":{"name":"取消結果","proto":dealerCancelResult_pb2.dealerCancelResultRequest()},
"40010":{"name":"取消結果回包","proto":dealerCancelResult_pb2.dealerCancelResultResponse()},
"50001":{"name":"管理端登錄","proto":None},
"60001":{"name":"管理端登錄回包","proto":None},
#"50002":{"name":"查詢視頻列表","proto":None},
#"60002":{"name":"視頻列表","proto":None},
"50003":{"name":"添加視頻","proto":addVideo_pb2.addVideoRequest()},
"60003":{"name":"添加視頻回包","proto":addVideo_pb2.addVideoResponse()},
"50004":{"name":"修改視頻","proto":modifyVideo_pb2.modifyVideoRequest()},
"60004":{"name":"添加視頻回包","proto":modifyVideo_pb2.modifyVideoResponse()},
"50005":{"name":"查詢指定視頻桌台列表","proto":None},
"60005":{"name":"指定視頻桌台列表","proto":None},
"50006":{"name":"添加桌台","proto":addTable_pb2.addTableRequest()},
"60006":{"name":"添加桌台回包","proto":addTable_pb2.addTableResponse()},
"50007":{"name":"修改桌台","proto":modifyTable_pb2.modifyTableRequest()},
"60007":{"name":"修改桌台回包","proto":modifyTable_pb2.modifyTableResponse()},
#"50008":{"name":"查詢桌台限紅","proto":None},
#"60008":{"name":"桌台限紅","proto":None},
"50009":{"name":"修改桌台限紅","proto":modifyTableLimit_pb2.modifyTableLimitRequest()},
"60009":{"name":"修改桌台限紅回包","proto":None},
#"50010":{"name":"查詢個人限紅","proto":None},
#"60010":{"name":"個人限紅","proto":None},
"50011":{"name":"修改個人限紅","proto":modifyPersonalLimit_pb2.modifyPersonalLimitRequest()},
"60011":{"name":"修改個人限紅回包","proto":None},
#"50012":{"name":"查詢公告","proto":None},
#"60012":{"name":"有效公告","proto":None},
"50013":{"name":"新增公告","proto":bulletin_pb2.bulletinResponse()},
"60013":{"name":"新增公告回包","proto":None},
"50014":{"name":"刪除公告","proto":None},
"60014":{"name":"刪除公告回包","proto":None},
"50015":{"name":"查詢局結果","proto":None},
"60015":{"name":"局結果","proto":None},
"50016":{"name":"重新結算局","proto":None},
"60016":{"name":"重新結算局回包","proto":None},
"50017":{"name":"取消局注單結算","proto":None},
"60017":{"name":"取消局注單結算回包","proto":None},
}

def getName(command):
  command = str(command)
  item = command_list.get(command)
  if item:
    return item.get("name", "")

def getProto(command):
  command = str(command)
  item = command_list.get(command)
  if item:
    return item.get("proto")