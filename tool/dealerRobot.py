#coding:utf8

import gevent
from gevent import monkey
monkey.patch_all()

import struct,json
import random
import time
from datetime import datetime
import websocket
from firefly.netconnect.datapack import DataPackProtoc
from thread import start_new_thread

import commandHandler as ch
key = 'BaccaAsianArk'
code_list = json.load(open("code.json"))

from enum import Enum

def now():
    return time.strftime("%H:%M:%S")

class GameStatus(Enum):
    EGS_UNKNOWN = 0
    EGS_NEWSHOE = 1  # 新建靴
    EGS_BETTING = 2  # 只在有这个状态下才接受下注
    EGS_DISTACH_CARD = 3  # 发牌状态
    EGS_CLOSEGAME = 4  # 游戏结束

def produceData(sendstr,commandId):
    """78,37,38,48,9,0"""
    HEAD_0 = chr(78)
    HEAD_1 = chr(37)
    HEAD_2 = chr(38)
    HEAD_3 = chr(48)
    ProtoVersion = chr(9)
    ServerVersion = 0
    sendstr = sendstr
    data = struct.pack('!sssss3I',HEAD_0,HEAD_1,HEAD_2,\
                       HEAD_3,ProtoVersion,ServerVersion,\
                       len(sendstr)+4,commandId)
    senddata = data+sendstr
    return senddata


import Queue
data = Queue.Queue()

class Roboter(gevent.Greenlet):

    def __init__(self, loginname, video_index):
        """
        """
        gevent.Greenlet.__init__(self)
        self.loginname = loginname
        self.buff = ""
        self.tableid = ""
        self.status = GameStatus.EGS_UNKNOWN
        self.video_info = {}
        self.video_id = "V{:02d}".format(video_index)
        self.tables = {}
        self.round_code = ""
        self.ws = websocket.create_connection("ws://127.0.0.1:11009") # 202.77.29.210:21009
        self.dataprotocl=DataPackProtoc(78,37,38,48,9,0)
        self.datahandler=self.dataHandleCoroutine()
        self.datahandler.next()

    def connect(self):
        print "connected"

    def dealerLoginToServer(self):
        """ 荷官登錄 0x00030001 """
        gevent.sleep(random.randint(0, 3))
        command = 30001
        request = ch.getProto(command)
        request.loginName = self.loginname
        request.videoId = self.video_id
        a = produceData(request.SerializeToString(), command)
        self.ws.send(a)

    def dealerNewShoe(self):
        """ 新建靴 0x00030002 """
        gevent.sleep(random.randint(0, 3))
        command = 30002
        a = produceData("new shoe", command)
        self.ws.send(a)

    def dealerNewRound(self):
        """ 開局 0x00030003 """
        gevent.sleep(random.randint(0, 3))
        command = 30003
        a = produceData("new round", command)
        self.ws.send(a)

    def dealerSendCard(self, index):
        """ 發牌 0x00030005 """
        command = 30005
        gevent.sleep(random.randint(0, 3))
        request = ch.getProto(command)
        request.roundCode = self.round_code
        request.cardValue = random.randint(1, 13)
        request.cardIndex = index
        print now(), self.loginname, "send the %d card for" % index, self.video_id, "with value", request.cardValue
        a = produceData(request.SerializeToString(), command)
        self.ws.send(a)

    def dealerConfirmResult(self):
        """  確認結果 0x00030009"""
        command = 30009
        request = ch.getProto(command)
        request.roundCode = self.round_code
        a = produceData(request.SerializeToString(), command)
        self.ws.send(a)

    def dealerCancelResult(self):
        """  取消結果 0x0003000a"""
        command = 30010
        request = ch.getProto(command)
        request.roundCode = self.round_code
        a = produceData(request.SerializeToString(), command)
        self.ws.send(a)

    def _run(self):
        self.connect()
        self.starttime = datetime.now()
        self.dealerLoginToServer()
        while True:
            gevent.sleep(0.1)
            # print now(), self.loginname, "Waiting"
            self.datahandler.send(self.ws.recv())

    def requestHandler(self, command, request):
        response = ch.getProto(command)
        if response is not None:
            response.ParseFromString(request)
        # 登陆回包
        if command == 40001:
            if response.code == 0:
                print now(), self.loginname, "login success for", self.video_id
                self.dealerNewShoe()
        # 開局回包 0x00040003
        elif command == 40003:
            if response.code == 0:
                self.bettime = response.betTime
                self.starttime = datetime.now()
                self.round_code = response.roundCode
                self.status = GameStatus.EGS_BETTING
                print now(), self.loginname, "new round success for", self.video_id
        # 新建靴回包 0x00040002
        elif command == 40002:
            if response.code == 0:
                print now(), self.loginname, "new shoe success for", self.video_id
                self.status = GameStatus.EGS_NEWSHOE
                self.dealerNewRound()
        # 提示發牌 0x00040004
        elif command == 40004:
            self.status = GameStatus.EGS_DISTACH_CARD
            # self.ui.updateEntry(self.ui.cardIndex, response.cardIndex)
            self.dealerSendCard(response.cardIndex)
        # 遊戲結果 0x00040007
        elif command == 40007:
            self.status = GameStatus.EGS_CLOSEGAME
            self.dealerConfirmResult()
            self.starttime = datetime.now()
        # 視頻狀態 0x00040006
        elif command == 40006:
            self.bettime = response.bettime
            self.starttime = datetime.now()
            self.status = GameStatus(int(response.status))
            self.round_code = response.roundcode
        # 确认结果回包
        elif command == 40009:
            if response.code == 0:
                te = (datetime.now() - self.starttime).total_seconds()
                output = "%s\t%s\t%.1f" % (time.strftime("%y/%m/%d\t%H:%M:%S"), "reckon", te)
                data.put(output)
                print now(), self.loginname, "confirmed result for", self.video_id, "with round", self.round_code, "time elapsed:", te, "seconds"
                #print now(), self.loginname, "wait for server to reckon for", self.video_id, "with round", self.round_code
                #gevent.sleep(15)
                print now(), self.loginname, "start again", self.video_id
                if random.random() < 0.8:
                    self.dealerNewRound()
                else:
                    self.dealerNewShoe()
            else:
                print now(), self.loginname, "get result confirm reply with code", response.code

    def dataHandleCoroutine(self):
        length = self.dataprotocl.getHeadlength()
        while True:
            data = yield
            # print now(), self.loginname, "data recv"
            self.buff += data
            while self.buff.__len__() >= length:
                # print type(self.buff[:length]), len(self.buff[:length])
                unpackdata = self.dataprotocl.unpack(self.buff[:length])
                #print unpackdata
                if not unpackdata.get('result'):
                    print ('illegal data package --')
                    #self.transport.loseConnection()
                    break
                command = unpackdata.get('command')
                rlength = unpackdata.get('length')
                request = self.buff[length:length+rlength]
                if request.__len__()< rlength:
                    print('some data lose')
                    break
                self.buff = self.buff[length+rlength:]
                self.requestHandler(command, request)

def writer():
    import csv
    f = open("dealerrecord.log", "wb")
    #w = csv.writer(f, delimiter = '\t')
    f.write("datetime\taction\ttime elapsed\n")
    while True:
        gevent.sleep(10)
        while data.qsize() > 0:
            d = data.get()
            f.write(d + "\n")
        f.flush()

if __name__=="__main__":
    start_new_thread(writer, ())
    rotlist = []
    for i in range(1, 10):
        rot = Roboter("joshua%s"%i, i)
        rot.start()
        rotlist.append(rot)
    gevent.joinall(rotlist)
