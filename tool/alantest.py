
from pprint import pprint
from datetime import datetime
f = open("slow-query.log", "r")
t = f.read()
#print t
queries = []
d = t.split("# Time: ")
for dd in d:
    try:
        query = {}
        query["raw"] = dd
        query["time"] = datetime.strptime(dd.split("\n")[0].split(".")[0], "%Y-%m-%dT%H:%M:%S")
        query["query_time"] = float(dd.split("# Query_time: ")[1].split("  Lock_time:")[0])
        query["lock_time"] = float(dd.split("  Lock_time: ")[1].split(" Rows_sent:")[0])
        query["query_string"] = dd.split("SET timestamp=")[1].split("\n")[1]
        queries.append(query)
    except:
        pass

queries = filter(lambda x : "information_schema" not in x["query_string"], queries)
queries = filter(lambda x : "performance_schema" not in x["query_string"], queries)
queries = filter(lambda x : "Select_priv" not in x["query_string"], queries)
#queries = filter(lambda x : x["lock_time"]>0, queries)
#queries = filter(lambda x : x["query_time"]>5, queries)
#queries = filter(lambda x : x["time"]>datetime.strptime("2015-12-17 03:00:00", "%Y-%m-%d %H:%M:%S"), queries)
queries = sorted(queries, key = lambda x : x["time"])

pprint(queries)

