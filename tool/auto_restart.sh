#!/bin/bash
#Created by thomas
#For Linux Server, Game Server monitor

GamSer_DIR="/home/thomas/AsianArkProject/BaccaratLive"
PID_DIR="/app/logs"
SLEEP_TIME=30
while :
  do
     echo "++++++++++++++++GameServer Monitoring++++++++++++++++++++++++++"
     #get the game service pid
     game1_pid=$(sudo cat ${GamSer_DIR}${PID_DIR}/pid/game1.pid)
     gate_pid=$(sudo cat ${GamSer_DIR}${PID_DIR}/pid/gate.pid)
     net_pid=$(sudo cat ${GamSer_DIR}${PID_DIR}/pid/net.pid)
     dbfront_pid=$(sudo cat ${GamSer_DIR}${PID_DIR}/pid/dbfront.pid)
     admin_pid=$(sudo cat ${GamSer_DIR}${PID_DIR}/pid/admin.pid)

#    game1_pid=$(sudo ps -s | awk '$10=="python" && $11 =="appmain.py" && $12 == "game1" {print $2}')
#    gate_pid=$(sudo ps -s | awk '$10=="python" && $11 =="appmain.py" && $12 == "gate" {print $2}')
#    net_pid=$(sudo ps -s | awk '$10=="python" && $11 =="appmain.py" && $12 == "net" {print $2}')
#    dbfront_pid=$(sudo ps -s | awk '$10=="python" && $11 =="appmain.py" && $12 == "dbfront" {print $2}')
#    admin_pid=$(sudo ps -s | awk '$10=="python" && $11 =="appmain.py" && $12 == "admin" {print $2}')

     has_game1_pid=$(sudo ps -ax | awk '{ print $1 }' | grep -e "^${game1_pid}$")
     has_gate_pid=$(sudo ps -ax | awk '{ print $1 }' | grep -e "^${gate_pid}$")
     has_net_pid=$(sudo ps -ax | awk '{ print $1 }' | grep -e "^${net_pid}$")
     has_dbfront_pid=$(sudo ps -ax | awk '{ print $1 }' | grep -e "^${dbfront_pid}$")
     has_admin_pid=$(sudo ps -ax | awk '{ print $1 }' | grep -e "^${admin_pid}$")

     if [ ! $has_game1_pid ]; then
        #kill the 8777 port
        TCP_PID=$(sudo lsof -i:8887 | awk '{print $2}' |grep -v 'PID')
        if [ $TCP_PID ]; then
            sudo kill -9 ${TCP_PID}
            echo "TCP Port:8887, PID:"${TCP_PID}" has been killed!"
        fi
        echo "game1 service lose, GameServer restarting..."
        cd ${GamSer_DIR}
        nohup sudo python startmaster.py>/dev/null 2>&1 &
        sleep ${SLEEP_TIME}
        continue
    elif [ ! $has_gate_pid ]; then

        #kill the 8777 port
        TCP_PID=$(sudo lsof -i:8887 | awk '{print $2}' |grep -v 'PID')
        if [ $TCP_PID ]; then
            sudo kill -9 ${TCP_PID}
            echo "TCP Port:8887, PID:"${TCP_PID}" has been killed!"
        fi
        echo "gate service lose, GameServer restarting..."
        cd ${GamSer_DIR}
        nohup sudo python ${GamSer_DIR}/startmaster.py>/dev/null 2>&1 &
        sleep ${SLEEP_TIME}
        continue

    elif [ ! $has_net_pid ]; then

        #kill the 8777 port
        TCP_PID=$(sudo lsof -i:8887 | awk '{print $2}' |grep -v 'PID')
        if [ $TCP_PID ]; then
            sudo kill -9 ${TCP_PID}
            echo "TCP Port:8887, PID:"${TCP_PID}" has been killed!"
        fi
        echo "net service lose, GameServer restarting..."
        cd ${GamSer_DIR}
        nohup sudo python ${GamSer_DIR}/startmaster.py>/dev/null 2>&1 &
        sleep ${SLEEP_TIME}
        continue

    elif [ ! $has_admin_pid ]; then

        #kill the 8777 port
        TCP_PID=$(sudo lsof -i:8887 | awk '{print $2}' |grep -v 'PID')
        if [ $TCP_PID ]; then
        sudo kill -9 ${TCP_PID}
        echo "TCP Port:8887, PID:"${TCP_PID}" has been killed!"
        fi

        echo "admin service lose, GameServer restarting..."
        cd ${GamSer_DIR}
        nohup sudo python ${GamSer_DIR}/startmaster.py>/dev/null 2>&1 &
        sleep ${SLEEP_TIME}
        continue

    elif [ ! $has_dbfront_pid ]; then

        #kill the 8777 port
        TCP_PID=$(sudo lsof -i:8887 | awk '{print $2}' |grep -v 'PID')
        if [ $TCP_PID ]; then
        sudo kill -9 ${TCP_PID}
        echo "TCP Port:8887, PID:"${TCP_PID}" has been killed!"
        fi

        echo "dbfront service lose, GameServer restarting..."
        cd ${GamSer_DIR}
        nohup sudo python ${GamSer_DIR}/startmaster.py>/dev/null 2>&1 &
        sleep ${SLEEP_TIME}
        continue
    fi
    echo ""
sleep ${SLEEP_TIME}
done
