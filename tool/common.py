__author__ = 'Alan'
###############################################################################
#
# The MIT License (MIT)
#
# Copyright (c) Tavendo GmbH
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
#
###############################################################################

import struct
from firefly.netconnect.datapack import DataPackProtoc
from twisted.internet.protocol import  ClientFactory
from autobahn.twisted.websocket import WebSocketClientProtocol, \
    WebSocketClientFactory

def sendData(sendstr,commandId):
    """78,37,38,48,9,0"""
    HEAD_0 = chr(78)
    HEAD_1 = chr(37)
    HEAD_2 = chr(38)
    HEAD_3 = chr(48)
    ProtoVersion = chr(9)
    ServerVersion = 0
    sendstr = sendstr
    data = struct.pack('!sssss3I',HEAD_0,HEAD_1,HEAD_2,\
                       HEAD_3,ProtoVersion,ServerVersion,\
                       len(sendstr)+4,commandId)
    senddata = data+sendstr
    return senddata

def resolveRecvdata(data):
    print 'Rev'
    # print data
    head = struct.unpack('!sssss3I',data[:17])
    length = head[6] - 6
    # print length
    data = data[17:17+length]
    return data



class MyClientProtocol(WebSocketClientProtocol):

    buff = ""

    def _connectionMade(self):
        super(MyClientProtocol, self)._connectionMade()
        print "connectionMade"
        self.datahandler=self.dataHandleCoroutine()
        self.datahandler.next()


    def onConnect(self, response):
        print("Server connected: {0}".format(response.peer))


    def onOpen(self):
        print("WebSocket connection open.")


    def onMessage(self, payload, isBinary):
        print "messsage"
        if isBinary:
            print("Binary message received: {0} bytes".format(len(payload)))
            self.datahandler.send(payload)
        else:
            print("Text message received: {0}".format(payload.decode("utf8")))
            self.datahandler.send(payload)


    def onClose(self, wasClean, code, reason):
        print("WebSocket connection closed: {0}".format(reason))


    def dataHandleCoroutine(self):
        """
        """
        length = self.factory.dataprotocl.getHeadlength()
        print length
        while True:
            data = yield
            self.buff += data
            while self.buff.__len__() >= length:
                # print type(self.buff[:length]), len(self.buff[:length])
                unpackdata = self.factory.dataprotocl.unpack(self.buff[:length])
                print unpackdata
                if not unpackdata.get('result'):
                    print ('illegal data package --')
                    #self.transport.loseConnection()
                    break
                command = unpackdata.get('command')
                rlength = unpackdata.get('length')
                request = self.buff[length:length+rlength]
                if request.__len__()< rlength:
                    print('some data lose')
                    break
                self.buff = self.buff[length+rlength:]
                self.requestHandler(command, request)


    def requestHandler(self, command, request):
        pass


class WSClientFactory(WebSocketClientFactory):

    protocol = MyClientProtocol

    def __init__(self, url=None, debug=False, dataprotocl=DataPackProtoc(78,37,38,48,9,0)):
        super(WSClientFactory, self).__init__(url=url, debug=debug)
        self.dataprotocl = dataprotocl

    def startedConnecting(self, connector):
        print 'Started to connect.'

    def buildProtocol(self, addr):
        proto = ClientFactory.buildProtocol(self, addr)
        print 'Connected.'
        return proto

    def clientConnectionLost(self, connector, reason):
        print 'Lost connection.  Reason:', reason

    def clientConnectionFailed(self, connector, reason):
        print 'Connection failed. Reason:', reason