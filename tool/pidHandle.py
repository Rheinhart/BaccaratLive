#coding:utf8
__author__ = 'thomas'
import os
import signal

pid_dir = 'app/logs/pid/'

def writePid(servername,pid):
    """将当前每个service的PID记录到logs/pid/下
    """
    try:
        with open('%s%s.pid'%(pid_dir,servername),'w') as pf:
            pf.write('%d'%(pid))
    except IOError:
        pass

def CloseTcpPort():
    """kill the TCP Port Process:8887
    """
    TCP_PID =os.popen("sudo lsof -i:8887 | awk '{print $2}' |grep -v 'PID' ").read()
    if TCP_PID:
        print("closing the prot 8887....")
        os.system("sudo kill -9 %s"%TCP_PID)

def killOldPid():
    """在Unix下退出服务器后,threads.deferToThread会驻留在系统后台，重新启动GameSer需要先检查并杀死残留进程
    """
    for root,dirs,files in os.walk(pid_dir):
        for pf in files:
            with open(os.path.join(pid_dir, pf)) as f:
                if f:
                    old_pid = f.readline()
                    if old_pid:
                        try:
                            os.system("sudo kill -9 %s"%old_pid)
                            print 'Old Process %s pid:%s has been killed'%(pf[:-4],old_pid)
                        except:
                            print 'Old Process %s pid:%s has been already closed'%(pf[:-4],old_pid)
