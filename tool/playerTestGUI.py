#coding=utf8

import sys
sys.path.append("..")
import time
from datetime import datetime
import json
from enum import Enum
from hashlib import md5

from Tkinter import *
from twisted.internet import tksupport, task
from twisted.python import log
from twisted.internet import reactor

from firefly.server.logobj import loogoo

from common import WSClientFactory, sendData, MyClientProtocol
import commandHandler as ch
key = 'BaccaAsianArk'
code_list = json.load(open("code.json"))

class GameStatus(Enum):
    EGS_UNKNOWN = 0
    EGS_NEWSHOE = 1  # 新建靴
    EGS_BETTING = 2  # 只在有这个状态下才接受下注
    EGS_DISTACH_CARD = 3  # 发牌状态
    EGS_CLOSEGAME = 4  # 游戏结束

class ClientStatus(Enum):
    NOT_LOGIN = 0,
    INLOGIN = 1,
    INGAME = 2,
    INTABLE = 3

class UI:
    def __init__(self, master):
        self.root = master

    def init(self, protocol):
        
        self.protocol = protocol
        inputPanel = Frame(self.root)
        inputPanel.pack(side="left", fill="y")
        displayPanel = Frame(self.root)
        displayPanel.pack(side="left", fill="y")
        tablePanel = Frame(self.root)
        tablePanel.pack(side="left", fill="y")
        self.drawDisplayPanel(displayPanel)
        self.drawInputPanel(inputPanel)
        self.drawTablePanel(tablePanel)

    def drawDisplayPanel(self, parent):
        self.txt = Text(parent, height=50, width=50)
        self.scr = Scrollbar(parent)
        self.scr.config(command=self.txt.yview)
        self.txt.config(yscrollcommand=self.scr.set)
        self.scr.pack(side="right", fill="y", expand=False)
        self.txt.pack(side="left", fill="both", expand=True)

    def drawInputPanel(self, parent):
        self.makebutton(parent, text="Login", command = self.protocol.loginToServer)
        self.loginname = self.makeentry(parent, "Loginname", default="alanwong")
        self.password = self.makeentry(parent, "Password", default="123456")
        self.makebutton(parent, text="Login Game", command = self.protocol.loginToGameServer)
        self.token = self.makeentry(parent, "Token", default="")
        self.makebutton(parent, text="Get Video Seats", command = self.protocol.getVideoSeats)
        self.videoid_request = self.makeentry(parent, "Video id", default="V01")
        self.makebutton(parent, text="Auto Enter Table", command = self.protocol.autoEnterTable)
        self.makebutton(parent, text="Enter Table", command = self.protocol.enterTable)
        self.tableid = self.makeentry(parent, "Table id", default="T01")
        self.seat = self.makeentry(parent, "Seat", default="1")
        self.makebutton(parent, text="Exit Table", command = self.protocol.exitTable)
        self.makebutton(parent, text="Bet", command = self.protocol.bet)
        self.playtype = self.makeentry(parent, "Play type", default="1")
        self.amount_cents = self.makeentry(parent, "Amount (cents)", default="5000")
        self.makebutton(parent, text="Get player position", command = self.protocol.getPlayerPosition)
        self.loginname_request = self.makeentry(parent, "Loginname")
        self.makebutton(parent, text="Pot details", command = self.protocol.potDetails)
        self.tableid_q = self.makeentry(parent, "Table id")
        self.playtype_q = self.makeentry(parent, "Play type")
        self.roundcode_q = self.makeentry(parent, "Round code")
        self.makebutton(parent, text="Logout", command = self.protocol.logout)

    def drawTablePanel(self, parent):
        self.videoid = self.makeentry(parent, "Video id")
        self.dealername = self.makeentry(parent, "Dealer name")
        self.gametype = self.makeentry(parent, "Game type")
        self.gamestatus = self.makeentry(parent, "Game status")
        self.roundcode = self.makeentry(parent, "Round code")
        self.bettime = self.makeentry(parent, "Bet time")
        self.credit_cents = self.makeentry(parent, "Credit (cents)")
        self.limitid = self.makeentry(parent, "limitid")

    def makeentry(self, parent, caption, default="", width=None, **options):
        Label(parent, text=caption).pack()
        entry = Entry(parent, **options)
        if default:
            entry.insert(END, default)
        if width:
            entry.config(width=width)
        entry.pack()
        return entry

    def makebutton(self, parent, text="", command=None):
        button = Button(parent, text=text, command = command, bg="#07B4FF", fg="black")
        button.pack(fill="x")

    def updateEntry(self, entry, text):
        entry.delete(0, END)
        entry.insert(END, str(text))

    def output(self, response):
        self.txt.insert(END, str(response))
        if not type(response) is str:
            try:
                self.txt.insert(END,  "code = " + code_list.get(str(response.code), "")+'\n')
            except:
                pass
        self.txt.insert(END, '\n')
        self.txt.see(END)
        self.txt.update()
        time.sleep(0.1)

class PlayerProtocol(MyClientProtocol):

    startTime = datetime.now()
    status = ClientStatus.NOT_LOGIN
    bettime = 0
    videos = {}
    tables = {}

    def __init__(self):
        MyClientProtocol.__init__(self)
        self.ui = ui
        self.ui.init(self)
        self.updateBetTime()
        self.t = None

    def updateBetTime(self):
        if self.status == GameStatus.EGS_BETTING:
            time_diff = self.bettime - (datetime.now() - self.startTime).seconds
            now = max(0, time_diff)
        else:
            now = 0
        self.ui.updateEntry(self.ui.bettime, now)
        self.ui.root.after(500, self.updateBetTime)

    def output(self, text):
        self.ui.output(text)

    def sendHeartBeat(self):
        print "sendHeartBeat"
        command = 1
        a = sendData("", command)
        self.sendMessage(a)

    def loginToServer(self):
        """ 登录 0x00010001 """
        command = 10001
        loginname = self.ui.loginname.get()
        password = self.ui.password.get()
        md5pwd = md5(loginname+key+password).hexdigest()
        request = ch.getProto(command)
        request.loginname = loginname
        request.password = md5pwd
        a = sendData(request.SerializeToString(), command)
        self.sendMessage(a)

    def logout(self):
        """ 登出 0x00010003 """
        command = 10003
        a = sendData("", command)
        self.sendMessage(a)

    def loginToGameServer(self):
        """ 登录 0x00010011 """
        command = 10017
        request = ch.getProto(command)
        request.loginname = self.ui.loginname.get()
        request.token = self.ui.token.get()
        a = sendData(request.SerializeToString(), command)
        self.sendMessage(a)
        self.t = task.LoopingCall(self.sendHeartBeat)
        self.t.start(10.0)  # call every second
        # self.sendHeartBeat()

    def getVideoSeats(self):
        """ 請求視频座位列表 0x00010032 """
        command = 10050
        request = ch.getProto(command)
        request.videoid = self.ui.videoid_request.get()
        a = sendData(request.SerializeToString(), command)
        self.sendMessage(a)

    def getPlayerPosition(self):
        """ 查詢玩家位置 0x00010034 """
        command = 10052
        request = ch.getProto(command)
        request.loginname = self.ui.loginname_request.get()
        a = sendData(request.SerializeToString(), command)
        self.sendMessage(a)

    def enterTable(self):
        """ 进桌 0x00010019 """
        command = 10025
        request = ch.getProto(command)
        request.tableid = self.ui.tableid.get()
        request.seat = int(self.ui.seat.get())
        a = sendData(request.SerializeToString(), command)
        self.sendMessage(a)

    def autoEnterTable(self):
        command = 10048
        request = ch.getProto(command)
        request.videoid = self.ui.videoid_request.get()
        a = sendData(request.SerializeToString(), command)
        self.sendMessage(a)

    def exitTable(self):
        """ 离桌 0x0001001a """
        command = 10026
        request = ch.getProto(command)
        a = sendData(request.SerializeToString(), command)
        self.sendMessage(a)
        self.status = ClientStatus.INGAME

    def bet(self):
        """ 下注 0x0001001c """
        command = 10028
        request = ch.getProto(command)
        request.roundcode = self.ui.roundcode.get()
        request.playtype = int(self.ui.playtype.get())
        request.amount_cents = self.ui.amount_cents.get()
        a = sendData(request.SerializeToString(), command)
        self.sendMessage(a)

    def potDetails(self):
        """ 查询实时彩池明细 0x0001001e """
        command = 10030
        request = ch.getProto(command)
        request.tableid = self.ui.tableid_q.get()
        request.playtype = int(self.ui.playtype_q.get())
        request.roundcode = self.ui.roundcode_q.get()
        a = sendData(request.SerializeToString(), command)
        self.sendMessage(a)

    def updateTableInfo(self):
        tableid = self.ui.tableid.get()
        videoid = self.tables.get(tableid).get("videoId")
        video = self.videos.get(videoid)

    def queryBetRecords(self):
        command = 10041
        request = ch.getProto(command)
        request.startTime ="2015-09-30 12:39:07"
        request.endTime = "2015-11-30 12:39:07"
        request.gameType = "BJL"
        request.page = 1
        request.countOfPage = 20
        request.roundCode = "V0115B03000"
        request.billNo = "18"
        a = sendData(request.SerializeToString(), command)
        self.sendMessage(a)

    def requestHandler(self, command, request):
        self.output("*"*45)
        self.output(str(command).center(45, " "))
        command_text = ch.getName(command)
        self.output(command_text.center(45, " "))
        self.output("*"*45)
        response = ch.getProto(command)
        if command == 905 and self.t:
            self.t.stop()
        if response is None:
            return
        response.ParseFromString(request)
        self.output(response)
        #for descriptor  in response.DESCRIPTOR.fields:
        #    value = getattr(response, descriptor.name)
        # 登录回包 0x00020001
        if command == 20001:
            if response.code == 0:
                self.ui.updateEntry(self.ui.token, response.token)
        # 視頻桌台信息 0x00020005
        elif command == 20005:
            for video in response.video:
                self.videos[video.videoId] = {
                    "videoId": video.videoId,
                    "gameType": video.gameType,
                    "url": video.url
                }
                for table in video.table:
                    self.tables[table.tableId] = {
                        "videoId": video.videoId,
                        "tableId": table.tableId,
                        "limitId": table.limitId,
                        "seats": table.seats
                    }
            print self.videos
            print self.tables
        # 玩家額度更新 0x00020018
        elif command == 20024:
            self.ui.updateEntry(self.ui.credit_cents, response.credit_cents)
        # 視頻狀態 0x00020023
        elif command == 20035:
            self.bettime = response.bettime
            self.startTime = datetime.now()
            self.status = GameStatus(int(response.status))
            self.ui.updateEntry(self.ui.gamestatus, self.status.name)
            self.ui.updateEntry(self.ui.dealername, response.dealername)
            self.ui.updateEntry(self.ui.gametype, response.gametype)
            self.ui.updateEntry(self.ui.roundcode, response.roundcode)
            self.ui.updateEntry(self.ui.videoid, response.videoid)
        # 視頻狀態更新 0x0002002d
        elif command == 20045:
            if response.videoid == self.ui.videoid.get():
                self.ui.updateEntry(self.ui.gametype, response.gametype)
                self.ui.updateEntry(self.ui.roundcode, response.roundcode)
                if response.HasField("dealername"):
                    self.ui.updateEntry(self.ui.dealername, response.dealername)
                if response.HasField("status"):
                    self.status = GameStatus(int(response.status))
                    self.ui.updateEntry(self.ui.gamestatus, self.status.name)
                    self.bettime = response.bettime
                    self.startTime = datetime.now()
        # 遊戲結果 0x00020020
        elif command == 20032:
            self.status = GameStatus.EGS_CLOSEGAME
            self.ui.updateEntry(self.ui.gamestatus, self.status.name)
        # 新建靴 0x0002002b
        elif command == 20043:
            self.status = GameStatus.EGS_NEWSHOE
            self.ui.updateEntry(self.ui.gamestatus, self.status.name)
        # 获得最新公告
        elif command == 50013:
            self.output("*"*45)
            self.output(str('公告解码:').center(45, " "))
            self.output(response.text.encode('utf-8'))
            self.output("*"*45)
        # 添加视频
        elif command == 50003:
            self.bettime = response.betTime
            self.startTime = datetime.now()
            self.ui.updateEntry(self.ui.gametype, response.gameType)
            self.ui.updateEntry(self.ui.videoid, response.videoId)
        # 修改视频
        elif command == 50004:
            self.ui.updateEntry(self.ui.gamestatus, self.status.name)
            self.ui.updateEntry(self.ui.videoid, response.videoId)
        # 添加桌台
        elif command == 50006:
            self.ui.updateEntry(self.ui.tableid, response.tableId)
            self.ui.updateEntry(self.ui.videoid, response.videoId)
            self.ui.updateEntry(self.ui.limitid, response.limitId)
        # 修改桌台
        elif command == 50007:
            self.ui.updateEntry(self.ui.tableid, response.tableId)
            self.ui.updateEntry(self.ui.videoid, response.videoId)
            self.ui.updateEntry(self.ui.limitid, response.limitId)
        # 玩家最後位置 0x00020031
        elif command == 20049:
            if response.tableid:
                self.ui.updateEntry(self.ui.tableid, response.tableid)
                self.ui.updateEntry(self.ui.seat, response.seat)
                self.enterTable()


class PlayerFactory(WSClientFactory):
    protocol = PlayerProtocol

# log.addObserver(loogoo("../app/logs/client.log"))
# log.startLogging(sys.stdout)
log.startLogging(open('../app/logs/client.log', 'w'))

def on_closing():
    root.destroy()
    reactor.stop()

root = Tk()
root.protocol("WM_DELETE_WINDOW", on_closing)
ui = UI(root)
tksupport.install(root)

# Local
factory = PlayerFactory(u"ws://127.0.0.1:11009", debug=False)
reactor.connectTCP("127.0.0.1", 11009, factory)

# Server
# factory = PlayerFactory(u"ws://202.77.29.210:21009", debug=False)
# reactor.connectTCP("202.77.29.210", 21009, factory)

reactor.run()